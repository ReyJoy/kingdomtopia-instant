﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class WaterAnimate : MonoBehaviour
    {
        private Material mat;
        private float xOffset = 0;
        private float yOffset = 0;
        private static readonly int OffsetUvY = Shader.PropertyToID("_OffsetUvY");
        private static readonly int OffsetUvX = Shader.PropertyToID("_OffsetUvX");

        private void Awake()
        {
            mat = GetComponent<Image>().material;
        }
        
        void Start()
        {
            DOTween.To(()=> xOffset, x=> xOffset = x, 1, 5).SetLoops(-1, LoopType.Restart).SetEase(Ease.Linear);
            DOTween.To(()=> yOffset, x=> yOffset = x, 1, 5).SetLoops(-1, LoopType.Restart).SetEase(Ease.Linear);
        }
        
        void Update()
        {
            mat.SetFloat(OffsetUvX, xOffset);
            mat.SetFloat(OffsetUvY, yOffset);
        }
    }
}