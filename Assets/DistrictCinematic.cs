﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictCinematic : MonoBehaviour
    {
        public UpgradeMenu upgradeMenu;
        private CompositeDisposable districtDisposables;
        public List<int> districtJustAutomated = new List<int>();

        private Queue<int> districtQueue = new Queue<int>();
        private void Start()
        {
            UpgradeMenu.OnOpen += OnUpgradeMenuOpen;
            UpgradeMenu.OnClosed += OnUpgradeMenuClosed;

        }

        private void OnUpgradeMenuOpen()
        {
            districtJustAutomated = new List<int>();
            districtDisposables = new CompositeDisposable();
            var districts = DistrictManager.Instance.districts;
            
            for (int i = 0; i < districts.Count; i++)
            {
                var idx = i;
                var dist = districts[i];
                
                if (!dist.hasManager.Value && dist.isPurchased.Value)
                {
                    dist.hasManager.Where(hasMgr => hasMgr).Subscribe(delegate(bool b)
                    {
                        districtJustAutomated.Add(idx);
                    }).AddTo(districtDisposables);
                }
            }
        }

        private void OnUpgradeMenuClosed()
        {
            if (districtJustAutomated.Count > 0)
            {
                //Debug.Log($"{districtJustAutomated.Count} has just been automated");

                districtQueue = new Queue<int>(districtJustAutomated.OrderBy(x => x));
                StartCoroutine(AnimateDistrictJustAutomated(0f));
            }
            
            if (districtDisposables != null && !districtDisposables.IsDisposed)
            {
                districtDisposables.Dispose();
            }
        }
        
        IEnumerator AnimateDistrictJustAutomated(float delay)
        {
            BlockerHUD.Instance.Hide();
            if (districtQueue.Count == 0)
            {
                yield return null;
            }
            else
            {
                BlockerHUD.Instance.Show(BlockerType.Invisible);
                yield return new WaitForSeconds(delay);

                var districtIdx = districtQueue.Dequeue();
                
                MapScrollingManager.Instance.CenterToDistrict(districtIdx, 1f, delegate
                {
                    BlockerHUD.Instance.Hide();
                    InitializeBuilding(districtIdx);
                    StartCoroutine(AnimateDistrictJustAutomated(0.5f));
                });
            }
        }
        
        void InitializeBuilding(int idx)
        {
            var building = DistrictHUD.Instance.table.items[idx].building;
                    
            building.InitializeBuildingView();
            building.PlayAutomateFx();
        }
    }
}

