#!/usr/bin/env bash
CHECKOUT_DIR=${1?Error: no name given}
BUILD_TARGET=${2?Error: no name given}
PRODUCT_NAME=${3?Error: no name given}
BUILD_NAME=${4?Error: no name given}
BUNDLE_ID=${5?Error: no name given}
EXPORT_TYPE=${6?Error: no name given}
BUILD_MODE=${7?Error: no name given}
JSON_KEY_FILE=${8?Error: no name given}
APPLE_TEAM_ID=${9?Error: no name given}

cd $CHECKOUT_DIR"/AutomatedBuilds"
BUILD_TARGET_LOWERCASE=$(echo "$BUILD_TARGET" | tr '[:upper:]' '[:lower:]')

BUILD_COMMAND="build_debug"
if [ $BUILD_MODE == "PROD" ] 
then 
    BUILD_COMMAND="build_release"
fi 

fastlane $BUILD_TARGET_LOWERCASE $BUILD_COMMAND product_name:"$PRODUCT_NAME" build_name:"$BUILD_NAME" bundle_id:"$BUNDLE_ID" export_type:"$EXPORT_TYPE" json_key_file:$JSON_KEY_FILE apple_team_id:$APPLE_TEAM_ID
