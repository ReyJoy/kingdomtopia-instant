#!/usr/bin/env bash
UNITY_VER=${1?Error: no name given}
CHECKOUT_DIR=${2?Error: no name given}
BUILD_TARGET=${3?Error: no name given}
BUILD_NUMBER=${4?Error: no name given}
DEV_BUILD=${5?Error: no name given}
BUILD_MODE=${6?Error: no name given}
EXPORT_TYPE=${7?Error: no name given}
CERTIFICATE_FOLDER=${8?Error: no name given}
CERTIFICATE_PIN=${9?Error: no name given}
USERNAME=${10?Error: no name given}
PASSWORD=${11?Error: no name given}

echo CHECKOUT_DIR
"/Applications/Unity/Hub/Editor/"$UNITY_VER"/Unity.app/Contents/MacOS/Unity" -quit -batchmode -username $USERNAME -password $PASSWORD -logfile - -projectPath $CHECKOUT_DIR -executemethod Joyseed.Build.CommandLine.CommandLineBuild -buildReporter "TeamCity" -buildTarget "$BUILD_TARGET" -buildNumber $BUILD_NUMBER -devBuild "$DEV_BUILD" -buildMode "$BUILD_MODE" -androidExportType "$EXPORT_TYPE" -certPath $CERTIFICATE_FOLDER -certPIN $CERTIFICATE_PIN