﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.Build
{
    public class BuildPipelineDemo : MonoBehaviour
    {
        public GameObject sboxText;
        
        void Start()
        {
            #if JOYSEED_SBOX
            sboxText.SetActive(true);
            #else
            sboxText.SetActive(false);
            #endif
        }
    }
}
