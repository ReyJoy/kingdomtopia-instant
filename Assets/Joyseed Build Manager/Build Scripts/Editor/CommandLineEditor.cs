﻿/// <summary>
/// Builder script tweaked based on our own requirement. Based on Nordeus' Unity Build Pipeline script.
/// </summary>

using UnityEditor;

namespace Joyseed.Build
{
	public static class CommandLineEditor
	{
		[MenuItem ("Tools/Joyseed/Build Window")]
		public static void OpenBuildWindow ()
		{
			EditorWindow.GetWindow(typeof(BuildPipelineWindow), true, "Joyseed Build Window");
		}
	}
}