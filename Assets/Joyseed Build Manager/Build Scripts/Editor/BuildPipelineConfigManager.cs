using System;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Linq;
using Joyseed.CertificateCreator;

namespace Joyseed.Build
{
    public class BuildPipelineWindow : EditorWindow
    {
        // fill able variables
        private static int _verCode;
        public BuildParameters param;

        private void Awake()
        {
            _verCode = PlayerSettings.Android.bundleVersionCode;
            param = new BuildParameters();
        }

        private void OnGUI()
        {
            ShowBuildConfiguration();
            ShowProjectSettings();
            ShowPlatformSpecificSettings();
            ShowBuildSettings();
            ShowBuildSummary();
        }

        void ShowBuildConfiguration()
        {
            // project build configuration
            GUILayout.Label ("== Project Build Configuration", EditorStyles.boldLabel);
            
            param.buildNumber = EditorGUILayout.IntField("Build Number", param.buildNumber);
            param.buildMode = (JoyseedBuildMode)EditorGUILayout.EnumPopup("Build Mode", param.buildMode);
            param.extraName = EditorGUILayout.TextField("Extra Name", param.extraName);
            
            if (GUILayout.Button("Set Build Mode Settings"))
            {
                BuildDefineSymbols.SetDefineSymbolSetting(param);
            }
        }

        void ShowProjectSettings()
        {
            // data from project settings
            GUILayout.Space(15);
            GUILayout.Label ("== Value from Project Settings", EditorStyles.boldLabel);
            GUILayout.Label("Package Name", EditorStyles.boldLabel);
            GUILayout.TextField(PlayerSettings.GetApplicationIdentifier(param.buildTarget.ToBuildTargetGroup()));
            
            param.buildTarget = (CompatibleBuildTarget)EditorGUILayout.EnumPopup("Build Target", param.buildTarget);
            PlayerSettings.bundleVersion = EditorGUILayout.TextField("Version", PlayerSettings.bundleVersion);
            _verCode = EditorGUILayout.IntField("Ver Code / Build Num", _verCode);
            
            SaveState();
        }

        private static KeystoreInfo keystoreInfo;
        
        void ShowPlatformSpecificSettings()
        {
            if (param.buildTarget == CompatibleBuildTarget.Android)
            {            
                GUILayout.Space(15);
                GUILayout.Label ("== Android Specific Settings", EditorStyles.boldLabel);

                param.certificatePIN = EditorGUILayout.IntField("Encryption PIN", param.certificatePIN);
                
                if (GUILayout.Button("Load Certificates From Folder..."))
                {
                    param.certificatePath = EditorUtility.OpenFolderPanel("", "", "");
                    keystoreInfo = param.GetKeystoreInfo();

                    if (keystoreInfo == null)
                    {
                        param.certificatePath = "";
                    }
                }
                
                if (keystoreInfo == null)
                {
                    EditorGUILayout.HelpBox("Keystore information is empty. Load certificate first.", MessageType.Error);
                }
                else
                {
                    GUILayout.Label("Certificate Path", EditorStyles.boldLabel);
                    GUILayout.TextField(param.certificatePath);

                    GUILayout.Label("Keystore Name :\t\t" + keystoreInfo.keystoreFile);
                    GUILayout.Label("Keystore Pass :\t\t" + "[KEYSTORE PASS]");
                    GUILayout.Label("Keyalias Name :\t\t" + keystoreInfo.keyaliasName);
                    GUILayout.Label("Keyalias Pass :\t\t" + "[KEYALIAS PASS]");
                }

                GUILayout.Space(15);
                
                param.androidExportType = (AndroidExportType)EditorGUILayout.EnumPopup("Export Type", param.androidExportType);
            }
        }

        void ShowBuildSettings()
        {
            GUILayout.Label ("== Build Settings", EditorStyles.boldLabel);
            param.devBuild = EditorGUILayout.Toggle("Development Build", param.devBuild);
            param.buildAndRun = EditorGUILayout.Toggle("Build and Run", param.buildAndRun);
        }

        private void ShowBuildSummary()
        {
            // build summary
            GUILayout.Space(15);
            GUILayout.Label ("== Summary", EditorStyles.boldLabel);
            GUILayout.Label("Build Name", EditorStyles.boldLabel);
            GUILayout.TextField(Utility.GetBuildName(param));
            GUILayout.Label("Scripting Define Symbols");
            GUILayout.TextField(PlayerSettings.GetScriptingDefineSymbolsForGroup(param.buildTarget.ToBuildTargetGroup()));
            
            GUILayout.Space(15);
            if (GUILayout.Button("Save Config and Build"))
            {
                SaveState();
                Builder.Build(param);
            }
        }

        static void SaveState()
        {
            PlayerSettings.Android.bundleVersionCode = _verCode;
            PlayerSettings.iOS.buildNumber = _verCode.ToString();
        }
    }
}