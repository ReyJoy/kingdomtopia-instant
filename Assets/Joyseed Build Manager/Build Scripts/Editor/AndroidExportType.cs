namespace Joyseed.Build
{
    public enum AndroidExportType
    {
        Apk,
        AndroidStudioProject,
        AppBundle,
    }
}