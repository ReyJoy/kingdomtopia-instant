using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Joyseed.CertificateCreator;
using UnityEditor;
using UnityEngine;
using System.Text;

namespace Joyseed.Build
{
    public class BuildParameters
    {
        [Header("Parameters")] 
        public CompatibleBuildTarget buildTarget;
        public int buildNumber;
        public string buildReporter = "Unity";
        public bool devBuild;
        public bool buildAndRun;
        public JoyseedBuildMode buildMode;
        public string extraName;

        [Header("Android-Only")]
        public string certificatePath;
        public int certificatePIN;
        public AndroidExportType androidExportType;

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[BUILD PARAMETERS] ");
            sb.Append($"| buildTarget {buildTarget}");
            sb.Append($"| buildNumber {buildNumber}");
            sb.Append($"| buildReporter {buildReporter}");
            sb.Append($"| devBuild {devBuild}");
            sb.Append($"| buildAndRun {buildAndRun}");
            sb.Append($"| buildMode {buildMode}");
            sb.Append($"| extraName {extraName}");

            if (buildTarget == CompatibleBuildTarget.Android)
            {
                sb.Append($"| certificatePath {certificatePath}");
                sb.Append($"| certificatePIN {certificatePIN}");
                sb.Append($"| androidExportType {androidExportType}");
            }

            return sb.ToString();
        }

        public BuildParameters()
        {
            
        }

        public BuildParameters(Dictionary<string, string> args)
        {
            // get all arguments to strings
            args.TryGetValue(Arguments.BuildTarget,      out var _buildTarget);
            args.TryGetValue(Arguments.BuildNumber,      out var _buildNumber);
            args.TryGetValue(Arguments.BuildReporter,    out var _buildReporter);
            args.TryGetValue(Arguments.DevBuild,         out var _devBuild);
            args.TryGetValue(Arguments.ExtraName,        out var _extraName);
            args.TryGetValue(Arguments.BuildMode,        out var _buildMode);
            args.TryGetValue(Arguments.AndroidExportType,out var _androidExportType);
            args.TryGetValue(Arguments.CertPath,         out var _certPath);
            args.TryGetValue(Arguments.CertPIN,          out var _certPin);
            
            // assign to variables
            buildTarget = (CompatibleBuildTarget)Enum.Parse(typeof(CompatibleBuildTarget), _buildTarget);
            buildNumber = int.Parse(_buildNumber);
            buildReporter = _buildReporter;
            bool.TryParse(_devBuild, out devBuild);
            buildMode = (JoyseedBuildMode)Enum.Parse(typeof(JoyseedBuildMode), _buildMode);
            extraName = _extraName;
            
            certificatePath = _certPath;
            int.TryParse(_certPin, out certificatePIN);
            Enum.TryParse(_androidExportType, out androidExportType);
        }
        
        public KeystoreInfo GetKeystoreInfo()
        {
            var bundleID = PlayerSettings.GetApplicationIdentifier(buildTarget.ToBuildTargetGroup());
            return KeystoreInfoLoader.LoadFile(certificatePath, certificatePIN).keystores.SingleOrDefault(ks => ks.bundleID == bundleID);
        }

        public class Arguments
        {
            public const string BuildTarget = "-buildTarget";
            public const string BuildNumber = "-buildNumber";
            public const string BuildReporter = "-buildReporter";
            public const string DevBuild = "-devBuild";
            public const string ExtraName = "-extraName";
            public const string BuildMode = "-buildMode";
            
            public const string AndroidExportType = "-androidExportType";
            public const string CertPath = "-certPath";
            public const string CertPIN = "-certPIN";
        }
    }
}