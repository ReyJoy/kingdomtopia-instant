/// <summary>
/// Builder script tweaked based on our own requirement. Based on Nordeus' Unity Build Pipeline script.
/// </summary>

using System.Collections.Generic;
using Nordeus.Build.Reporters;
using UnityEngine;

namespace Joyseed.Build
{
	public static class CommandLine
	{
        #region Methods

        /// <summary>
        /// Performs the command line build by using the passed command line arguments.
        /// </summary>
        public static void CommandLineBuild ()
        {
            BuildParameters parameters = new BuildParameters(GetCommandLineArguments());
            Builder.Build(parameters);
        }

        /// <summary>
        /// Gets all the command line arguments relevant to the build process. All commands that don't have a value after them have their value at string.Empty.
        /// </summary>
        static Dictionary<string, string> GetCommandLineArguments ()
        {
            Dictionary<string, string> arguments = new Dictionary<string, string> ();

            string[] args = System.Environment.GetCommandLineArgs ();

            for (int i = 0; i < args.Length; i++) {
                if (args [i].StartsWith ("-")) {
                    string command = args [i];
                    string value = string.Empty;

                    if (i < args.Length - 1 && !args [i + 1].StartsWith ("-".ToString ())) {
                        value = args [i + 1];
                        i++;
                    }

                    if (!arguments.ContainsKey (command)) {
                        Debug.LogFormat("Adding {0} | {1}", command, value);
                        arguments.Add (command, value);
                    } else {
                        BuildReporter.Current.Log ("Duplicate command line argument " + command, BuildReporter.MessageSeverity.Warning);
                    }
                }
            }

            return arguments;
        }

		#endregion
	}
}