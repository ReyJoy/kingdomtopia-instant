using UnityEditor;

namespace Joyseed.Build
{
    public enum CompatibleBuildTarget
    {
        Android,
        iOS
    }

    public static class CompatibleBuildTargetExtension
    {
        public static BuildTarget ToUnityBuildTarget(this CompatibleBuildTarget target)
        {
            return (BuildTarget)System.Enum.Parse(typeof(BuildTarget), target.ToString());
        }

        public static BuildTargetGroup ToBuildTargetGroup(this CompatibleBuildTarget target)
        {
            return (BuildTargetGroup)System.Enum.Parse(typeof(BuildTargetGroup), target.ToString());
        }
    }
}