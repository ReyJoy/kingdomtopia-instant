﻿using System;
using System.Collections.Generic;
using System.IO;
using Nordeus.Build.Reporters;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEditor.Build.Reporting;

namespace Joyseed.Build
{
	public static class Builder
	{
        #region Constants
        
        private const string TargetDirectory = "AutomatedBuilds";
        private const string EnvUnityBuildName = "env.UNITY_BUILD_NAME";
        private const string EnvBundleId = "env.BUNDLE_ID";
        private const string EnvProductName = "env.PRODUCT_NAME";
		#endregion

        #region Methods

        public static void Build(BuildParameters param)
        {
            string buildName = Utility.GetBuildName(param);
            
            ConfigureReporter(param, buildName);
            
            BuildReporter.Current.Log("Building Project: " + param.ToString());
            
            string buildPath = PrepareBuildPath(GetTargetDirectory(param), buildName, param);
            ConfigurePlatformSettings(param);

            try
            {
                CopyFastlaneFolder();
                BuildDefineSymbols.SetDefineSymbolSetting(param);
                BuildProject(buildPath, param);
            } 
            catch (Exception e) 
            {
                BuildReporter.Current.Log (e.Message, BuildReporter.MessageSeverity.Error);
            }
        }

        static void CopyFastlaneFolder()
        {
            var sourceDir = "Assets/Standard Assets/Joyseed Build Manager/Fastlane Scripts";

            FileUtil.DeleteFileOrDirectory(TargetDirectory);
            FileUtil.CopyFileOrDirectory(sourceDir, TargetDirectory);
        }

        static string PrepareBuildPath(string directory, string buildName, BuildParameters param)
        {
            FileUtil.DeleteFileOrDirectory(directory);
            Directory.CreateDirectory(directory);

            string path = Path.Combine(directory, buildName);
            
            if (File.Exists(path)) {
                Debug.Log("Previous file exists. Deleting previous file.");
                File.Delete(path);
            }

            if (param.buildTarget == CompatibleBuildTarget.Android && param.androidExportType == AndroidExportType.Apk)
            {
                if (param.androidExportType == AndroidExportType.Apk)
                {
                    path += ".apk";
                }
                else if (param.androidExportType == AndroidExportType.AppBundle)
                {
                    path += ".aab";
                }
            }

            return path;
        }

        static string GetTargetDirectory(BuildParameters param)
        {
            return Path.Combine(TargetDirectory, param.buildTarget.ToString());
        }

        static void ConfigureReporter(BuildParameters param, string buildName)
        {
            if (!string.IsNullOrEmpty (param.buildReporter))  {
                BuildReporter.Current = BuildReporter.CreateReporterByName (param.buildReporter);
            }
            
            BuildReporter.Current.Log("Building Project: " + buildName);
            BuildReporter.Current.SetEnvironmentParameter(EnvUnityBuildName, buildName);
            BuildReporter.Current.SetEnvironmentParameter(EnvBundleId, PlayerSettings.GetApplicationIdentifier(param.buildTarget.ToBuildTargetGroup()));
            BuildReporter.Current.SetEnvironmentParameter(EnvProductName, PlayerSettings.productName);
        }

        static void ConfigurePlatformSettings(BuildParameters param)
        {
            // set special parameters for Android build
            if (param.buildTarget == CompatibleBuildTarget.Android)
            {
                var keystoreInfo = param.GetKeystoreInfo();
                
                PlayerSettings.Android.keystoreName = Path.Combine(param.certificatePath, keystoreInfo.keystoreFile + ".keystore");
                PlayerSettings.Android.keystorePass = keystoreInfo.keystorePass;
                PlayerSettings.Android.keyaliasName = keystoreInfo.keyaliasName;
                PlayerSettings.Android.keyaliasPass = keystoreInfo.keyaliasPass;
            }
        }

        static void BuildProject(string path, BuildParameters param)
        {

            var options = new BuildPlayerOptions
            {
                scenes = ActiveScenePaths.ToArray(), 
                target = param.buildTarget.ToUnityBuildTarget(), 
                locationPathName = path,
            };

            if (!UnityEditorInternal.InternalEditorUtility.inBatchMode)
            {
                options.options |= BuildOptions.ShowBuiltPlayer;
            }

            if (param.buildAndRun)
            {
                options.options |= BuildOptions.AutoRunPlayer;
            }

            if (param.devBuild)
            {
                options.options |= BuildOptions.Development;
            }

            if (param.buildTarget == CompatibleBuildTarget.Android)
            {
                var exportType = param.androidExportType;

                EditorUserBuildSettings.buildAppBundle = false;
                if (exportType == AndroidExportType.AndroidStudioProject)
                {
                    options.options |= BuildOptions.AcceptExternalModificationsToPlayer;
                }
                else if (exportType == AndroidExportType.AppBundle)
                {
                    EditorUserBuildSettings.buildAppBundle = true;
                }
            }

            PlayerSettings.SplashScreen.show = false;
            
            var report = BuildPipeline.BuildPlayer(options);

            if (report.summary.result == BuildResult.Succeeded)  
            {
                BuildReporter.Current.IndicateSuccessfulBuild();
                AssetDatabase.SaveAssets();
            }
            else
            {
                BuildReporter.Current.Log(report.name, BuildReporter.MessageSeverity.Error);
            }
        }

		private static List<string> ActiveScenePaths
		{
            get
            {
                List<string> scenePaths = new List<string>();
                foreach (var scene in EditorBuildSettings.scenes)
                {
                    if (scene.enabled) scenePaths.Add(scene.path);
                }
                return scenePaths;
            }
		}

        public static BuildTarget ToBuildTarget (this string value)
        {
            return (BuildTarget)Enum.Parse(typeof(BuildTarget), value);
        }
		#endregion
	}
}