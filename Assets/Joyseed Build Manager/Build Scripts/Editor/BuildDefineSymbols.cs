using System;
using UnityEditor;
using System.Linq;

namespace Joyseed.Build
{
    public static class BuildDefineSymbols
    {
        private const string SBOX_DEFINE = "JOYSEED_SBOX";

        public static void SetDefineSymbolSetting(BuildParameters parameters)
        {
            var mode = parameters.buildMode;
            
            if (mode == JoyseedBuildMode.SBOX)
            {
                EnableDefineSymbol(parameters);
            }
            else
            {
                DisableDefineSymbol(parameters);
            }
        }
        
        public static void EnableDefineSymbol(BuildParameters parameters)
        {
            var target = parameters.buildTarget.ToBuildTargetGroup();

            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(target).Trim();
            var symbols = definesString.Split(';');
            
            if (!symbols.Contains(SBOX_DEFINE))
            {
                if (definesString.EndsWith(";", StringComparison.InvariantCulture) == false)
                {
                    definesString += ";";
                }

                definesString += SBOX_DEFINE;

                PlayerSettings.SetScriptingDefineSymbolsForGroup(target, definesString);
            }
        }

        public static void DisableDefineSymbol(BuildParameters parameters)
        {
            var target = parameters.buildTarget.ToBuildTargetGroup();

            var definesString = PlayerSettings.GetScriptingDefineSymbolsForGroup(target).Trim();
            var symbols = definesString.Split(';');

            if (symbols.Contains(SBOX_DEFINE))
            {
                if (definesString.Contains(SBOX_DEFINE + ";"))
                    definesString = definesString.Replace(SBOX_DEFINE + ";", "");
                else
                    definesString = definesString.Replace(SBOX_DEFINE, "");

                PlayerSettings.SetScriptingDefineSymbolsForGroup(target, definesString);
            }
        }
    }
}