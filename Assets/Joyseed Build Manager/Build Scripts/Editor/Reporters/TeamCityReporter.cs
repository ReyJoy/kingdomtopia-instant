﻿using UnityEngine;
using System.Collections;

namespace Nordeus.Build.Reporters
{
	/// <summary>
	/// Reports messages to TeamCity.
	/// </summary>
	public class TeamCityReporter : UnityReporter
	{
		protected override void LogInternal(string message, MessageSeverity severity = MessageSeverity.Info)
		{
			base.LogInternal(message, severity);

			string status = "NORMAL";
			if (severity == MessageSeverity.Error) status = "ERROR";

			string log = string.Format("##teamcity[message text='{0}' errorDetails='ERROR_DETAILS_HERE'", message);
			
			Debug.LogFormat("{0} status='{1}']\n", log, status);
		}

		public override void IndicateSuccessfulBuild()
		{
			base.IndicateSuccessfulBuild();

			// Magic string to indicate successful build.
			Debug.Log("Successful build ~0xDEADBEEF");
		}

        public override void SetEnvironmentParameter(string name, string value)
        {
            base.SetEnvironmentParameter(name, value);

            string log = string.Format("##teamcity[setParameter name='{0}' value='{1}']", name, value);

            Debug.Log(log);
        }
	}
}