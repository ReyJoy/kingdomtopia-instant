using UnityEditor;
using System;
using System.IO;
using Joyseed.CertificateCreator;

namespace Joyseed.Build
{
    public static class Utility
    {
        private const string BaseBundle = "com.joyseed";
        
        public static string GetBuildName(BuildParameters param)
        {
            return GetBuildName(param.buildTarget, param.buildNumber.ToString(), param.buildMode.ToString(), param.extraName, param.devBuild);
        }

        public static string GetBuildName(CompatibleBuildTarget target, string buildNumber, string buildMode, string extraName, bool developmentBuild)
        {
            string name = string.Format("{0}_{1}_b{2}_v{3}",
                PlayerSettings.GetApplicationIdentifier(target.ToBuildTargetGroup()).Replace(BaseBundle + ".", ""),
                DateTime.Now.ToString("yyMMdd"),
                buildNumber,
                PlayerSettings.bundleVersion);

            if (target == CompatibleBuildTarget.Android) 
                name += "_vc" + PlayerSettings.Android.bundleVersionCode.ToString();
            else if (target == CompatibleBuildTarget.iOS)
                name += "_vc" + PlayerSettings.iOS.buildNumber;

            name += string.Format("_{0}", buildMode);

            if (developmentBuild)
                name += "_DEV";
            else
                name += "_NONDEV";

            if (!string.IsNullOrEmpty(extraName))
                name += "_" + extraName;

            return name;
        }
    }
}