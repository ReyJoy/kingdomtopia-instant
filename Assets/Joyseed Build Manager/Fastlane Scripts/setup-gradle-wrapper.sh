#!/usr/bin/env bash
NAME=${1?Error: no name given}
NAME2=${2:-friend}
echo "initializing gradlewrapper at Android/$NAME/$NAME2/"
pwd
(
    cd Android/$NAME/"$NAME2"/
    
    if [ $? -eq 1 ]
    then
        exit 1
    fi
    
    gradle wrapper --gradle-version=5.1.1
)