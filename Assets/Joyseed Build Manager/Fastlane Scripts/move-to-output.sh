#!/usr/bin/env bash
ORIGIN=${1?Error: no name given}
OUTPUT_FOLDER=${2?Error: no name given}
OUTPUT_FILENAME=${3?Error: no name given}

[ -d $OUTPUT_FOLDER ] || mkdir "$OUTPUT_FOLDER"
mv $(pwd)"/""$ORIGIN" $(pwd)"/"$OUTPUT_FOLDER"/"$OUTPUT_FILENAME