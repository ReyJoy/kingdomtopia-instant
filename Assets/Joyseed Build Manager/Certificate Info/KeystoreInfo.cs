﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.CertificateCreator
{
    [System.Serializable]
    public class KeystoreInfo
    {
        public const string KEYSTORE_INFO_FILE = "Joyseed-Keystore-Info.txt";

        public string bundleID;
        public string keystoreFile;
        public string keystorePass;
        public string keyaliasName;
        public string keyaliasPass;

        public KeystoreInfo()
        {
            
        }

        public KeystoreInfo(string bundleId, string keystoreFile, string keystorePass, string keyaliasName, string keyaliasPass)
        {
            bundleID = bundleId;
            this.keystoreFile = keystoreFile;
            this.keystorePass = keystorePass;
            this.keyaliasName = keyaliasName;
            this.keyaliasPass = keyaliasPass;
        }
    }

}
