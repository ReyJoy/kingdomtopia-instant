using System.IO;
using UnityEngine;
using Joyseed.Build;
using UnityEditor;
using Joyseed.Cryptography;

namespace Joyseed.CertificateCreator
{
    public static class KeystoreInfoLoader
    {
        public static KeystoreInfoList LoadFile(string folderPath, int encryptionPIN)
        {
            var keystoreInfoFile = Path.Combine(folderPath, KeystoreInfo.KEYSTORE_INFO_FILE);
            
            if (File.Exists(keystoreInfoFile))
            {
                var loadedText = File.ReadAllText(keystoreInfoFile);
                var json = Encryption.EncryptDecrypt(loadedText, encryptionPIN);
                
                return JsonUtility.FromJson<KeystoreInfoList>(json);
            }
            else
            {
                Debug.LogError(keystoreInfoFile + " does not exist.");
                return null;
            }
        }
    }
}