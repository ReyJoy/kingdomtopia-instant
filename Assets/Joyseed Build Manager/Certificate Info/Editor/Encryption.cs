using System.Text;

namespace Joyseed.Cryptography
{
    public static class Encryption
    {
        public static string EncryptDecrypt(string text, int encryptionKey)  
        {  
            var inputSb = new StringBuilder(text);  
            var outputSb = new StringBuilder(text.Length);
            
            char _textCh;  
            for (int iCount = 0; iCount < text.Length; iCount++)  
            {  
                _textCh = inputSb[iCount];  
                _textCh = (char)(_textCh ^ encryptionKey);  
                outputSb.Append(_textCh);  
            }  
            return outputSb.ToString();  
        }  
    }
}