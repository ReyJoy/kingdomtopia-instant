namespace Joyseed.CertificateCreator
{
    [System.Serializable]
    public class KeystoreInfoList
    {
        public KeystoreInfo[] keystores;
    }
}