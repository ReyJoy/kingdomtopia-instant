using System;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [Serializable]
    public class MenuStackElement
    {
        public Transform parent;
        public List<Menu> stack;
    }
}