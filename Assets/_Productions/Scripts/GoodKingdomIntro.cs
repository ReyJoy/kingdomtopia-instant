﻿using System;
using System.Collections;
using System.Collections.Generic;
using Joyseed.IdleKingdom.UI;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace Joyseed.IdleKingdom
{
    public class GoodKingdomIntro : MonoBehaviour
    {
        public string bundleUrl;
        public AssetBundleManager bundleManager;
        public string nextSceneName;
        public Sequence sequence;
        public float cameraDuration;

        private bool finishDialog;
        private bool finishCamera;
        private bool changingScene;
        private bool sceneReady;
        
        private AsyncOperation asyncOperation;
        
        private void Start()
        {
            //asyncOperation = SceneManager.LoadSceneAsync(nextSceneName);
            //asyncOperation.allowSceneActivation = false;
            
            MapScrollingManager.Instance.CenterToCastle(true, () => finishCamera = true, cameraDuration);
            SequenceSystem.Instance.RunSequence(sequence, () => finishDialog = true);
            StartCoroutine(LoadAssetBundleFromWeb());
        }

        private void Update()
        {
            if (!changingScene && finishCamera && finishDialog && sceneReady)
            {
                changingScene = true;
                BlockerHUD.Instance.Show(BlockerType.Fade, ChangeScene);
            }
        }

        private void ChangeScene()
        {
            bundleManager.LoadScene("instant", 0);
        }
        
        private IEnumerator LoadAssetBundleFromWeb()
        {
            foreach (var assetPackage in bundleManager.assetPackages)
            {
                Debug.Log($"Loading AssetBundle {assetPackage.bundleName}");
                var webAssetUrl = $"{bundleUrl}{assetPackage.bundleName}";
                
                using (var www = UnityWebRequestAssetBundle.GetAssetBundle(webAssetUrl))
                {
                    var op = www.SendWebRequest();

                    while (!op.isDone)
                    {
                        //loadingBar.fillAmount = www.downloadProgress;
                        //progress.text = $"{www.downloadProgress * 100}%";
                        yield return null;
                    }
                    
                    assetPackage.assetBundle = DownloadHandlerAssetBundle.GetContent(www);
                    
                    if (assetPackage.assetBundle == null)
                    {
                        Debug.LogError($"Failed to load AssetBundle for {assetPackage.bundleName}");
                        yield break;
                    }

                    Debug.Log($"Success loading AssetBundle for {assetPackage.bundleName}");
                    sceneReady = true;
                }
            }
            //loading.SetActive(false);
            //loadFinished.Raise();
        }
    }
}