﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bedivere.AnimatorStateMachine
{
    public interface IBDVStateBehaviour<TStateMachine>
    {
        void Construct(TStateMachine stateMachine, Animator animator);
        void Enable();
        void Disable();
    }
}
