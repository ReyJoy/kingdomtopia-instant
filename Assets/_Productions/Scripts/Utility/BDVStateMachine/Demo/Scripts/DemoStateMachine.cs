﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bedivere.AnimatorStateMachine.Demo
{
    public class DemoStateMachine : BDVStateMachine
    {
        public int ctr;

        void Start()
        {            
            this.ConstructAllStates(Animator);
        }
    }
}
