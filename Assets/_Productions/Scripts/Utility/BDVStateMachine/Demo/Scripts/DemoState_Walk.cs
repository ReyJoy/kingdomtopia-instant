﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bedivere.AnimatorStateMachine.Demo
{
    public class DemoState_Walk : BDVStateMachineBehaviour<DemoStateMachine>
    {
        protected override void OnUpdate()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                stateMachine.ctr++;
                Debug.Log(stateMachine.ctr);
                animator.SetTrigger("action");
            }
        }
    }
}
