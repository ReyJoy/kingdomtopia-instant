﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bedivere.AnimatorStateMachine
{
    public static class BDVStateMachineExtensions
    {    
        public static IEnumerable<IBDVStateBehaviour<TStateMachine>> GetStates<TStateMachine>(this TStateMachine stateMachine, Animator animator)
        {
            var states = animator.GetBehaviours<StateMachineBehaviour>();
            //Debug.Log($"{animator.name}, typeT {typeof(TStateMachine)}");
            
            for (int i = 0; i < states.Length; i++)
            {
                var state = states[i] as IBDVStateBehaviour<TStateMachine>;
//                Debug.Log($"{states[i]} - type {states[i].GetType()} - {state}");
                if (state == null) continue;
                yield return state;
            }
        }

        public static void ConstructAllStates<TStateMachine>(this TStateMachine stateMachine, Animator animator)
        {
            foreach(IBDVStateBehaviour<TStateMachine> state in stateMachine.GetStates(animator))
            {
                state.Construct(stateMachine, animator);
            }
        }

        public static void DisableAllStates<TStateMachine>(this TStateMachine stateMachine, Animator animator)
        {
            foreach(IBDVStateBehaviour<TStateMachine> state in stateMachine.GetStates(animator))
            {
                state.Disable();
            }
        }
    }
}
