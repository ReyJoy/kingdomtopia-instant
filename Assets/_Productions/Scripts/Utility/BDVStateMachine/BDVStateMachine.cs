﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bedivere.AnimatorStateMachine
{
    [RequireComponent(typeof(Animator))]
    public class BDVStateMachine : MonoBehaviour
    {
        void OnDisable()
        {
            this.DisableAllStates(Animator);
        }

        [SerializeField] private Animator animator;
        public Animator Animator 
        {
            get 
            {
                if (animator == null) 
                {
                    animator = this.GetComponent<Animator>();
                }

                return animator;
            }
        }
    }
}
