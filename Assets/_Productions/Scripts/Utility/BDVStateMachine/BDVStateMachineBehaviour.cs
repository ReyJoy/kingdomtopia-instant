﻿using UnityEngine;

namespace Bedivere.AnimatorStateMachine
{
    public class BDVStateMachineBehaviour<TStateMachine> : StateMachineBehaviour, IBDVStateBehaviour<TStateMachine>
    {
        protected Animator animator; 
        private bool isActive;
        protected TStateMachine stateMachine;

        public void Test()
        {
            Debug.Log($"{this.GetType()}, TStateMachine {typeof(TStateMachine)}");
        }

        public void Construct(TStateMachine stateMachine, Animator animator)
        {
            this.stateMachine = stateMachine;
            this.animator = animator;

            //Debug.Log($"{this.name} Construct StateMachine, animator : {animator}");
        }

        public void Enable()
        {

        }

        public void Disable()
        {
            if (isActive)
            {
                OnExit();
                isActive = false;
            }
        }

        #region StateMachineBehaivour overrides
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //Debug.Log($"{this.GetType()} OnStateEnter, isActive : {isActive}");
            if (isActive) {
                return;
            }

            isActive = true;
            OnEnter();
        }
        
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //Debug.Log($"{this.GetType()} OnStateExit, isActive : {isActive}");
            if (!isActive) {
                return;
            }
            
            isActive = false;
            OnExit();
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            // Debug.Log($"{this.GetType()} OnStateUpdate, isActive : {isActive}");
            if (isActive) {
                OnUpdate();
            }
        }
        #endregion

        #region virtual methods
		protected virtual void OnEnter() {
			// stub
		}

		protected virtual void OnExit() {
			// stub
		}

		protected virtual void OnUpdate() {
			// stub
		}
        #endregion
    }
}
