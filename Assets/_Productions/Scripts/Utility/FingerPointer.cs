﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class FingerPointer : Singleton<FingerPointer>
    {
        public Transform finger;
        public Canvas canvas;

        protected override void Awake()
        {
            base.Awake();
            //Reset();
        }

        public void Place(Transform target, string layerName = "Tutorial", int layerOrder = 100, bool rotate = false)
        {
            finger.gameObject.SetActive(true);
            finger.position = target.position;
            if (rotate)
            {
                finger.Rotate(0, 0, 180);
            }
            else
            {
                finger.rotation = Quaternion.identity;
            }
            finger.SetParent(target);

            if (canvas.rootCanvas.renderMode == RenderMode.ScreenSpaceCamera)
            {
                canvas.overrideSorting = true;
                canvas.sortingOrder = layerOrder;
                canvas.sortingLayerName =layerName;
            }
        }

        public void Hide()
        {
            canvas.gameObject.SetActive(false);
        }

        public void Reset()
        {
            finger.SetParent(Instance.transform);
            finger.gameObject.SetActive(false);

            canvas.gameObject.layer = LayerMask.NameToLayer("Tutorial Overlay");
        }
    }
}