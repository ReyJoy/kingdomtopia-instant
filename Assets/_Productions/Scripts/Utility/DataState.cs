﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Joyseed.IdleKingdom
{
    public abstract class DataState<T> : DataState, IDataState where T : DataState<T>
    {
        [ReadOnly] public string ID;
        private string _typeString;

        public void Initialize(string Id)
        {
            SetID(Id);

            InitValues();
        }

        public void SetID(string Id)
        {
            this.ID = string.IsNullOrEmpty(Id) ? TypeString : Id;
        }

        public string TypeString
        {
            get 
            {
                if (string.IsNullOrEmpty(_typeString)) {
                    _typeString = this.GetType().Name;
                }

                return _typeString;
            }
        }


        public abstract void InitValues();
    }

    public class DataState
    {
        
    }

    public interface IDataState
    {
        void Initialize(string ID);
    }
}