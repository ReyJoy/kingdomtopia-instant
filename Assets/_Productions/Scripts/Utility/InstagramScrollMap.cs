﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Joyseed.IdleKingdom.UI;

namespace Joyseed.IdleKingdom
{
    public class InstagramScrollMap : MonoBehaviour, IScrollMap
    {
        public Transform mapObject, refObject;
        public ScrollRect scrollRect;

        public float[] districtScrollY;

        private Action tweenCallback;
        
        void Update()
        {
//            mapObject.position = new Vector3(mapObject.position.x, refObject.position.y, mapObject.position.z);
        }

        public void Scroll(float vertPos, float time, Action callback)
        {
            BlockerHUD.Instance.Show(BlockerType.Invisible);
            scrollRect.DOVerticalNormalizedPos(vertPos, time).SetEase(Ease.InOutQuad).OnComplete(() =>
            {
                BlockerHUD.Instance.Hide();
                if (callback != null) callback();
            });
        }

        public void DebugLogPos(Vector2 pos)
        {
//            Debug.Log(pos.y);
        }

    }
}