﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    [RequireComponent(typeof(Button))]
    public class ButtonSfx : MonoBehaviour
    {
        public string audioKey;
        private Button button;

        private void Awake()
        {
            button = GetComponent<Button>();
        }

        private void OnEnable()
        {
            button.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ => OnButtonClicked());
        }

        private void OnButtonClicked()
        {
            AudioController.Instance.PlayAudioClip(audioKey);
        }
    }
}