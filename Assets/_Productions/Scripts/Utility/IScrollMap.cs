﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public interface IScrollMap
    {
        void Scroll(float vertPos, float time, Action callback);
    }
}