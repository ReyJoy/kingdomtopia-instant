﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class MapScrollingManager : Singleton<MapScrollingManager>
    {
        private InstagramScrollMap scrollMap;
        public float[] districtScrollY;

        protected override void Awake()
        {
            base.Awake();
            scrollMap = GetComponentInChildren<InstagramScrollMap>();
        }

        public IObservable<Unit> CenterToCastleStream(bool isFromBottom)
        {
            return Observable.Create<Unit>(observer =>
            {
                CenterToCastle(isFromBottom, delegate()
                {
                    observer.OnNext(Unit.Default);
                    observer.OnCompleted();
                });
                
                return Disposable.Empty;
            });
        }

        public void CenterToCastle(bool isFromBottom, Action callback, float duration = 2f)
        {
            if (isFromBottom)
            {
                ScrollToBottom(delegate
                {
                    scrollMap.Scroll(1f, duration, callback);
                });
            }
        }

        public void ScrollToBottom(Action callback = null)
        {
            scrollMap.Scroll(0f, 0f, callback);
        }

        public void CenterToDistrict(int index, float time, Action callback)
        {
            scrollMap.Scroll(districtScrollY[index], time, callback);
        }

        public void EnableScroll(bool enable)
        {
            scrollMap.scrollRect.enabled = enable;
        }
    }
}