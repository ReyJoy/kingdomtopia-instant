﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class AudioController : Singleton<AudioController>
    {
        public GameObject audioObject;
        public List<AudioData> database = new List<AudioData>();

        public void PlayAudioClip(string key)
        {
            var audio = Instantiate(audioObject, Vector3.zero, Quaternion.identity).GetComponent<AudioSource>();
            var clip = GetAudioClipByKey(key);
            audio.clip = clip;
            audio.Play();

            Destroy(audio.gameObject, clip.length);
        }
        
        private AudioClip GetAudioClipByKey(string key)
        {
            AudioClip clip = null;
            foreach (var data in database)
            {
                if (data.key == key)
                {
                    clip = data.clip;
                }
            }

            return clip;
        }
    }

    [System.Serializable]
    public class AudioData
    {
        public string key;
        public AudioClip clip;
    }

}