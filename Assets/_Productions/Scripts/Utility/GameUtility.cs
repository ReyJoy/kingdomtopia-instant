﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public static class GameUtility
    {
        public static string GetShortStringTime(float time)
        {
            int hour = (int)(time / 3600);
            int minutes = (int) ((time % 3600) / 60);
            int seconds = (int) (time % 60);
            
            return string.Format("{0:00}:{1:00}:{2:00}", hour, minutes, seconds);
        }

        public static string ToShortTimeString(this float time)
        {
            var span = TimeSpan.FromSeconds(time);
            return ToShortTimeString(span);
        }

        public static string ToShortTimeString(this TimeSpan span)
        {
            int sec = span.Seconds;
            int min = span.Minutes;
            int hour = (int)span.TotalHours;

            if (span.TotalSeconds < 60)
            {
                return $"{sec}s";
            }
            else if (span.TotalMinutes < 60)
            {
                return $"{min}m {sec}s";
            }
            else
            {
                return $"{hour}h {min}m";
            }
        }

        static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static int GetTimeSpanFromStart(DateTime now)
        {
            int currentEpochTime = (int) (now - epoch).TotalSeconds;
            

            return currentEpochTime;
        }

        public static int ToEpoch(this DateTime time)
        {
            return GetTimeSpanFromStart(time);
        }

        public static DateTime GetTimeFromEpoch(int number)
        {
            return epoch.AddSeconds(number);
        }

        public static void ClampLimit(this double number)
        {
            if (double.IsNaN(number))
            {
                number = 0;
            }
            else if (double.IsPositiveInfinity(number))
            {
                number = double.MaxValue;
            }
            else if (double.IsNegativeInfinity(number))
            {
                number = double.MinValue;
            }
        }

        public static double Clamp(double value, double min, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;  
        }

        public static string GetCurrencyAnnotation(int exp)
        {
            string[] preset = new string[] {"", "K", "M", "B", "T"};
            
            int order = exp / 3;

            if (order < 5) {
                return preset[order];
            }
            else
            {
                int startLeft = 65;
                int startRight = 97;

                int leftChar = startLeft + ((order - 5) / 26);
                int rightChar = startRight + ((order - 5) % 26);
                
//                Debug.Log("<color=blue>CURRENCY : " + ((char)leftChar).ToString() + ((char)rightChar) + "</color>");
                return ((char)leftChar).ToString() + ((char)rightChar);
            }
        }

        public static void FadeImage(Image image, bool isFadeIn, bool isImmediately)
        {
            Color color = new Color(0, 0, 0, 0);

            if (isFadeIn) {
                color = new Color(0, 0, 0, 200f/255f);
            }

            var time = isImmediately ? 0f : 0.3f;
            image.DOColor(color, time);
        }

        public static void ChangeLayerRecursively(this Transform transform, string layerName)
        {
            var layer = LayerMask.NameToLayer(layerName);

            foreach (Transform child in transform)
            {
                Debug.Log(child.name);
                child.gameObject.layer = layer;
            }
        }
    }
}