﻿using System;
using System.Collections;
using System.Collections.Generic;
using Joyseed.IdleKingdom;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public static class CurrencyUtility
    {
        public static string ToAnnotated(this double value)
        {
            if (value < 0.5)
            {
                return "0";
            }
            else if (value < 1000)
            {
                return $"{value:0.##}";
            }
            else if (value <= 999999)
            {
                return $"{value:n0}";
            }
            else // only annotate for value above 999999
            {
                var exp = GetExponent(value);
                double significant = GetSignificant(value, exp);
                string annotation = GetAnnotation(exp);
                
                return $"{significant:0.##} {annotation}";
            }
        }

        private static double GetExponent(double value)
        {
            if (value == 0.0)
                return 0;

            return Math.Floor(Math.Log10(Math.Abs(value)));
        }

        private static double GetSignificant(double value, double exponent)
        {
            var adjustedExponent = exponent - (exponent % 3);

            return value / Math.Pow(10, adjustedExponent);
        }

        static string[] annotations = new string[] {"", "K", "M", "B", "T"};
        private static string GetAnnotation(double exponent)
        {
            if (exponent < 0)
            {
                Debug.LogError("Negative exponent is not supported");
                return "";
            }
            
            var thousands = (int) Math.Floor(exponent / 3);

            if (thousands < annotations.Length)
            {
                return annotations[thousands];
            }
            else
            {
                var annotIndex = thousands - annotations.Length;
                var leftChar = NumberToAlphabet(annotIndex / 26, true);
                var rightChar = NumberToAlphabet(annotIndex % 26, false);

                return $"{leftChar}{rightChar}";
            }
        }

        private static string NumberToAlphabet(int number, bool isCaps)
        {
            if (number > 25)
            {
                Debug.LogError("No value beyond 26 characters.");
                return "";
            }

            char c = (char) ((isCaps ? 65 : 97) + (number));
            return c.ToString();
        }
    }
}
