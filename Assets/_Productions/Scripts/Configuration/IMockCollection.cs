﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public interface IMockCollection
    {
        IDataModel[] Rows { get; }
    }
}

