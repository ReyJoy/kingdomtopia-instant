﻿using UniRx;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class PlayerJourneyState : DataState<PlayerJourneyState>
    {
        public DoubleReactiveProperty rebuildCount = new DoubleReactiveProperty(0);
        public DoubleReactiveProperty lifetimeRevenue = new DoubleReactiveProperty(0);
        public BoolReactiveProperty isIntroCompleted = new BoolReactiveProperty(false);
        
        public PlayerJourneyState()
        {
            rebuildCount = new DoubleReactiveProperty();
            lifetimeRevenue = new DoubleReactiveProperty();
            isIntroCompleted = new BoolReactiveProperty();
        }

        public override void InitValues()
        {

        }
    }
}
