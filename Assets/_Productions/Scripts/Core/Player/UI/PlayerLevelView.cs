﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class PlayerLevelView : MonoBehaviour
    {
        public TextMeshProUGUI levelText;
        public TextMeshProUGUI progressText;
        public Image progressFill;
        public GameObject notification;
        public Button button;
        public Color notCompletedColor;
        public Color completedColor;
        public Color targetColor;

        private void Awake()
        {
            notification.SetActive(false);
        }

        private void OnEnable()
        {
            PlayerLevel.Instance.Level.TakeUntilDisable(this).Subscribe(UpdateLevel);
            PlayerLevel.Instance.progressPercentage.TakeUntilDisable(this).Subscribe(UpdateProgress);
            PlayerLevel.Instance.CanLevelUp.TakeUntilDisable(this).Subscribe(ShowNotification);
            button.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ => ChapterMenu.Open());
        }
        
        private void UpdateLevel(int level)
        {
            levelText.text = string.Format("To Unlock Next Chapter", level + 1);
        }

        private void UpdateProgress(double progress)
        {
            progressFill.fillAmount = (float)progress;

            var rev = PlayerJourney.Instance.lifetimeRevenue.Value;
            var next = PlayerLevel.Instance.LifetimeRevenueForNextLevel;

            var progressString = "";
            if (rev < next)
            {
                progressString += $"<color=#{ColorUtility.ToHtmlStringRGB(notCompletedColor)}>";
            }
            else
            {
                progressString += $"<color=#{ColorUtility.ToHtmlStringRGB(completedColor)}>";
            }
            
            progressString += $"{rev.ToAnnotated()}</color>/<color=#{ColorUtility.ToHtmlStringRGB(targetColor)}>{next.ToAnnotated()}</color>";

            progressText.text = progressString;
        }

        private void ShowNotification(bool canLevelUp)
        {
            notification.SetActive(canLevelUp);
        }
    }
}