﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Joyseed.IdleKingdom
{
    public class PlayerLevelConfig : ScriptableObject, IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public double costBase;
        [ReadOnly] public double costExponent;
        [ReadOnly] public string castleID;
        [ReadOnly] public int coinReward;
        [ReadOnly] public int gemReward;

        public string dataID { get {return Id; }}

        private double revenueCost = -1;
        public double RevenueCost 
        { 
            get 
            {
                if (revenueCost < 0) {
                    revenueCost = costBase * Math.Pow(10, costExponent);
                }

                return revenueCost;
            }
        }
    }

    [System.Serializable]
    public class PlayerLevelConfigMock : IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public double costBase;
        [ReadOnly] public double costExponent;
        [ReadOnly] public string castleID;
        [ReadOnly] public int coinReward;
        [ReadOnly] public int gemReward;

        public string dataID { get {return Id; }}
    }

    public struct PlayerLevelScriptables : IMockCollection
    {
        public PlayerLevelConfigMock[] PlayerLevelConfigRow;
        public IDataModel[] Rows { get { return PlayerLevelConfigRow; }}
    }
}