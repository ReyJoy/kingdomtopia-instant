﻿using UnityEngine;
using UniRx;
using System;
using Sirenix.OdinInspector;

namespace Joyseed.IdleKingdom
{
    public class PlayerJourney : Singleton<PlayerJourney>
    {
        [Title ("Parameters")]
        public PlayerJourneyState state;
        
        public DoubleReactiveProperty lifetimeRevenue => state.lifetimeRevenue;

        private Inventory inventory;

        private double _tmpValue;
        
        public void Start()
        {
            inventory = Inventory.Instance;
            SubscribeListener();
        }

        private void SubscribeListener()
        {
            inventory.OnGive().TakeUntilDisable(this).Subscribe(OnGiveTransaction);
        }

        void OnGiveTransaction(Transaction transaction)
        {
            if (transaction.type == TransactionType.Coins)
            {
                _tmpValue = lifetimeRevenue.Value + transaction.amount;
                _tmpValue.ClampLimit();

                lifetimeRevenue.Value = _tmpValue;
            }
        }
    }
}