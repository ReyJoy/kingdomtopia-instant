﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class PlayerProgressionEditor : Singleton<PlayerProgressionEditor>
    {
        [TableList(AlwaysExpanded = true, ShowIndexLabels = true)]
        public List<PlayerProgression> playerProgression = new List<PlayerProgression>();

        public AnimationCurve progressionCurve;

        [Button(ButtonSizes.Large)]
        private void RepaintCurve()
        {
            progressionCurve = new AnimationCurve();
            for (int i = 1; i < playerProgression.Count; i++)
            {
                progressionCurve.AddKey(i, playerProgression[i].power);
            }
        }
    }

    [System.Serializable]
    public class PlayerProgression
    {
        [PreviewField(ObjectFieldAlignment.Center)]
        public Sprite icon;
        public int cost;
        public int power;
        public string chapterDescription;
        public Sequence reward;
    }
}