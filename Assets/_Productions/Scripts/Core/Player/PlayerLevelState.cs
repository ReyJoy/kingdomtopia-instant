﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class PlayerLevelState : DataState<PlayerLevelState>
    {
        public IntReactiveProperty level;

        public PlayerLevelState()
        {
            level = new IntReactiveProperty(1);
        }
        public override void InitValues()
        {
            
        }
    }
}