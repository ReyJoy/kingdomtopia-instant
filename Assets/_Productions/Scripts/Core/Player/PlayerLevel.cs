﻿using System;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class PlayerLevel : Singleton<PlayerLevel>
    {
        public PlayerLevelState state;
        public AnimationCurve curve;
        
        private PlayerJourney playerJourney;

        public IntReactiveProperty Level => state.level;
        public bool IsMax => Level.Value >= PlayerProgressionEditor.Instance.playerProgression.Count - 1;
        
        [ReadOnly]
        public BoolReactiveProperty CanLevelUp = new BoolReactiveProperty(false);
        
        [ReadOnly, ShowInInspector]
        public double LifetimeRevenueCurrentLevel
        {
            get
            {
                if (Level.Value <= 1) return 0;
                var currentLevel = PlayerProgressionEditor.Instance.playerProgression[Level.Value];
                var target = currentLevel.cost * Math.Pow(10, currentLevel.power);
                return target;
            }
        }
        
        [ReadOnly, ShowInInspector]
        public double LifetimeRevenueForNextLevel
        {
            get
            {
                if (IsMax) return double.MaxValue;
                var nextLevel = PlayerProgressionEditor.Instance.playerProgression[Level.Value + 1];
                var target = nextLevel.cost * Math.Pow(10, nextLevel.power);
                return target;
            }
        }
        
        [ReadOnly]
        public DoubleReactiveProperty progressPercentage = new DoubleReactiveProperty(0);

        public void OnEnable()
        {
            playerJourney = PlayerJourney.Instance;
            Initialize();
        }

        public void Initialize()
        {
            playerJourney.lifetimeRevenue.TakeUntilDestroy(this).Subscribe(_ => UpdateProgress());
            Level.TakeUntilDestroy(this).Subscribe(_ => UpdateProgress());
        }

        public void NextStory()
        {
            if (Level.Value + 1 >= PlayerProgressionEditor.Instance.playerProgression.Count)
            {
                return;
            }
            PlayerProgressionEditor.Instance.playerProgression[Level.Value + 1].reward.RunSequence();
            //Blackboard.Persistence.isSaveAllowed = false;
        }

        /*
        public void LevelUp()
        {
            if (Level.Value + 1 >= PlayerProgressionEditor.Instance.playerProgression.Count)
            {
                return;
            }

            Blackboard.Persistence.isSaveAllowed = true;
            AppState.Instance.SaveAll();
            Level.Value++;
            AnalyticsManager.Instance.SendProgression(Level.Value);
            ChapterTargetMenu.Open();
        }

        public void LevelDown()
        {
            Level.Value--;
        }*/

        private void UpdateProgress()
        {
            var lifetimeRevenue = PlayerJourney.Instance.lifetimeRevenue.Value;
            progressPercentage.Value = (lifetimeRevenue - LifetimeRevenueCurrentLevel) / (LifetimeRevenueForNextLevel - LifetimeRevenueCurrentLevel);
            CanLevelUp.Value = lifetimeRevenue >= LifetimeRevenueForNextLevel;
        }
    }
}