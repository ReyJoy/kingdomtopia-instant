﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class DialogueView : MonoBehaviour
    {
        public Image dialogueSprite;
        public TextMeshProUGUI dialogueTitle;
        public TextMeshProUGUI dialogueText;
        public Button dialogueButton;
        public Animator animator;
        public BoolReactiveProperty carretActive;

        private int maxVisibleChar = 0;
        private Action onComplete;

        public void DoRunningText(string text, float speed, Action onComplete)
        {
            dialogueText.text = text;
            maxVisibleChar = 0;
            dialogueText.maxVisibleCharacters = maxVisibleChar;
            this.onComplete = onComplete;
            
            InvokeRepeating("Running", 0,.05f);
        }

        void Running()
        {
            maxVisibleChar += 1;
            dialogueText.maxVisibleCharacters = maxVisibleChar;
            if (maxVisibleChar == dialogueText.text.Length)
            {
                onComplete?.Invoke();
                CancelInvoke("Running");
            }
        }

        public void StopRunning()
        {
            CancelInvoke("Running");
        }
    }
}