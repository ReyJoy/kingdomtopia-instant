﻿using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    [CreateAssetMenu(menuName = "Joyseed/Actor", fileName = "Actor.asset", order = 1)]
    public class Actor : ScriptableObject
    {
        public string name;
        public Sprite icon;
    }
}