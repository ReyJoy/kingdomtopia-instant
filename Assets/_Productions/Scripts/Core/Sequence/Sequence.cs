﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [CreateAssetMenu(menuName = "Joyseed/Sequence", fileName = "Sequence", order = 0)]
    public class Sequence : ScriptableObject
    {
        [GUIColor(0, 1, 0)]
        [DisableInEditorMode]
        [ResponsiveButtonGroup("Debug")]
        public void RunSequence()
        {
            SequenceSystem.Instance.RunSequence(this);
        }
        
        public void RunSequence(Action callback)
        {
            SequenceSystem.Instance.RunSequence(this, callback);
        }
        
        
        [GUIColor(0, 1, 0)]
        [DisableInEditorMode]
        [ResponsiveButtonGroup("Debug")]
        private void NextSequence()
        {
            SequenceSystem.Instance.NextSequence();
        }
        
        [TableList(DrawScrollView = false, ShowIndexLabels = true)]
        public List<SequenceData> sequences = new List<SequenceData>();
    }
}