﻿using System;
using System.Collections;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class SequenceSystem : Singleton<SequenceSystem>
    {
        [TabGroup("Dialogue")]
        public DialogueView dialogueRight;
        [TabGroup("Dialogue")]
        public DialogueView dialogueLeft;
        [TabGroup("Dialogue")]
        public Image backgroundRight;
        [TabGroup("Dialogue")]
        public Image backgroundLeft;
        [TabGroup("Dialogue")]
        public bool tapToNext;
        [TabGroup("Dialogue")]
        public string nextSfx;

        [TabGroup("Fade")]
        public Image fadeImage;

        [TabGroup("Show GameObject")]
        public Transform parentSpawn;
        
        [BoxGroup("Debugging Options")]
        [DisableInEditorMode] [ShowInInspector]
        private Sequence currentSequence;
        private int iSequence = 0;
        private Action onComplete;
        private bool dialogueFinishPlaying = false;
        private static readonly int Active = Animator.StringToHash("Active");
        private GameObject lastGameObject;

        private void OnEnable()
        {
            dialogueLeft.dialogueButton.onClick.AddListener(FinishDialogueSequence);
            dialogueRight.dialogueButton.onClick.AddListener(FinishDialogueSequence);
        }

        private void OnDisable()
        {
            dialogueLeft.dialogueButton.onClick.RemoveListener(FinishDialogueSequence);
            dialogueRight.dialogueButton.onClick.RemoveListener(FinishDialogueSequence);
        }

        public void RunSequence(Sequence sequence, Action onComplete)
        {
            currentSequence = sequence;
            this.onComplete = onComplete;

            RunSequence();
        }

        public void RunSequence(Sequence sequence)
        {
            onComplete = null;
            currentSequence = sequence;
            RunSequence();
        }
        
        [BoxGroup("Debugging Options")]
        [DisableInEditorMode]
        [Button(ButtonSizes.Medium)] [GUIColor(0, 1, 0)]
        private void RunSequence()
        {
            if (currentSequence.sequences.Count <= 0)
            {
                Debug.Log("Sequence is empty");
                return;
            }

            if (iSequence >= currentSequence.sequences.Count)
            {
                FinishSequence();
                return;
            }
            
            CheckSequenceType();
        }
        
        public void NextSequence()
        {
            if (currentSequence == null) return;
            if (currentSequence.sequences[iSequence].type == SequenceType.ShowGameObject)
            {
                if (lastGameObject != null)
                {
                    Destroy(lastGameObject);
                }
            }
            
            iSequence++;
            RunSequence();
        }

        private void FinishSequence()
        {
            //Debug.Log("Sequence Ended");
            iSequence = 0;
            currentSequence = null;
            onComplete?.Invoke();
        }

        private void CheckSequenceType()
        {
            var activeSequence = currentSequence.sequences[iSequence];
            switch (activeSequence.type)
            {
                case SequenceType.Dialog:
                    RunDialogSequence(activeSequence.dialogue);
                    break;
                case SequenceType.Fade:
                    RunFadeSequence(activeSequence.fade);
                    break;
                case SequenceType.CameraEvent:
                    RunCameraSequence(activeSequence.camera);
                    break;
                case SequenceType.ShowGameObject:
                    RunShowGameObjectSequence(activeSequence.showGameObject);
                    break;
                case SequenceType.Wait:
                    RunWaitObjectSequence(activeSequence.wait);
                    break;
                case SequenceType.Response:
                    RunResponseEventSequence(activeSequence.response);
                    break;
                case SequenceType.PlaySfx:
                    RunPlaySfxSequence(activeSequence.sfx);
                    break;
                case SequenceType.Finger:
                    RunFingerSequence(activeSequence.finger);
                    break;
            }
        }

        private void RunDialogSequence(SequenceDialog dialogue)
        {
            dialogueFinishPlaying = false;
            var view = dialogueLeft;

            backgroundRight.enabled = dialogue.useBackground;
            backgroundLeft.enabled = dialogue.useBackground;
            
            switch (dialogue.alignment)
            {
                case DialoguePortraitAlignment.Left:
                    view = dialogueLeft;
                    break;
                case DialoguePortraitAlignment.Right:
                    view = dialogueRight;
                    break;
            }
            
            view.gameObject.SetActive(true);
            view.animator.SetBool(Active, false);
            view.dialogueTitle.text = dialogue.actor.name;
            view.dialogueText.text = dialogue.text;
            view.dialogueText.maxVisibleCharacters = 0;
            view.DoRunningText(dialogue.text, dialogue.speed, delegate
            {
                dialogueFinishPlaying = true;
                view.animator.SetBool(Active, true);
            });
            
            var portraitSprite = dialogue.actor.icon;
            view.dialogueSprite.sprite = portraitSprite;
        }
        
        private void FinishDialogueSequence()
        {
            if (!dialogueFinishPlaying)
            {
                ClearDialogue();
            }
            else
            {
                dialogueLeft.gameObject.SetActive(false);
                dialogueRight.gameObject.SetActive(false);
                AudioController.Instance.PlayAudioClip(nextSfx);
                NextSequence();
            }
        }

        private void ClearDialogue()
        {
            dialogueLeft.StopRunning();
            dialogueRight.StopRunning();

            dialogueLeft.animator.SetBool(Active, true);
            dialogueRight.animator.SetBool(Active, true);

            dialogueLeft.dialogueText.maxVisibleCharacters = 9999;
            dialogueRight.dialogueText.maxVisibleCharacters = 9999;
            
            dialogueFinishPlaying = true;
        }
        
        private void RunFadeSequence(SequenceFade fade)
        {
            if (fade.fadeType == FadeType.FadeIn)
            {
                fadeImage.enabled = true;
                fadeImage.raycastTarget = true;
                fadeImage.color = new Color(fade.fadeColor.r, fade.fadeColor.g, fade.fadeColor.b, 0);
                fadeImage.DOFade(1, fade.duration).OnComplete(FinishFadeSequence);
            }
            else if (fade.fadeType == FadeType.FadeOut)
            {
                fadeImage.enabled = true;
                fadeImage.color = fade.fadeColor;
                fadeImage.raycastTarget = true;
                fadeImage.DOFade(0, fade.duration).OnComplete(delegate
                {
                    fadeImage.raycastTarget = false;
                    FinishFadeSequence();
                });
            }
        }

        private void FinishFadeSequence()
        {
            fadeImage.enabled = false;
            NextSequence();
        }

        private void RunCameraSequence(SequenceCamera camera)
        {
            var targetIndex = 0;
            var duration = camera.duration;
            switch (camera.cameraTarget)
            {
                case CameraTarget.Castle:
                    MapScrollingManager.Instance.CenterToCastle(camera.isFromBottom, NextSequence, duration);
                    break;
                case CameraTarget.District1:
                    targetIndex = 0;
                    break;
                case CameraTarget.District2:
                    targetIndex = 1;
                    break;
                case CameraTarget.District3:
                    targetIndex = 2;
                    break;
                case CameraTarget.District4:
                    targetIndex = 3;
                    break;
                case CameraTarget.District5:
                    targetIndex = 4;
                    break;
                case CameraTarget.District6:
                    targetIndex = 5;
                    break;
                case CameraTarget.District7:
                    targetIndex = 6;
                    break;
                case CameraTarget.District8:
                    targetIndex = 7;
                    break;
                case CameraTarget.District9:
                    targetIndex = 8;
                    break;
                case CameraTarget.District10:
                    targetIndex = 9;
                    break;
                default:
                    targetIndex = 0;
                    break;
            }

            if (camera.cameraTarget != CameraTarget.Castle)
            {
                MapScrollingManager.Instance.CenterToDistrict(targetIndex, duration, NextSequence);
            }
        }

        private void RunShowGameObjectSequence(SequenceGameObject go)
        {
            if (go.prefab == null)
            {
                NextSequence();
                return;
            }

            if (go.waitForNextSequence)
            {
                lastGameObject = Instantiate(go.prefab, parentSpawn);
            }
            else
            {
                Instantiate(go.prefab, parentSpawn);
                NextSequence();
            }
            
            if (go.useDuration)
            {
                StartCoroutine(FinishShowGameObjectSequence(go.duration));
            }
        }

        IEnumerator FinishShowGameObjectSequence(float delay)
        {
            yield return new WaitForSeconds(delay);
            Destroy(lastGameObject);
            NextSequence();
        }
        
        private void RunWaitObjectSequence(SequenceWait wait)
        {
            if (wait.useDuration)
            {
                StartCoroutine(FinishWaitSequence(wait.duration));
            }
        }

        IEnumerator FinishWaitSequence(float duration)
        {
            yield return new WaitForSeconds(duration);
            NextSequence();
        }

        private void RunResponseEventSequence(SequenceResponse activeSequenceResponse)
        {
            activeSequenceResponse.response.Invoke();
            NextSequence();
        }
        
        private void RunPlaySfxSequence(SequenceSfx sfx)
        {
            AudioController.Instance.PlayAudioClip(sfx.sfxName);
            NextSequence();
        }

        private void RunFingerSequence(SequenceFinger finger)
        {
            FingerPointer.Instance.Place(finger.target, finger.layerName, finger.layerOrder);
            NextSequence();
        }
    }
}