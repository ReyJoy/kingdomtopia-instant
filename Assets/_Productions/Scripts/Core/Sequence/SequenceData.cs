﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class SequenceData
    {
        [TableColumnWidth(100, Resizable = false)]
        public SequenceType type;

        [ShowIf("type", SequenceType.Dialog)]
        [InfoBox("@this.dialogue.text", InfoMessageType.None)]
        [BoxGroup("Settings"), HideLabel]
        public SequenceDialog dialogue;

        [ShowIf("type", SequenceType.Fade)]
        [BoxGroup("Settings"), HideLabel]
        public SequenceFade fade;

        [ShowIf("type", SequenceType.CameraEvent)]
        [BoxGroup("Settings"), HideLabel]
        public SequenceCamera camera;

        [ShowIf("type", SequenceType.ShowGameObject)]
        [BoxGroup("Settings"), HideLabel]
        public SequenceGameObject showGameObject;

        [ShowIf("type", SequenceType.Wait)] 
        [BoxGroup("Settings"), HideLabel]
        public SequenceWait wait;

        [ShowIf("type", SequenceType.Response)] 
        [BoxGroup("Settings"), HideLabel]
        public SequenceResponse response;
        
        [ShowIf("type", SequenceType.PlaySfx)] 
        [BoxGroup("Settings"), HideLabel]
        public SequenceSfx sfx;

        [ShowIf("type", SequenceType.Finger)]
        [BoxGroup("Settings"), HideLabel]
        public SequenceFinger finger;
    }

    public enum SequenceType
    {
        Dialog,
        CameraEvent,
        Fade,
        ShowGameObject,
        Wait,
        Finger,
        Response,
        PlaySfx
    }
    
    public enum CameraTarget
    {
        Castle, District1, District2, District3, District4, District5, District6, District7, District8, District9, District10
    }

    public enum FadeType {FadeIn, FadeOut}
    
    public enum DialoguePortraitAlignment {Left, Right}
    
    [System.Serializable]
    public class SequenceDialog
    {
        [HorizontalGroup("Split", 0.5f, LabelWidth = 70)]
        [BoxGroup("Split/Character")]
        public Actor actor;
        
        [BoxGroup("Split/Text")]
        public DialoguePortraitAlignment alignment;
        
        [BoxGroup("Split/Text")]
        public float speed = 25;
        
        [BoxGroup("Split/Text"), TextArea]
        public string text;

        [BoxGroup("Split/Character")]
        public bool useBackground = true;
    }

    [System.Serializable]
    public class SequenceFade
    {
        public FadeType fadeType;
        public Color fadeColor = Color.white;
        public float duration;
    }

    [System.Serializable]
    public class SequenceCamera
    {
        [EnumPaging]
        public CameraTarget cameraTarget;
        [ShowIf("cameraTarget", CameraTarget.Castle)]
        public bool isFromBottom;
        public float duration;
    }
    
    [TypeInfoBox("Wait for SequenceSystem.Instance.NextSequence called from outside if not using duration.")]
    [System.Serializable]
    public class SequenceGameObject
    {
        public GameObject prefab;
        public bool useDuration;
        [ShowIf("useDuration")]
        public float duration;
        [HideIf("useDuration")]
        public bool waitForNextSequence = true;
    }

    [TypeInfoBox("Wait for SequenceSystem.Instance.NextSequence called from outside if not using duration.")]
    [System.Serializable]
    public class SequenceWait
    {
        public bool useDuration;
        [ShowIf("useDuration")]
        public float duration;
    }

    [System.Serializable]
    public class SequenceResponse
    {
        public UnityEvent response;
    }
    
    [System.Serializable]
    public class SequenceSfx
    {
        public string sfxName;
    }

    [System.Serializable]
    public class SequenceFinger
    {
        public RectTransform target;
        public string layerName;
        public int layerOrder;
    }
}