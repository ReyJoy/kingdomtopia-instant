﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using System;

namespace Joyseed.IdleKingdom
{
    public class CurtainBlocker : BaseBlocker
    {
        public SkeletonGraphic skeletonGraphic;

        private const string closeAnim = "closeCurtain";
        private const string idleAnim = "idleCurtain";
        private const string openAnim = "openCurtain";
        private const string fullyCloseAnim = "fullyCloseCurtain";
        private const string fullyOpenAnim = "fullyOpenCurtain";

        private Action showCallback;

        private Spine.AnimationState SpineAnim { get { return skeletonGraphic.AnimationState; }}

        void OnEnable()
        {
        }

        void OnDisable()
        {
            SpineAnim.Complete -= OnShowAnimComplete;
            SpineAnim.Complete -= OnHideAnimComplete;
        }
        
        public override void Show(Action callback)
        {
            this.showCallback = callback;

            gameObject.SetActive(true);

            SpineAnim.Complete -= OnShowAnimComplete;
            SpineAnim.Complete += OnShowAnimComplete;

            SpineAnim.SetAnimation(0, fullyCloseAnim, false);
            SpineAnim.AddAnimation(0, openAnim, false, 1f);
            SpineAnim.AddAnimation(0, idleAnim, true, 0f);
        }

        void OnShowAnimComplete(TrackEntry trackEntry) 
        {
            Debug.Log($"OnShowAnimComplete {trackEntry.Animation.Name}");
            if (trackEntry.Animation.Name == openAnim && showCallback != null)
            {
                showCallback();
                
                SpineAnim.Complete -= OnShowAnimComplete;
            }
		}

        public override void Hide()
        {
            if (gameObject.activeSelf)
            {
                SpineAnim.Complete -= OnHideAnimComplete;
                SpineAnim.Complete += OnHideAnimComplete;

                SpineAnim.SetAnimation(0, closeAnim, false);
                SpineAnim.AddAnimation(0, fullyOpenAnim, false, 1f);
            }
        }

        void OnHideAnimComplete(TrackEntry trackEntry) 
        {
            Debug.Log($"OnHideAnimComplete {trackEntry.Animation.Name}");
            if (trackEntry.Animation.Name == fullyOpenAnim)
            {
                gameObject.SetActive(false);
                
                SpineAnim.Complete -= OnHideAnimComplete;
            }
		}
	}
}
