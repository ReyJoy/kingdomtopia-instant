﻿using UnityEngine;
using UniRx;
using System;

namespace Joyseed.IdleKingdom
{
    public class BlockerHUD : Singleton<BlockerHUD>
    {
        //public MenuManager menuManager;

        public BaseBlocker[] blockers;

        public static IObservable<Unit> ShowAsObservable(BlockerType type)
        {
            return Observable.Create<Unit>(observer =>
            {
                Instance.Show(type, delegate()
                {
                    observer.OnNext(Unit.Default);
                    observer.OnCompleted();
                });

                return Disposable.Empty;
            });
        }

        public static IObservable<Unit> HideAsObservable()
        {
            return Observable.Create<Unit>(observer =>
            {
                Instance.Hide();
                
                observer.OnNext(Unit.Default);
                observer.OnCompleted();

                return Disposable.Empty;
            });
        }

        public void Show(BlockerType type, Action callback = null)
        {
            Hide();

            var blocker = blockers[(int)type];
            blocker.Show(callback);

            MapScrollingManager.Instance.EnableScroll(false);;
            //menuManager.isBackButtonEnabled = false;
        }

        public void Hide()
        {
            for (int i = 0; i < blockers.Length; i++)
            {
                blockers[i].Hide();
            }

            //menuManager.isBackButtonEnabled = true;
            MapScrollingManager.Instance.EnableScroll(true);
        }
    }

    public enum BlockerType
    {
        Invisible,
        Standard,
        Fade,
        Black
    }
}