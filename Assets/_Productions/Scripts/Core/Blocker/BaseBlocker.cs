﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Joyseed.IdleKingdom
{
    public class BaseBlocker : MonoBehaviour
    {
        public virtual void Show(Action callback)
        {
            gameObject.SetActive(true);

            if (callback != null) callback();
        }
        
        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
