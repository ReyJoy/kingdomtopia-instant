﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UniRx;
using System;

namespace Joyseed.IdleKingdom
{
    public class FadeBlocker : BaseBlocker
    {
        public Image image;
        public float fadeTime = 0.5f;

        private Action completeCallback;
        
        public override void Show(Action callback)
        {
            completeCallback = callback;

            gameObject.SetActive(true);

            var color = image.color;
            color.a = 0f;
            image.color = color;

            image.DOFade(1f, fadeTime).SetEase(Ease.Linear).OnComplete(() => OnTweenComplete());
        }

        void OnTweenComplete()
        {
            if (completeCallback != null) completeCallback();
        }
    }

}