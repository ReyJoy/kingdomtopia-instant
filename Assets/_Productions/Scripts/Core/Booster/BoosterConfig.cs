﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class BoosterConfig : ScriptableObject, IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public double price;
        [ReadOnly] public string benefitType;
        [ReadOnly] public float benefitModifier;

        private BenefitConfig _benefit;

        public string dataID { get {return Id; }}
        public BenefitConfig benefit 
        {
            get 
            {
                if (_benefit == null) {
                    _benefit = new BenefitConfig((BenefitType)System.Enum.Parse(typeof(BenefitType), benefitType), benefitModifier);
                }

                return _benefit;
            }
        }
    }

    [System.Serializable]
    public class BoosterConfigMock : IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public double price;
        [ReadOnly] public string benefitType;
        [ReadOnly] public float benefitModifier;

        private BenefitConfig _benefit;

        public string dataID { get {return Id; }}
        public BenefitConfig benefit 
        {
            get 
            {
                if (_benefit == null) {
                    _benefit = new BenefitConfig((BenefitType)System.Enum.Parse(typeof(BenefitType), benefitType), benefitModifier);
                }

                return _benefit;
            }
        }
    }

    public struct BoosterScriptables : IMockCollection
    {
        public BoosterConfigMock[] BoosterConfigRow;
        public IDataModel[] Rows { get { return BoosterConfigRow; }}
    }
}