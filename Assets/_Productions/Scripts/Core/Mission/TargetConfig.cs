﻿using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class TargetConfig
    {
        public TargetType type;
        public string typeId;

        public double target = 1;

        public override string ToString()
        {
            return $"type {type} - typeId {typeId} - target {target}";
        }
    }

    public enum TargetType
    {
        None = 0,
        DistrictBuy,
        BeginTutorial,
        EndTutorial,
        RebuildableCondition,
        Currency,
        DistrictMilestone,
        StartGame,
        DistrictCanBuy,
        DistrictLevel,
        DistrictClaim,
        DistrictBuyTotal,
        UpgradeBuyTotal
    }
}