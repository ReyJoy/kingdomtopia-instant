﻿namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class Mission
    {
        public Target target;
        public MissionConfig config;

        public Mission(Target target, MissionConfig config)
        {
            this.target = target;
            this.config = config;
        }
    }
}