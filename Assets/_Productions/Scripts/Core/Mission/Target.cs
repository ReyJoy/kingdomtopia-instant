﻿using System;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public abstract class Target<T> : Target
    {
        protected T entity;

        public virtual Target Construct(T entity)
        {
            this.entity = entity;
            
            return this;
        }
        
    }

    public abstract class Target : MonoBehaviour
    {
        public DoubleReactiveProperty progress = new DoubleReactiveProperty();
        public DoubleReactiveProperty maxProgress = new DoubleReactiveProperty();
        [ReadOnly] public BoolReactiveProperty isCompleted = new BoolReactiveProperty();

        [ShowInInspector] protected TargetConfig config;
        
        private IDisposable disposable;
        private bool _completed;

        public void Initialize(TargetConfig config)
        {
            this.config = config;
            
            maxProgress.Value = config.target;
            progress.Value = 0.0;

            disposable = progress.TakeUntilDestroy(gameObject).Subscribe(OnProgress);
            TrackProgress();
        }

        private void OnProgress(double progress)
        {
            _completed = progress >= maxProgress.Value;
            isCompleted.Value = _completed;
            if (_completed)
            {
                disposable.Dispose();
            }
        }

        protected abstract void TrackProgress();
        
    }
}