﻿using System;
using System.Collections.Generic;
using System.Linq;
using Joyseed.IdleKingdom.UI;
using UnityEngine;
using Sirenix.OdinInspector;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class MissionManager : Singleton<MissionManager>
    {
        public MissionManagerState state;
        [ReadOnly]
        public Mission currentMission;
        public MissionConfig[] availableMissions;
        [ShowInInspector, ReadOnly]
        public BoolReactiveProperty missionStarted = new BoolReactiveProperty(false);

        public Action onMissionStart;
        public Action onMissionProgress;
        public Action onMissionComplete;
        
        /*public void LoadCurrentMission()
        {
            // Load mission is available
            if (state.activeMission.Value != "" && currentMission.config == null)
            {
                foreach (var mission in availableMissions)
                {
                    if (mission.dataID != state.activeMission.Value) continue;
                    RunMission(mission, true);
                    return;
                }
            }
        }*/

        public void RunMission(MissionConfig config, bool load = false)
        {
            currentMission = CreateMission(config, load);
            currentMission.target.progress.TakeUntilDestroy(currentMission.target)
                .Subscribe(delegate(double d)
                {
                    onMissionProgress?.Invoke();
                });
            currentMission.target.isCompleted.TakeUntilDestroy(currentMission.target).Subscribe(delegate(bool b)
            {
                if (b && missionStarted.Value)
                {
                    onMissionComplete?.Invoke();
                }
            });
            onMissionStart?.Invoke();
            missionStarted.Value = true;
            CheckMissionCompleted();
        }

        private Mission CreateMission(MissionConfig config, bool load = false)
        {
            var target = TargetConstructor.Create(config.target);
            target.transform.SetParent(transform);

            if (!load)
            {
                state.SetMission(config);
            }
            else
            {
                target.progress.Value = state.activeMissionProgress.Value;
            }
            
            target.progress.TakeUntilDestroy(target).Subscribe(delegate(double progress)
            {
                state.activeMissionProgress.Value = progress;
            });
            
            return new Mission(target, config);
        }

        public void ClaimMissionReward()
        {
            if (currentMission == null)
            {
                Debug.LogError("There is no active mission.");
                return;
            }
            
            if (!currentMission.target.isCompleted.Value)
            {
                Debug.LogError("This mission is not yet completed!");
                return;
            }
            
            //Debug.Log($"Give Reward of {currentMission.config.reward.rewardValueAmount} - {currentMission.config.reward.rewardValueString} {currentMission.config.reward.rewardType}");
            switch (currentMission.config.reward.rewardType)
            {
                case RewardType.Coins:
                    RewardCoin(currentMission.config.reward.rewardValueAmount);
                    //HUDParticle.Instance.SpawnCoin();
                    break;
                case RewardType.Gems:
                    RewardGem(currentMission.config.reward.rewardValueAmount);
                    break;
                default:
                    //Debug.Log("No reward");
                    break;
            }

            currentMission.config.onComplete.Invoke();
            var nextTutorial = currentMission.config.nextTutorial;
            if (nextTutorial != null)
            {
                Instantiate(nextTutorial);
            }
            state.CompleteMission(currentMission.config);

            Destroy(currentMission.target.gameObject);
            currentMission = null;
            missionStarted.Value = false;
        }

        public void CheckMissionCompleted()
        {
            if (missionStarted.Value && currentMission.target.isCompleted.Value)
            {
                onMissionComplete?.Invoke();
                FingerPointer.Instance.Hide();
            }
        }

        private void RewardCoin(double amount)
        {
            Inventory.Instance.Give(TransactionType.Coins, amount);
        }

        private void RewardGem(double amount)
        {
            Inventory.Instance.Give(TransactionType.Gems, amount);
        }
    }
}