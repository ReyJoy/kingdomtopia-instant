﻿namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class Reward
    {
        public RewardType rewardType;
        public double rewardValueAmount;
        public string rewardValueString;

        public Reward(RewardType rewardType, double rewardValueAmount, string rewardValueString)
        {
            this.rewardType = rewardType;
            this.rewardValueAmount = rewardValueAmount;
            this.rewardValueString = rewardValueString;
        }
    }

    public enum RewardType
    {
        None = 0,
        Coins = 1,
        Gems = 2,
        Dummy = 99
    }
}