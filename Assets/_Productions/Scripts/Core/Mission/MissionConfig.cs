﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Joyseed.IdleKingdom
{
    [CreateAssetMenu(fileName = "Mission", menuName = "IdleKingdom/Mission", order = 10)]
    public class MissionConfig : ScriptableObject, IDataModel
    {
        [PreviewField(ObjectFieldAlignment.Center), HideLabel]
        public Sprite icon;
        
        public string missionDescriptionKey;
        
        public TargetConfig target;
        public Reward reward;
        public BaseTutorialStateMachine nextTutorial;
        public UnityEvent onComplete;

        public string dataID => name;

        [Button(ButtonSizes.Large)]
        public void RunThisMission()
        {
            MissionManager.Instance.RunMission(this);
        }
    }
}