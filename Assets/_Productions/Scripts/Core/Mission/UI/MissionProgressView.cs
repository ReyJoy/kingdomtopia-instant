﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class MissionProgressView : MonoBehaviour
    {
        public GameObject view;
        public TextMeshProUGUI title;
        public Image iconImage;
        public Image fillImage;
        public TextMeshProUGUI progressText;
        public Button completeButton;
        public TextMeshProUGUI rewardText;
        public GameObject rewardInfo;
        public ParticleSystem particle;
        public string completeSfx;
        
        private Mission currentMission;

        private void OnEnable()
        {
            MissionManager.Instance.onMissionStart += OnMissionStart;
            MissionManager.Instance.onMissionProgress += OnMissionProgress;
            MissionManager.Instance.onMissionComplete += OnMissionComplete;

            completeButton.onClick.AddListener(ClaimMissionReward);
        }

        private void Start()
        {
            Show(false);
        }

        private void OnMissionComplete()
        {
            ShowMissionClaimButton(true);
            AudioController.Instance.PlayAudioClip("notification");
            //FingerPointer.Instance.Place(completeButton.transform);
        }

        private void OnMissionProgress()
        {
            var target = MissionManager.Instance.currentMission.target;
            progressText.text = $"{target.progress} / {target.maxProgress}";
            if (MissionManager.Instance.currentMission.config.target.type == TargetType.Currency)
            {
                fillImage.fillAmount = (float)(target.progress.Value / target.maxProgress.Value);
                    
            }
            else
            {
                var percentage = (float)(target.progress.Value / target.maxProgress.Value);
                DOTween.Kill(this);
                fillImage.DOFillAmount(percentage, 0.25f).SetEase(Ease.Linear);
                view.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.125f);
                view.transform.DOScale(new Vector3(1f, 1f, 1f), 0.125f).SetDelay(0.125f);
            }
        }

        private void OnMissionStart()
        {
            Show(true);
            currentMission = MissionManager.Instance.currentMission;
            title.text = currentMission.config.missionDescriptionKey;
            ShowMissionClaimButton(false);
            rewardInfo.SetActive(false);
            rewardText.text = currentMission.config.reward.rewardValueAmount.ToString();
            AudioController.Instance.PlayAudioClip("notification");
            view.transform.DOLocalMoveX(540f, 0.5f).From();
            OnMissionProgress();
        }
        
        public void ShowMissionClaimButton(bool b)
        {
            title.gameObject.SetActive(!b);
            completeButton.gameObject.SetActive(b);
            rewardInfo.SetActive(b);
        }

        public void Show(bool b)
        {
            view.SetActive(b);
        }

        void ClaimMissionReward()
        {
            view.SetActive(false);
            particle.Play();
            AudioController.Instance.PlayAudioClip(completeSfx);
            MissionManager.Instance.ClaimMissionReward();
        }
    }
}