﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictTableView : BaseDistrictTableView
    {
        public DistrictView[] items;

        public override void Construct()
        {
            for (int i = 0; i < items.Length; i++)
            {
                int idx = i;

                //items[idx].Construct(DistrictManager.Instance.districts[i], MilestoneManager.Instance.milestones[i], Blackboard.PlayerJourney);

                items[idx].buyButton.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x =>
                {
                    if (onBuy != null) onBuy.OnNext(idx);
                });
                
                items[idx].upgradeButton.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x =>
                {
                    if (onUpgrade != null) onUpgrade.OnNext(idx);
                });

                /*items[idx].mayorButton.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x =>
                {
                    if (onMayor != null) onMayor.OnNext(idx);
                });*/
                
                items[idx].collectButton.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(x =>
                {
                    if (onCollect != null) onCollect.OnNext(idx);
                });
            }
        }

        public override void Initialize()
        {
            for (int i = 0; i < items.Length; i++)
            {
                int idx = i;

                items[idx].Initialize();
            }
        }
    }
}