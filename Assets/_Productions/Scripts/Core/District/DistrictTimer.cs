﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictTimer : MonoBehaviour
    {
        [Title ("Components")]
        public District district;
        public TimeManager timeManager;

        [Title ("Bookkeeping")]
        [ReadOnly] public DoubleReactiveProperty collectTime = new DoubleReactiveProperty(0);
        [ReadOnly] public BoolReactiveProperty isComplete = new BoolReactiveProperty();

        [Title ("Getter")]
        public float baseCollectTime { get { return district.config.InitTime; } }
        public DoubleReactiveProperty progressTimer { get { return district.state.progressTimer; } }
        public bool isProgressComplete { get { return progressTimer.Value >= collectTime.Value; }}
        private const float LimitToMeasureRealTimer = 0.25f;

        private Subject<Unit> onTimerRestart;
        public IObservable<Unit> OnTimerRestart() { return onTimerRestart ?? (onTimerRestart = new Subject<Unit>()); }

        private int _prevLevel;
        private bool isOnTickAlready = false;
        private double lastTime;
        private bool initialized = false;

        public void Start()
        {
            _prevLevel = 0;

            district.level.TakeUntilDisable(this).Subscribe(OnLevelChanged);
            district.hasManager.TakeUntilDisable(this).Subscribe(OnHasManagerChanged);

            if (district.isPurchased.Value) {
                if (!isOnTickAlready)
                {
                    timeManager.OnTick().TakeUntilDisable(this).Subscribe(OnTick);
                    isOnTickAlready = true;
                }
                
            }

            initialized = true;
            CalculateElapsedTime();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
            {
                CalculateElapsedTime();
            }
            else
            {
                lastTime = progressTimer.Value;
            }
        }

        void OnLevelChanged(int level)
        {
            if (level == 0)
            {
                isComplete.Value = true;
            }
            else
            {
                if (_prevLevel == 0)
                {
                    lastTime = progressTimer.Value;
                    
                    RestartTimer();
                    
                    if (!isOnTickAlready)
                    {
                        timeManager.OnTick().TakeUntilDisable(this).Subscribe(OnTick);
                        isOnTickAlready = true;
                    }
                }
            }

            _prevLevel = level;
        }

        void OnTick(float interval)
        {
            if (!isComplete.Value)
            {
                progressTimer.Value += interval;

                CheckTimerStatus();
            }
        }

        void CheckTimerStatus()
        {
            if (progressTimer.Value >= collectTime.Value)
            {
                CompleteTimer();
            }
        }

        void CompleteTimer()
        {
            isComplete.Value = true;

            if (district.hasManager.Value)
            {
                RestartTimer();
            }
        }

        public void RestartTimer()
        {
            progressTimer.Value = 0f;
            isComplete.Value = false;

            if (onTimerRestart != null) onTimerRestart.OnNext(Unit.Default);
        }

        void OnHasManagerChanged(bool hasManager)
        {
            if (district.isPurchased.Value)
            {
                CheckTimerStatus();
            }
        }

        void CalculateElapsedTime()
        {
            if (!initialized) return;
            var span = timeManager.SecondsSinceLastSession;

            if (district.isPurchased.Value)
            {
                var spannedTime = lastTime + span;

                if (district.hasManager.Value)
                {
                    progressTimer.Value = spannedTime % collectTime.Value;

                    int spannedCycle = (int)Math.Floor(spannedTime / collectTime.Value);
                    district.pendingProfit.Value = spannedCycle * district.claimProfit;

                    // Debug.Log($"Pending Profit of {Id} -> {claimProfit} X {spannedCycle} = {pendingProfit.Value}");

                    isComplete.Value = false;
                }
                else
                {
                    progressTimer.Value = GameUtility.Clamp(spannedTime, 0, collectTime.Value);

                    CheckTimerStatus();
                }
            }
        }

        public void UpdateCollectTime()
        {
            collectTime.Value = baseCollectTime / district.speedModifier;

            CheckTimerStatus();
        }
    }
}