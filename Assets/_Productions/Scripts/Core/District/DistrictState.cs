﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.Serialization;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class DistrictState : DataState<DistrictState>
    {
        public IntReactiveProperty level;
        public DoubleReactiveProperty progressTimer;
        public BoolReactiveProperty hasManager;
        
        public DistrictState()
        {
            level = new IntReactiveProperty(0);
            progressTimer = new DoubleReactiveProperty(0);
            hasManager = new BoolReactiveProperty(false);

            InitValues();
        }

        public override void InitValues()
        {
            level.Value = 0;
            progressTimer.Value = 0f;
            hasManager.Value = false;
        }
    }
}