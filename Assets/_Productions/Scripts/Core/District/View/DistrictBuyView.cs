﻿using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class DistrictBuyView : MonoBehaviour
    {
        public TextMeshProUGUI districtName;
        public TextMeshProUGUI districtPrice;
        public Image icon;
        public Button buyButton;
        public string buySfx;

        private District district;

        public void Initialize(District d)
        {
            district = d;
            districtName.text = district.DistrictName;
            icon.sprite = district.Icon;
            districtPrice.text = district.BuyPrice.Value.ToAnnotated();

            district.canBuyDistrict.TakeUntilDestroy(this).Subscribe(OnCanBuyDistrictChanged);
        }

        private void OnCanBuyDistrictChanged(bool canBuy)
        {
            buyButton.interactable = canBuy;
        }
    }
}