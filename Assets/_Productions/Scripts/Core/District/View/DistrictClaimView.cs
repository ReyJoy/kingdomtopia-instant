﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class DistrictClaimView : MonoBehaviour
    {
        [Title ("Components")]
        public GameObject claim;
        public Button claimButton;
        public ParticleSystem particle;
        public string claimSfx;
        //public DistrictClaimTextEffect claimTextEffect;
        
        private District district;
        private DistrictTimer timer;
        private CompositeDisposable disposables;

        private Subject<bool> onClaimActivated;
        public IObservable<bool> OnClaimActivated() { return onClaimActivated ?? (onClaimActivated = new Subject<bool>()); }

        public void Initialize(District d)
        {
            district = d;
            timer = district.timer;
            ActivateClaimButton(false);

            disposables?.Dispose();
            disposables = new CompositeDisposable();
            
            district.isPurchased.TakeUntilDestroy(this).Subscribe(OnPurchased).AddTo(disposables);
            district.OnProfitClaimed().TakeUntilDestroy(this).Subscribe(OnProfitClaimed).AddTo(disposables);
            timer.isComplete.TakeUntilDestroy(this).Subscribe(OnTimerComplete).AddTo(disposables);
            //claimButton.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(_ => OnClickClaimed());
        }

        void OnPurchased(bool isPurchased)
        {
            if (!isPurchased)
            {
                ActivateClaimButton(false);
            }
        }

        void OnProfitClaimed(double profit)
        {
            if (!district.hasManager.Value)
            { 
                particle.gameObject.SetActive(true); 
                particle.Play();
                AudioController.Instance.PlayAudioClip(claimSfx);
                //claimTextEffect.Play(profit);
            }
        }

        void OnTimerComplete(bool isComplete)
        {
            // only show claim button when district is purchased, but has no manager
            if (!(district.isPurchased.Value && !district.hasManager.Value))
            {
                ActivateClaimButton(false);
                return;
            }
            
            bool showClaim = isComplete;
            ActivateClaimButton(isComplete);
        }

        void ActivateClaimButton(bool isActive)
        {
            claim.SetActive(isActive);
            onClaimActivated?.OnNext(isActive);
        }

        void OnClickClaimed()
        {
            district.CollectProfitManually();
        }
    }
}
