﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class DistrictMilestoneView : MonoBehaviour
    {
        private Milestone milestone;
        public Image icon;
        public TextMeshProUGUI typeText;
        public Animator animator;
        public string showSfx;
        private District district;

        private void Start()
        {
            animator.Play("DistrictMilestone_Close");
        }

        public void Initialize(District d)
        {
            district = d;
            milestone = district.milestone;
            milestone.OnUnlocked().TakeUntilDisable(this).Subscribe(Show);
        }

        public void Show(MilestoneConfig milestone)
        {
            var targetDistrict = DistrictManager.Instance.GetDistrictById(milestone.benefitTarget);
            icon.sprite = targetDistrict.Icon;
            typeText.text = $"{targetDistrict.DistrictName} {milestone.benefit.type.ToString()} x{milestone.benefitModifier}";
            animator.Play("DistrictMilestone_Appear", -1, 0f);
            AudioController.Instance.PlayAudioClip(showSfx);
        }
    }

}