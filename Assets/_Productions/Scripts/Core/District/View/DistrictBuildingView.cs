﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class DistrictBuildingView : MonoBehaviour
    {
        private SkeletonGraphic skeletonGraphic;
        //public Material normalMat, grayscaleMat;
        public ParticleSystem constructParticle, automateParticle;

        private District district;
        private DistrictTimer timer;
        private bool isPreviouslyPurchased;

        private void Awake()
        {
            skeletonGraphic = GetComponentInChildren<SkeletonGraphic>();
        }

        public void Initialize(District d)
        {
            district = d;
            timer = district.timer;
            
            isPreviouslyPurchased = district.isPurchased.Value;
            InitializeSpine();
            InitializeBuildingView();

            timer.isComplete.TakeUntilDestroy(this).Subscribe(OnTimerComplete);
            district.isPurchased.TakeUntilDestroy(this).Subscribe(OnDistrictPurchased);
        }

        public void InitializeBuildingView()
        {
            SetConstructedGraphic(district.isPurchased.Value);
        }

        private void OnDistrictPurchased(bool isPurchased)
        {
            InitializeBuildingView();

            if (!isPreviouslyPurchased && isPurchased)
            {
                PlayConstructionFx();
            }

            isPreviouslyPurchased = isPurchased;
        }

        void OnTimerComplete(bool isComplete)
        {
            if (isComplete && !district.hasManager.Value && district.isPurchased.Value)
            {
                //SetMaterial(grayscaleMat);
            }
            else
            {
                //SetMaterial(normalMat);
            }
        }

        void SetConstructedGraphic(bool isConstructed)
        {
            if (isConstructed)
            {
                SetActiveAnimation(district.hasManager.Value);
            }
            else
            {
                UnconstructDistrict();
            }
        }

        void InitializeSpine()
        {
            UnconstructDistrict();
        }

        void UnconstructDistrict()
        {
            skeletonGraphic.AnimationState.SetAnimation(0, "unconstructed", true);
            //skeletonGraphic.startingAnimation = "unconstructed";
            //skeletonGraphic.Initialize(true);
            //SetMaterial(normalMat);
        }

        private void PlayConstructionFx()
        {
            constructParticle.gameObject.SetActive(true);
            constructParticle.Play();
            AudioController.Instance.PlayAudioClip("effect_construction");
        }

        public void PlayAutomateFx()
        {
            automateParticle.gameObject.SetActive(true);
            automateParticle.Play();
            AudioController.Instance.PlayAudioClip("effect_mayor");
        }

        void SetMaterial(Material material)
        {
            skeletonGraphic.material = material;
            skeletonGraphic.SetMaterialDirty();
        }

        void SetActiveAnimation(bool isActive)
        {
            var newAnimation = isActive ? "animation" : "ready";

            if (newAnimation != skeletonGraphic.startingAnimation)
            {
                skeletonGraphic.AnimationState.SetAnimation(0, newAnimation, true);
                //skeletonGraphic.startingAnimation = newAnimation;
                //skeletonGraphic.Initialize(true);
            }
        }
    }
}
