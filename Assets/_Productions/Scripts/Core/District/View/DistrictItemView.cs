﻿using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class DistrictItemView : MonoBehaviour
    {
        public TextMeshProUGUI districtText, levelText, timerText, buyMultiplierText, profitText, priceText;
        public Button upgradeButton;
        public string upgradeSfx;
        public Image barProgress, barMilestone, portrait;
        public GameObject districtTime;
        public Color progressDefaultColor;
        public Sprite progressBarSprite;
        public Material barbershopMaterial;

        private District district;
        private DistrictTimer timer;
        
        //private UpgradeConfig mayorUpgrade { get { return Blackboard.UpgradeManager.mayor.GetUpgrade(district.MayorId); }}
        // bool canAffordAuto { get { return Inventory.Instance.CanSpend(TransactionType.Coins, mayorUpgrade.UpgradeCost); }}
        private const float LimitToMeasureRealTimer = 0.25f;
        
        public void Initialize(District d)
        {
            district = d;
            timer = district.timer;
            
            InitializeDistrictItem();
        }

        private void InitializeDistrictItem()
        {
            districtText.text = district.DistrictName;
            portrait.sprite = district.Icon;

            district.level.TakeUntilDestroy(this).Subscribe(UpdateLevel);
            district.UpgradePrice.TakeUntilDestroy(this).Subscribe(UpdatePrice);
            district.profit.TakeUntilDestroy(this).Subscribe(_ => UpdateProfit());
            district.progressTimer.TakeUntilDestroy(this).Subscribe(_ => UpdateCollectionProgress());
            timer.isComplete.TakeUntilDestroy(this).Subscribe(_ => UpdateCollectionProgress());
            district.totalBuy.TakeUntilDestroy(this).Subscribe(UpdateTotalBuy);
            district.canBuyUpgrade.TakeUntilDestroy(this).Subscribe(UpdateUpgradeInteractable);
            district.hasManager.TakeUntilDestroy(this).Subscribe(OnManagerChanged);
            timer.collectTime.TakeUntilDestroy(this).Subscribe(_ => OnTimerTotalChanged());
            district.nextMilestone.TakeUntilDestroy(this).Subscribe(_ => ResetBar());
        }

        void ResetBar()
        {
            barMilestone.fillAmount = 0f;
        }
        
        void UpdateLevel(int level)
        {
            barMilestone.fillAmount = (float)(district.level.Value - district.lastReachMilestone.Value) / (float)(district.nextMilestone.Value - district.lastReachMilestone.Value);
            levelText.text = level.ToString();
        }
        
        void UpdatePrice(double price)
        {
            priceText.text = price.ToAnnotated();
        }

        void UpdateProfit()
        {
            if (isBarMeasureTimer) {
                profitText.text = district.profit.Value.ToAnnotated();
            }
            else {
                profitText.text = $"{district.profitPerSecond.ToAnnotated()}/s";
            }
        }
        
        public bool isBarMeasureTimer
        {
            get 
            {
                if (district.hasManager.Value) {
                    return district.timer.collectTime.Value > LimitToMeasureRealTimer;
                }
                else {
                    return true;
                }
            }
        }

        void UpdateCollectionProgress()
        {
            if (isBarMeasureTimer) 
            {
                if (timer.isComplete.Value)
                {
                    barProgress.fillAmount = 1f;

                    var remaining = 0f;
                    timerText.text = remaining.ToShortTimeString();

                }
                else
                {
                    var time = district.timer.progressTimer.Value;

                    barProgress.fillAmount = (float)(time / timer.collectTime.Value);

                    var remaining = (float)(timer.collectTime.Value - time);
                    timerText.text = remaining.ToShortTimeString();
                }
            }
        }
        
        void UpdateTotalBuy(int totalBuy)
        {
            buyMultiplierText.text = "x" + totalBuy.ToString();
        }
        
        void UpdateUpgradeInteractable(bool interactable)
        {
            upgradeButton.interactable = interactable;
        }
        
        void OnManagerChanged(bool hasManager)
        {
            UpdateTimerView();
            UpdateProfit();
        }
        
        void OnTimerTotalChanged()
        {
            UpdateProfit();
            UpdateTimerView();
        }
        
        void UpdateTimerView()
        {
            districtTime.SetActive(isBarMeasureTimer);

            if (isBarMeasureTimer)
            {
                NormalProgressBar();
            }
            else
            {
                BarbershopProgressBar();
            }
        }
        
        private void NormalProgressBar()
        {
            barProgress.material = null;
            barProgress.color = progressDefaultColor;
            barProgress.sprite = progressBarSprite;
        }

        private void BarbershopProgressBar()
        {
            barProgress.sprite = null;
            barProgress.material = barbershopMaterial;
        }

        private void OnClickUpgrade()
        {
            AudioController.Instance.PlayAudioClip(upgradeSfx);
            district.Upgrade();
        }
    }
}