﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class DistrictView : MonoBehaviour
    {
        public District district;
        public DistrictItemView item;
        public DistrictBuildingView building;
        public DistrictBuyView buy;
        public DistrictClaimView claim;
        public DistrictMilestoneView milestone;
        public DistrictTooltipView tooltip;
        
        public Button upgradeButton { get { return item.upgradeButton; }}
        //public Button mayorButton { get { return item.mayorButton; }}
        public Button collectButton { get { return claim.claimButton; }}
        public Button buyButton { get { return buy.buyButton; }}

        private void OnEnable()
        {
            district.isPurchased.TakeUntilDisable(this).Subscribe(OnPurchased);
        }

        public void Initialize()
        {
            buy.Initialize(district);
            item.Initialize(district);
            claim.Initialize(district);
            milestone.Initialize(district);
            building.Initialize(district);
        }

        void OnPurchased(bool isPurchased)
        {
            buy.gameObject.SetActive(!isPurchased);
            item.gameObject.SetActive(isPurchased);
            tooltip.gameObject.SetActive(isPurchased);
            //claim.Initialize();
        }
    }

}