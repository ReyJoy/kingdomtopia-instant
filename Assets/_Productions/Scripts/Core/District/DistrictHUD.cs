﻿using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictHUD : Singleton<DistrictHUD>
    {
        public DistrictTableView table;

        List<District> dist { get { return DistrictManager.Instance.districts; }}

        protected override void Awake()
        {
            base.Awake();
            table.Construct();
        }

        public void Start()
        {
            table.Initialize();

            table.OnBuy().TakeUntilDisable(this).Subscribe(idx => dist[idx].Buy());
            table.OnUpgrade().TakeUntilDisable(this).Subscribe(idx => dist[idx].Upgrade());
            table.OnCollect().TakeUntilDisable(this).Subscribe(idx => dist[idx].CollectProfitManually());
        }
    }
}