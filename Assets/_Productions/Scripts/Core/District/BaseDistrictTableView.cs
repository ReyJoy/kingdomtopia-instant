﻿using UnityEngine;
using UniRx;
using System;

namespace Joyseed.IdleKingdom
{
    public abstract class BaseDistrictTableView : MonoBehaviour
    {
        protected Subject<int> onBuy, onUpgrade, onCollect, onMayor;

        public IObservable<int> OnBuy() { return onBuy ?? (onBuy = new Subject<int>()); }
        public IObservable<int> OnUpgrade() { return onUpgrade ?? (onUpgrade = new Subject<int>()); }
        public IObservable<int> OnCollect() { return onCollect ?? (onCollect = new Subject<int>()); }
        public IObservable<int> OnMayor() { return onMayor ?? (onMayor = new Subject<int>());}

        public abstract void Construct();
        public abstract void Initialize();
    }
}