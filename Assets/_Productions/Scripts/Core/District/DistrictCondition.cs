﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public enum DistrictCondition
    {
        Empty,
        Progress,
    }

    public enum BuyMultiplier
    {
        Single,
        Ten,
        Hundred,
        Max,
        Next
    }

    public class BuyMultiplierReactiveProperty : ReactiveProperty<BuyMultiplier>
    {
        public BuyMultiplierReactiveProperty()
        {

        }

        public BuyMultiplierReactiveProperty(BuyMultiplier initValue) : base(initValue)
        {
            
        }
    }

    public class DistrictReactiveProperty : ReactiveProperty<DistrictCondition>
    {
        public DistrictReactiveProperty()
        {
            
        }

        public DistrictReactiveProperty(DistrictCondition initValue) : base(initValue)
        {
            
        }
    }
}