﻿using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictManager : Singleton<DistrictManager>
    {
        public List<District> districts = new List<District>();
        
        private Dictionary<string, District> _districsDict = new Dictionary<string, District>();
        
        public District GetDistrictById(string id)
        {
            if (_districsDict.Count == 0)
            {
                _districsDict = new Dictionary<string, District>();

                for (int i = 0; i < districts.Count; i++)
                {
                    _districsDict.Add(districts[i].Id, districts[i]);
                }
            }

            return _districsDict[id];
        }
    }
}