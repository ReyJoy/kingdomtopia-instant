﻿using System;
using System.Collections;
using System.Collections.Generic;
using Joyseed.IdleKingdom;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class District : MonoBehaviour
    {
        public DistrictConfig config;
        public DistrictTimer timer;
        public DistrictState state;
        public Milestone milestone;
        
        [Title ("Bookkeeping")]
        [ReadOnly] public IntReactiveProperty lastReachMilestone = new IntReactiveProperty();
        [ReadOnly] public IntReactiveProperty nextMilestone = new IntReactiveProperty();
        [ReadOnly] public DoubleReactiveProperty BuyPrice = new DoubleReactiveProperty();
        [ReadOnly] public DoubleReactiveProperty UpgradePrice = new DoubleReactiveProperty();
        [ReadOnly] public IntReactiveProperty Tier = new IntReactiveProperty(1);
        [ReadOnly] public DoubleReactiveProperty profit = new DoubleReactiveProperty();
        [ReadOnly] public IntReactiveProperty totalBuy = new IntReactiveProperty();
        [ReadOnly] public BoolReactiveProperty canBuyUpgrade = new BoolReactiveProperty();
        [ReadOnly] public BoolReactiveProperty canBuyDistrict = new BoolReactiveProperty();
        [ReadOnly] public DoubleReactiveProperty pendingProfit = new DoubleReactiveProperty();
        [ReadOnly] public IntReactiveProperty level = new IntReactiveProperty();
        [ReadOnly] public BoolReactiveProperty isPurchased = new BoolReactiveProperty();
        [ReadOnly, ShowInInspector] Dictionary<DistrictParameterType, double> multipliers = new Dictionary<DistrictParameterType, double>()
        {
            {DistrictParameterType.Profit, 1},
            {DistrictParameterType.Speed, 1},
            {DistrictParameterType.Price, 1},
            {DistrictParameterType.LevelUp, 0}
        };
        
        #region properties
        public string Id { get { return config.Id; } }
        public Sprite Icon => config.icon;
        public string DistrictName => config.districtName;
        public IntReactiveProperty internalLevel { get { return state.level; }}
        public DoubleReactiveProperty progressTimer { get { return state.progressTimer; } }
        public BoolReactiveProperty hasManager { get { return state.hasManager; } }
        public double InitCost { get { return config.InitCost; } }
        public float Coefficient { get { return config.Coefficient; } }
        public float InitTime { get { return config.InitTime; } }
        public float InitRevenue { get { return config.InitRevenue; } }
        public string MayorId { get { return config.MayorId; } }
        public double profitPerSecond { get { return profit.Value / timer.collectTime.Value; }}
        public double speedModifier { get { return multipliers[DistrictParameterType.Speed]; }}
        public double discountMultiplier { get { return multipliers[DistrictParameterType.Price]; }}
        public double profitMultiplier { get { return multipliers[DistrictParameterType.Profit]; }}
        public double claimProfit { get { return profitPerSecond * timer.collectTime.Value; }}
        #endregion
        
        private int _newLv;
        Subject<double> onProfitClaimed;
        public IObservable<double> OnProfitClaimed() { return onProfitClaimed ?? (onProfitClaimed = new Subject<double>()); }

        private void Awake()
        {
            InitializeDistrict();
        }

        private void OnEnable()
        {
            Inventory.Instance.coins.TakeUntilDisable(this).Subscribe(x => UpdateUpgradePrice());
            GlobalModifier.Instance.globalProfit.TakeUntilDisable(this).Subscribe(x => UpdateProfit());
            timer.isComplete.Where(val => val == true).TakeUntilDisable(this).Subscribe(_ => OnTimerComplete());
            timer.timeManager.OnGameTick().TakeUntilDisable(this).Subscribe(GiveAccumulatedProfit);
        }

        private void InitializeDistrict()
        {
            InitializeValues();
        }
        
        void InitializeValues()
        {
            totalBuy.Value = 0;
            
            InitializeBonuses();
            UpdateAllParameters();
        }
        
        void InitializeBonuses()
        {
            hasManager.Value = false;
        }
        
        void UpdateAllParameters()
        {
            UpdateInternalParameters();
            timer.UpdateCollectTime();
        }
        
        void UpdateInternalParameters()
        {
            UpdateLevel();
            UpdateProfit();
            UpdateUpgradePrice();
        }
        
        void UpdateLevel()
        {
            _newLv = internalLevel.Value + (int)multipliers[DistrictParameterType.LevelUp];
            level.Value = _newLv;
            isPurchased.Value = _newLv > 0;
        }
        
        void UpdateProfit()
        {
            profit.Value = this.GetProfit(GlobalModifier.Instance.globalProfit.Value);
        }
        
        void UpdateUpgradePrice()
        {
            //UpdateUpgradePrice(player.multiplier.Value);
            UpdateUpgradePrice(BuyMultiplier.Single);
        }

        void UpdateUpgradePrice(BuyMultiplier buyMultiplier)
        {
            totalBuy.Value = this.GetUpgradeAmount(buyMultiplier);
            BuyPrice.Value = this.GetBuyPrice();
            UpgradePrice.Value = this.GetUpgradePriceByAmount(totalBuy.Value);

            UpdateCanBuyUpgrade();
        }

        void UpdateCanBuyUpgrade()
        {
            canBuyDistrict.Value = (Inventory.Instance.coins.Value >= BuyPrice.Value);
            canBuyUpgrade.Value = (Inventory.Instance.coins.Value >= UpgradePrice.Value);
        }
        
        public void Buy()
        {
            Upgrade(1, InitCost);
        }
        
        public void Upgrade()
        {
            Upgrade(totalBuy.Value, UpgradePrice.Value);
        }
        
        void Upgrade(int amount, double upgradePrice)
        {
            if (Inventory.Instance.CanSpend(TransactionType.Coins, upgradePrice))
            {
                Inventory.Instance.TrySpend(TransactionType.Coins, upgradePrice);
                LevelUp(amount);
            }
        }
        
        public void LevelUp(int amount)
        {
            internalLevel.Value += amount;
            UpdateInternalParameters();
        }
        
        public void CollectProfitManually()
        {
            GiveProfit();
            timer.RestartTimer();
        }
        
        void GiveProfit()
        {
            Inventory.Instance.Give(TransactionType.Coins, claimProfit);
            onProfitClaimed?.OnNext(claimProfit);
        }
        
        void OnTimerComplete()
        {
            var status = timer.collectTime.Value.CompareTo(0.3);
            if (hasManager.Value && status > 0)
            {
                CollectProfitManually();
            }
        }
        
        void GiveAccumulatedProfit(float f)
        {
            if (!hasManager.Value || !isPurchased.Value) return;
            
            var status = timer.collectTime.Value.CompareTo(0.3);
            if (status > 0) return;
            
            Inventory.Instance.Give(TransactionType.Coins, profitPerSecond*0.3f);
        }
        
        public void AddMultiplier(DistrictParameterType parameter, double value)
        {
            if (multipliers.ContainsKey(parameter))
            {
                if (parameter == DistrictParameterType.LevelUp) {
                    multipliers[parameter] += value;
                }
                else {
                    multipliers[parameter] *= value;
                }

                UpdateAllParameters();
            }
        }
        
        public void GiveManager()
        {
            if (!hasManager.Value)
            {
                hasManager.Value = true;
            }
        }
        
        public void SetNextMilestone(int prevLevel, int nextLevel)
        {
            nextMilestone.Value = nextLevel;
            lastReachMilestone.Value = prevLevel;
        }
    }
}