﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Joyseed.IdleKingdom
{
    public class DistrictConfig : ScriptableObject, IDataModel
    {
        public Sprite icon;
        public string districtName;
        [ReadOnly] public string Id;
        [ReadOnly] public double InitCost;
        [ReadOnly] public float Coefficient;
        [ReadOnly] public float InitTime;
        [ReadOnly] public float InitRevenue;
        [ReadOnly] public string MayorId;

        public string dataID { get {return Id; }}

        public override string ToString()
        {
            return $"[District] id {Id}, initCost {InitCost}, coeff {Coefficient}, initTime {InitTime}, initRev {InitRevenue}, mayorID {MayorId}";
        }
    }

    [System.Serializable]
    public class DistrictConfigMock : IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public double InitCost;
        [ReadOnly] public float Coefficient;
        [ReadOnly] public float InitTime;
        [ReadOnly] public float InitRevenue;
        [ReadOnly] public string MayorId;

        public string dataID { get {return Id; }}

        public override string ToString()
        {
            return $"[District] id {Id}, initCost {InitCost}, coeff {Coefficient}, initTime {InitTime}, initRev {InitRevenue}, mayorID {MayorId}";
        }
    }

    public struct DistrictScriptables : IMockCollection
    {
        public DistrictConfigMock[] DistrictConfigRow;
        public IDataModel[] Rows { get { return DistrictConfigRow; }}
    }
}