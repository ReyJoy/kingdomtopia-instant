﻿public enum DistrictParameterType
    {
        None,
        Speed,
        Profit,
        Price,
        LevelUp
    }