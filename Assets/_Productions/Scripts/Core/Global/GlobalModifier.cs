﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class GlobalModifier : Singleton<GlobalModifier>
    {
        [Title ("Parameters")]
        public DoubleReactiveProperty globalProfit = new DoubleReactiveProperty(1f);
        public double heirloomProfit;
        public double videoBonusProfit;


        public void Initialize()
        {
            globalProfit.Value = 1;
            heirloomProfit = 0;
            videoBonusProfit = 1;

            UpdateGlobalProfit();
        }

        public void SetHeirloomProfit(double modifier)
        {
            heirloomProfit = modifier;
            UpdateGlobalProfit();
        }

        void UpdateGlobalProfit()
        {
            globalProfit.Value = (1 + heirloomProfit) * videoBonusProfit;
        }
    }
}