﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using System;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class TimeState : DataState<TimeState>
    {
        public StringReactiveProperty lastActiveTime = new StringReactiveProperty();
        public IntReactiveProperty sessionCount = new IntReactiveProperty();

        public TimeState()
        {
            lastActiveTime.Value = new DateTime(1970, 1, 1).ToString(TimeManager.FMT);
            sessionCount = new IntReactiveProperty(0);
        }

        public override void InitValues()
        {

        }
    }
}