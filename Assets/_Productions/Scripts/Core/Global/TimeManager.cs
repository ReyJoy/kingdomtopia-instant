﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UniRx;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Joyseed.IdleKingdom
{
    public class TimeManager : Singleton<TimeManager>
    {
        public TimeState state;

        [Title ("Parameters")]
        public float intervalInSecond = 0.1f;

        [Title("Bookkeeping")] public const string FMT = "O";
        public DateTime lastActiveTime 
        { 
            get => DateTime.ParseExact(state.lastActiveTime.Value, FMT, CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind);
            set => state.lastActiveTime.Value = value.ToString(FMT);
        }
        
        public double SecondsSinceLastSession => (DateTime.UtcNow - lastActiveTime).TotalSeconds;

        private Subject<float> onTick, onTickEverySec, onGameTick;
        private Subject<bool> onSessionAppFocus;
        private Subject<Unit> onNewSession;
        
        public IObservable<float> OnTick() { return onTick ?? (onTick = new Subject<float>()); }
        public IObservable<float> OnTickEverySec() { return onTickEverySec ?? (onTickEverySec = new Subject<float>()); }

        public IObservable<float> OnGameTick()
        {
            return onGameTick ?? (onGameTick = new Subject<float>());}
        public IObservable<bool> OnSessionAppFocus() { return onSessionAppFocus ?? (onSessionAppFocus = new Subject<bool>()); }

        private bool isInitialized;
        
        private GlobalModifier globalModifier;

        protected override void Awake()
        {
            base.Awake();
            globalModifier = GlobalModifier.Instance;
        }

        public void Start()
        {
            isInitialized = true;
            TrySetLastActiveTime();
            SubscribeObservables();
        }

        void SubscribeObservables()
        {
            var tickSecond = TimeSpan.FromSeconds(1);

            Observable.Interval(tickSecond).TakeUntilDestroy(this).Subscribe(TickEverySecond);
            Observable.Interval(TimeSpan.FromSeconds(0.3f)).TakeUntilDestroy(this).Subscribe(TickGameTime);
        }

        #region MonoBehaviour calls
        private void OnApplicationFocus(bool hasFocus)
        {
            if (isInitialized)
            {
                if (!hasFocus)
                {
                    TrySetLastActiveTime();
                }

                onSessionAppFocus?.OnNext(hasFocus);
            }
        }

        private void Update()
        {
            TickInterval(Time.deltaTime);
        }
        #endregion

        public void TrySetLastActiveTime()
        {
            lastActiveTime = DateTime.UtcNow;
        }

        private void TickInterval(float interval)
        {
            onTick?.OnNext(interval);
        }

        private void TickEverySecond(long interval)
        {
            onTickEverySec?.OnNext(1);
        }

        private void TickGameTime(long interval)
        {
            onGameTick?.OnNext(0.3f);
        }
    }
}