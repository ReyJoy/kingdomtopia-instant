﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bedivere.AnimatorStateMachine;
using UniRx;
using System;
using UnityEngine.Events;

namespace Joyseed.IdleKingdom
{
    public abstract class BaseTutorialStateMachine : BDVStateMachine
    {
        public bool isPreventSaveWhenRunning = true;

        public string Id => gameObject.name.Replace("(Clone)", "");
        
        private Subject<BaseTutorialStateMachine> onBegin, onCompleted;
        
        public IObservable<BaseTutorialStateMachine> OnBegin() { return onBegin ?? (onBegin = new Subject<BaseTutorialStateMachine>()); }
        public IObservable<BaseTutorialStateMachine> OnCompleted() { return onCompleted ?? (onCompleted = new Subject<BaseTutorialStateMachine>()); }

        
        public MissionConfig nextMission;
        public BaseTutorialStateMachine nextTutorial;
        public UnityEvent onComplete;

        private BoolReactiveProperty isReady = new BoolReactiveProperty(true);

        private int stepIndex = 0;
        
        protected virtual void Construct()
        {
            this.ConstructAllStates(Animator);
        }

        public virtual IObservable<Unit> OnTutorialReady()
        {
            return isReady.Where(ready => ready).AsUnitObservable();
        }

        public void BeginTutorial()
        {
            TutorialManager.Instance.BeginTutorial(this);

            FinishStep();
            
            onBegin?.OnNext(this);
        }

        public void CompleteTutorial()
        {
            TutorialManager.Instance.CompleteTutorial(this);

            if (nextMission != null)
            {
                nextMission.RunThisMission();
            }

            if (nextTutorial != null)
            {
                Instantiate(nextTutorial, transform.parent);
            }
            onComplete.Invoke();
            
            onCompleted?.OnNext(this);
        }

        public void ExitTutorial()
        {
            Destroy(gameObject);
        }

        public void FinishStep()
        {
            stepIndex++;
            Animator.SetTrigger("finished");
        }
    }
}
