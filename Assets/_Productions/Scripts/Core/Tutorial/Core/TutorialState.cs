﻿using System;
using System.Collections.Generic;
using UniRx;

namespace Joyseed.IdleKingdom
{
    [Serializable]
    public class TutorialState : DataState<TutorialState>
    {
        public List<string> completedTutorials;
        public StringReactiveProperty activeTutorial;
        public string lastTutorial;
        
        Subject<Unit> onCompleted;
        public IObservable<Unit> OnCompleted() { return onCompleted ?? (onCompleted = new Subject<Unit>()); }

        public TutorialState()
        {
            completedTutorials = new List<string>();
            activeTutorial = new StringReactiveProperty();
        }

        public void Complete(string tutorialID)
        {
            activeTutorial.Value = "";
            if (completedTutorials.Contains(tutorialID)) return;
            
            completedTutorials.Add(tutorialID);
            onCompleted?.OnNext(Unit.Default);
        }

        public bool IsCompleted(string tutorialID)
        {
            return completedTutorials.Contains(tutorialID);
        }

        public void SetTutorial(string tutorialID)
        {
            activeTutorial.Value = tutorialID;
            lastTutorial = tutorialID;
        }
        
        public override void InitValues()
        {
            
        }
    }
}