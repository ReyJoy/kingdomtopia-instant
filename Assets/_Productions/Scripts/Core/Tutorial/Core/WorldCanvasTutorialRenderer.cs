﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class WorldCanvasTutorialRenderer : MonoBehaviour
    {
        private Canvas canvas;
        private GraphicRaycaster raycaster;

        void Start()
        {
            AddRaycaster();
        }

        public void AddRaycaster()
        {
            gameObject.layer = LayerMask.NameToLayer("Tutorial");
            
            canvas = gameObject.AddComponent<Canvas>();
            raycaster = gameObject.AddComponent<GraphicRaycaster>();
            canvas.rootCanvas.worldCamera = WorldCamera.Instance.worldTutorialCamera;
        }

        public void RemoveRaycaster()
        {
            gameObject.layer = LayerMask.NameToLayer("Default");

            canvas.rootCanvas.worldCamera = WorldCamera.Instance.worldCamera;
            if (raycaster) GameObject.Destroy(raycaster);
            if (canvas) GameObject.Destroy(canvas);
            GameObject.Destroy(this);
        }
    }
}
