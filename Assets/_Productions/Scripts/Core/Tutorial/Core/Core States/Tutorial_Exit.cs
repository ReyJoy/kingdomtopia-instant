using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using System;

namespace Joyseed.IdleKingdom
{
    public class Tutorial_Exit : BDVStateMachineBehaviour<BaseTutorialStateMachine>
    {
        protected override void OnEnter()
        {
            stateMachine.ExitTutorial();
        }

        protected override void OnExit()
        {
        }
    }
}