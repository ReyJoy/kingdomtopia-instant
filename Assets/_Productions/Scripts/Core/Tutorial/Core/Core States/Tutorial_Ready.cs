﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using System;

namespace Joyseed.IdleKingdom
{
    public class Tutorial_Ready : BDVStateMachineBehaviour<BaseTutorialStateMachine>
    {
        private IDisposable disposable;

        protected override void OnEnter()
        {
            stateMachine.OnTutorialReady().Take(1).Subscribe(_ => stateMachine.BeginTutorial());
        }

        protected override void OnExit()
        {
        }
    }
}
