﻿using System;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public static class TargetConstructor
    {
        public static Target Create(TargetConfig config)
        {
            //Debug.Log($"Generating Target : {config}");
            var gObj = new GameObject($"Target {config.type} {config.typeId}");
            Target target = null;
            
            switch (config.type)
            {
                case TargetType.None:
                    Debug.LogError($"TargetType {config.type} is explicitly not defined.");
                    break;
                
                case TargetType.DistrictBuy:
                    target = gObj.AddComponent<BuyDistrictTarget>().Construct(DistrictManager.Instance.GetDistrictById(config.typeId));
                    break;
                
                case TargetType.BeginTutorial:
                    target = gObj.AddComponent<TutorialBeginTarget>().Construct(TutorialManager.Instance);
                    break;
                
                case TargetType.EndTutorial:
                    target = gObj.AddComponent<TutorialEndTarget>().Construct(TutorialManager.Instance);
                    break;

                case TargetType.Currency:
                    target = gObj.AddComponent<CurrencyTarget>().Construct(Inventory.Instance);
                    break;

                case TargetType.DistrictMilestone:
                    target = gObj.AddComponent<DistrictMilestoneTarget>().Construct(MilestoneManager.Instance);
                    break;
                
                case TargetType.StartGame:
                    target = gObj.AddComponent<GameStartTarget>().Construct(TimeManager.Instance);
                    break;

                case TargetType.DistrictCanBuy:
                    target = gObj.AddComponent<DistrictCanBuyTarget>().Construct(DistrictManager.Instance);
                    break;
                
                case TargetType.DistrictLevel:
                    target = gObj.AddComponent<DistrictLevelTarget>().Construct(DistrictManager.Instance);
                    break;

                case TargetType.DistrictClaim:
                    target = gObj.AddComponent<DistrictClaimTarget>().Construct(DistrictManager.Instance);
                    break;
                
                case TargetType.DistrictBuyTotal:
                    target = gObj.AddComponent<DistrictBuyTotalTarget>().Construct(DistrictManager.Instance);
                    break;
                
                case TargetType.UpgradeBuyTotal:
                    target = gObj.AddComponent<UpgradeBuyTotalTarget>().Construct(UpgradeManager.Instance);
                    break;
                
                default:
                    Debug.LogError($"TargetType {config.type} is not defined.");
                    break;
            }
            
            target.Initialize(config);

            return target;
        }
    }
}