﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class TutorialRenderer : MonoBehaviour
    {
        private Canvas canvas;
        private GraphicRaycaster raycaster;

        void Start()
        {
            AddRaycaster();
        }

        public void AddRaycaster()
        {
            canvas = gameObject.AddComponent<Canvas>();
            raycaster = gameObject.AddComponent<GraphicRaycaster>();

            canvas.overrideSorting = true;
            canvas.sortingOrder = 5;
            canvas.sortingLayerName = "Tutorial";
        }

        public void RemoveRaycaster()
        {
            if (raycaster) GameObject.Destroy(raycaster);
            if (canvas) GameObject.Destroy(canvas);
            GameObject.Destroy(this);
        }
    }
}
