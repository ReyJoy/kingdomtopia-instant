﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Joyseed.IdleKingdom
{
    public class TutorialManager : Singleton<TutorialManager>
    {
        [Title ("Injectables")]
        public TutorialState state;
        public BaseTutorialStateMachine tutorialClaimReward;
        public BaseTutorialStateMachine[] stateMachines;
        
        private List<IObservable<BaseTutorialStateMachine>> completedStreams;
        
        private MissionManager missionManager;

        private Subject<string> onTutorialBegin, onTutorialComplete;
        public IObservable<string> OnTutorialBegin() { return onTutorialBegin ?? (onTutorialBegin = new Subject<string>()); }
        public IObservable<string> OnTutorialComplete() { return onTutorialComplete ?? (onTutorialComplete = new Subject<string>()); }
        
        public void Construct(MissionManager missionManager)
        {
            this.missionManager = missionManager;
        }

        public void Initialize()
        {
            
        }
        
        public void LoadCurrentTutorial()
        {
            // Load tutorial if available
            if (state.activeTutorial.Value != "" && state.activeTutorial.Value != tutorialClaimReward.Id)
            {
                foreach (var tutorial in stateMachines)
                {
                    if (tutorial.Id != state.activeTutorial.Value) continue;
                    ShowTutorial(tutorial);
                    return;
                }
            }

            // Try to load future tutorial if available
            LoadFutureTutorial();
        }

        private void LoadFutureTutorial()
        {
            //Debug.Log("Try load future tutorial");
            //Debug.Log("Not FTUE anymore");
                foreach (var tutorial in stateMachines)
                {
                    //Debug.Log($"Checking tutorial {tutorial.Id}");
                    if (!state.completedTutorials.Contains(tutorial.Id))
                    {
                        //Debug.Log($"Tutorial {tutorial.Id} is not completed");
                        ShowTutorial(tutorial);
                    }
                }
        }

        public void BeginTutorial(BaseTutorialStateMachine tutorial)
        {
            state.SetTutorial(tutorial.Id);
            OnTutorialBegin(tutorial);
        }

        public void CompleteTutorial(BaseTutorialStateMachine tutorial)
        {
            state.Complete(tutorial.Id);
            OnTutorialCompleted(tutorial);
            ShowTutorialClaimMission();
        }

        private void ShowTutorial(BaseTutorialStateMachine tutorial)
        {
            Instantiate(tutorial, transform);
        }

        public BaseTutorialStateMachine GetTutorialById(string Id)
        {
            Debug.Log(Id);
            return stateMachines.SingleOrDefault(tut => tut.Id == Id);
        }
        
        private void OnTutorialBegin(BaseTutorialStateMachine tutorial)
        {
            //Debug.Log($"tutorial begin : {tutorial.Id}");
            
            onTutorialBegin?.OnNext(tutorial.Id);
        }

        private void OnTutorialCompleted(BaseTutorialStateMachine tutorial)
        {
            //Debug.Log($"tutorial completed : {tutorial.Id}");
            state.Complete(tutorial.Id);

            onTutorialComplete?.OnNext(tutorial.Id);
        }

        public void ShowTutorialClaimMission()
        {
            if ((state.lastTutorial == "Tutorial First District"))
            {
                BeginTutorial(tutorialClaimReward);
                ShowTutorial(tutorialClaimReward);
            }
        }
    }
}
