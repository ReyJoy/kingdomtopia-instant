﻿using System.Collections.Generic;
using UniRx;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class MissionManagerState : DataState<MissionManagerState>
    {
        public List<string> completedMissions = new List<string>();
        public StringReactiveProperty activeMission = new StringReactiveProperty();
        public DoubleReactiveProperty activeMissionProgress = new DoubleReactiveProperty();
        
        public MissionManagerState()
        {
            completedMissions = new List<string>();
            activeMission = new StringReactiveProperty();
            activeMissionProgress = new DoubleReactiveProperty();
        }

        public override void InitValues()
        {

        }

        public void CompleteMission(MissionConfig missionConfig)
        {
            if (!completedMissions.Contains(missionConfig.dataID))
            {
                completedMissions.Add(missionConfig.dataID);
            }
            
            SetMission(null);
        }

        public void SetMission(MissionConfig mission)
        {
            if (mission == null)
            {
                activeMission.Value = string.Empty;
            }
            else
            {
                activeMission.Value = mission.dataID;
            }
            
            activeMissionProgress.Value = 0f;

        }
    }
}