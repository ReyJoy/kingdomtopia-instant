﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class TutorialUpgradeDistrict_Upgrade : BDVStateMachineBehaviour<TutorialUpgradeDistrict>
    {
        private District district => DistrictManager.Instance.districts[0];
        private DistrictView districtView => DistrictHUD.Instance.table.items[0];
        private TutorialRenderer building, item, tooltip;
        private IDisposable disposable;

        protected override void OnEnter()
        {
            stateMachine.StartCoroutine(ShowTutorial());
        }

        IEnumerator ShowTutorial()
        {
            BlockerHUD.Instance.Show(BlockerType.Black);
            yield return new WaitForSeconds(0.5f);

            building = districtView.building.gameObject.AddComponent<TutorialRenderer>();
            item = districtView.item.gameObject.AddComponent<TutorialRenderer>();
            //tooltip = districtView.tooltip.gameObject.AddComponent<TutorialRenderer>();

            FingerPointer.Instance.Place(districtView.upgradeButton.transform);
            //districtView.tooltip.Show(I2.Loc.ScriptLocalization.Tooltip.upgrade_01);

            disposable = district.level.Subscribe(OnLevelChanged);
        }

        void OnLevelChanged(int level)
        {
            if (level > 1)
            {            
                //districtView.tooltip.Hide();
                FingerPointer.Instance.Reset();
                stateMachine.StartCoroutine(CommenceFinishStep());
            }
        }

        private IEnumerator CommenceFinishStep()
        {
            var button = item.gameObject.GetComponentInChildren<Button>();
            button.enabled = false;
            yield return new WaitForSeconds(1f);
            button.enabled = true;

            //districtView.tooltip.Show(I2.Loc.ScriptLocalization.Tooltip.upgrade_02, 3f);
            stateMachine.FinishStep();
        }

        protected override void OnExit()
        {
            disposable.Dispose();
            
            building.RemoveRaycaster();
            item.RemoveRaycaster();
            //tooltip.RemoveRaycaster();
            
            BlockerHUD.Instance.Hide();

        }
    }
}