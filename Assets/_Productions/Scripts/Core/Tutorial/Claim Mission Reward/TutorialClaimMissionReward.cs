﻿using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialClaimMissionReward : BaseTutorialStateMachine
    {
        public Sequence[] sequences;
        void Awake()
        {
            Construct();
        }

        protected override void Construct()
        {
            base.Construct();
            this.ConstructAllStates(Animator);
        }
    }
}