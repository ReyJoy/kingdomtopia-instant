﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class TutorialClaimMissionReward_01 : BDVStateMachineBehaviour<TutorialClaimMissionReward>
    {
        TutorialRenderer _renderer;
        private IDisposable disposable;
        private Button claimButton;
        protected override void OnEnter()
        {
            TopUIView.Instance.missionProgressView.GetComponent<Canvas>().sortingLayerName = "Tutorial";
            BlockerHUD.Instance.Show(BlockerType.Black);
            claimButton = TopUIView.Instance.missionProgressView.completeButton;
            _renderer = claimButton.gameObject.AddComponent<TutorialRenderer>();
            FingerPointer.Instance.Place(claimButton.transform);
        
            disposable = claimButton.OnClickAsObservable().Subscribe(_ => OnEventRaised());
        }

        protected override void OnExit()
        { 
            disposable.Dispose();
        }
    
        public void OnEventRaised()
        {
            BlockerHUD.Instance.Hide();
            FingerPointer.Instance.Reset();
            _renderer.RemoveRaycaster();

            stateMachine.FinishStep();
        }
    }

}
