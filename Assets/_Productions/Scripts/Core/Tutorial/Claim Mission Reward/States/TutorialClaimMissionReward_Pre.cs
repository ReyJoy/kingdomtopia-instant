﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class TutorialClaimMissionReward_Pre : BDVStateMachineBehaviour<TutorialClaimMissionReward>
    {
        private CompositeDisposable disposables;

        protected override void OnEnter()
        {
            TopUIView.Instance.missionProgressView.GetComponent<Canvas>().sortingLayerName = "Top UI";
            SequenceSystem.Instance.RunSequence(stateMachine.sequences[0], () => stateMachine.FinishStep());
        }

        protected override void OnExit()
        {
        }
    }

}