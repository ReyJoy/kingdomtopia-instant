﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictLevelTarget : Target<DistrictManager>
    {
        protected override void TrackProgress()
        {
            var district = entity.GetDistrictById(config.typeId);
            district.level.TakeUntilDisable(gameObject).Subscribe(delegate(int level)
            {
                progress.Value = level;
            });
        }
    }
}