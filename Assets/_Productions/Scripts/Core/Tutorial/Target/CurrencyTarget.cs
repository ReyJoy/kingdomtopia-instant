﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class CurrencyTarget : Target<Inventory>
    {
        protected override void TrackProgress()
        {
            var currency = entity.GetCurrency((TransactionType)Enum.Parse(typeof(TransactionType), config.typeId));
            currency.TakeUntilDisable(gameObject).Subscribe(delegate(double value)
            {
                progress.Value = value;
            });
        }
    }
}