﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialEndTarget : Target<TutorialManager>
    {
        protected override void TrackProgress()
        {
            if (entity.state.completedTutorials.Contains(config.typeId))
            {
                progress.Value += 1;
            }
            
            entity.OnTutorialComplete().Where(id => id == config.typeId).TakeUntilDisable(this).Subscribe(s => 
            {
                progress.Value += 1;
            });
        }
    }
}