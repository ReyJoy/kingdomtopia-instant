﻿using System;
using System.Collections.Generic;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class DistrictBuyTotalTarget : Target<DistrictManager>
    {
        List<District> Districts => entity.districts;
        
        protected override void TrackProgress()
        {
            List<IObservable<Unit>> streams = new List<IObservable<Unit>>();
            
            for (int i = 0; i < Districts.Count; i++)
            {
                var purchasedStream = Districts[i].isPurchased.Where(purchased => purchased).AsUnitObservable();
                streams.Add(purchasedStream);
            }

            Observable.Merge(streams).TakeUntilDisable(gameObject).Subscribe(_ => OnDistrictPurchased());
        }

        private void OnDistrictPurchased()
        {
            int totalCount = 0;
            for (int i = 0; i < Districts.Count; i++)
            {
                if (Districts[i].isPurchased.Value)
                {
                    totalCount++;
                }
            }

            progress.Value = totalCount;
        }
    }
}