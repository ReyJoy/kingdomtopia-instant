﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictMilestoneTarget : Target<MilestoneManager>
    {
        protected override void TrackProgress()
        {
            var currency = entity.OnMilestoneUnlocked().TakeUntilDisable(gameObject).Subscribe(
                delegate(MilestoneConfig milestoneConfig)
                {
                    var typeId = config.typeId;

                    if (string.IsNullOrEmpty(typeId) || milestoneConfig.district == typeId)
                    {
                        progress.Value += 1;
                    }
                });
        }
    }
}