﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class DistrictCanBuyTarget : Target<DistrictManager>
    {
        protected override void TrackProgress()
        {
            var district = entity.GetDistrictById(config.typeId);
            district.canBuyDistrict.Where(canBuy => canBuy).TakeUntilDisable(gameObject).Subscribe(delegate(bool b)
            {
                progress.Value += 1;
            });
        }
    }
}