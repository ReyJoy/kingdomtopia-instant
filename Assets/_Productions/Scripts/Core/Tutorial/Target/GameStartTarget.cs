﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class GameStartTarget : Target<TimeManager>
    {
        protected override void TrackProgress()
        {
            Debug.LogWarning("GameStartMission: Update this to include proper session tracking in the future.");
            progress.Value += 1;
        }
    }
}