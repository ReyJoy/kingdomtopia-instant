﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class BuyDistrictTarget : Target<District>
    {
        protected override void TrackProgress()
        {
            entity.isPurchased.TakeUntilDisable(this).Subscribe(delegate(bool isPurchased)
            {
                if (isPurchased)
                {
                    progress.Value += 1;
                }
            });
        }
    }
}