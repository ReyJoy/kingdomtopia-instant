﻿using UniRx;

namespace Joyseed.IdleKingdom
{
    public class UpgradeBuyTotalTarget : Target<UpgradeManager>
    {
        private UpgradeCollection collection;
        protected override void TrackProgress()
        {
            collection = entity.GetCollectionByName(config.typeId);
            collection.OnPurchased().AsUnitObservable().TakeUntilDisable(gameObject).Subscribe(_ => OnUpgradePurchased());
            
            OnUpgradePurchased();
        }

        private void OnUpgradePurchased()
        {
            progress.Value = collection.items.Count - collection.availableItems.Count;
        }
    }
}