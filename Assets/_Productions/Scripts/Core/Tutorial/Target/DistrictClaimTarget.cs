﻿using UniRx;

namespace Joyseed.IdleKingdom
{
    public class DistrictClaimTarget : Target<DistrictManager>
    {
        protected override void TrackProgress()
        {
            var district = entity.GetDistrictById(config.typeId);
            district.OnProfitClaimed().TakeUntilDisable(gameObject).Subscribe(delegate(double amount)
            {
                progress.Value += 1;
            });
        }
    }
}