﻿using System;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialBeginTarget : Target<TutorialManager>
    {
        protected override void TrackProgress()
        {
            entity.OnTutorialBegin().Where(id => id == config.typeId).TakeUntilDisable(this).Subscribe(s => 
            {
                progress.Value += 1;
            });
        }
    }
}