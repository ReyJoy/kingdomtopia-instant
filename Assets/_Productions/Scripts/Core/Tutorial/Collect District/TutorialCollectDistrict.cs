﻿using Bedivere.AnimatorStateMachine;
namespace Joyseed.IdleKingdom
{
    public class TutorialCollectDistrict : BaseTutorialStateMachine
    {
        public Sequence[] sequences;

        void Awake()
        {
            Construct();
        }

        protected override void Construct()
        {
            base.Construct();
            this.ConstructAllStates(Animator);
        }
    }
}