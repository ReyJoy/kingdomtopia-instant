﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialCollectDistrict_Collect : BDVStateMachineBehaviour<TutorialCollectDistrict>
    {
        private CompositeDisposable disposables;

        private District district;
        private TutorialRenderer item, claim;

        protected override void OnEnter()
        {
            stateMachine.StartCoroutine(StartTutorial());
        }

        IEnumerator StartTutorial()
        {
            BlockerHUD.Instance.Show(BlockerType.Black);
            yield return new WaitForSeconds(0.5f);
            district = DistrictManager.Instance.districts[0];
            
            disposables = new CompositeDisposable();
            var districtView =  DistrictHUD.Instance.table.items[0];
            FingerPointer.Instance.Place(districtView.claim.claimButton.transform);

            district.timer.isComplete.Where(x => x == true).Subscribe(_ => OnTimerComplete()).AddTo(disposables);
        }

        void OnTimerComplete()
        {

            var districtView =  DistrictHUD.Instance.table.items[0];
            claim = districtView.claim.gameObject.AddComponent<TutorialRenderer>();

            district.OnProfitClaimed().Subscribe(_ => OnProfitClaimed()).AddTo(disposables);
        }

        void OnProfitClaimed()
        {
            stateMachine.FinishStep();
        }

        protected override void OnExit()
        {
            claim.RemoveRaycaster();

            BlockerHUD.Instance.Hide();
            FingerPointer.Instance.Reset();
            disposables.Dispose();
        }
    }
}