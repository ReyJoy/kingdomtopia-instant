﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialMayor_Pre : BDVStateMachineBehaviour<TutorialMayor>
    {
        private CompositeDisposable disposables;

        protected override void OnEnter()
        {
            SequenceSystem.Instance.RunSequence(stateMachine.sequences[0], () => stateMachine.FinishStep());
        }

        protected override void OnExit()
        {
        }
    }
}