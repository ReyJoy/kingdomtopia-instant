﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialMayor_Menu : BDVStateMachineBehaviour<TutorialMayor>
    {
        TutorialRenderer _renderer;

        private IDisposable disposable;

        protected override void OnEnter()
        {
            //stateMachine.unlockMenuUpgrade.Value = true;
            BottomUIMenu.Instance.unlockUpgradeMenu.Value = true;
            BlockerHUD.Instance.Show(BlockerType.Black);
            GameObject highlight = BottomUIView.Instance.tabbedSelectionView.items[0].gameObject;
            _renderer = highlight.AddComponent<TutorialRenderer>();
            FingerPointer.Instance.Place(highlight.transform);

            UpgradeMenu.OnOpen += OnEventRaised;
        }

        protected override void OnExit()
        {
            BlockerHUD.Instance.Hide();
            _renderer.RemoveRaycaster();
            FingerPointer.Instance.Reset();
            
            UpgradeMenu.OnOpen -= OnEventRaised;
        }

        public void OnEventRaised()
        {
            FingerPointer.Instance.Reset();
            _renderer.RemoveRaycaster();

            stateMachine.FinishStep();
        }
    }
}