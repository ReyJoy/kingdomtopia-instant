﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialMayor_BackToHome : BDVStateMachineBehaviour<TutorialMayor>
    {
        TutorialRenderer _renderer;

        private IDisposable disposable;

        protected override void OnEnter()
        {
            BlockerHUD.Instance.Show(BlockerType.Black);
            GameObject highlight = BottomUIView.Instance.tabbedSelectionView.items[1].gameObject;
            _renderer = highlight.AddComponent<TutorialRenderer>();
            FingerPointer.Instance.Place(highlight.transform);

            UpgradeMenu.OnClosed += OnEventRaised;
        }

        protected override void OnExit()
        {
            _renderer.RemoveRaycaster();
            FingerPointer.Instance.Reset();
        }

        public void OnEventRaised()
        {
            BlockerHUD.Instance.Hide();
            FingerPointer.Instance.Reset();
            _renderer.RemoveRaycaster();
            UpgradeMenu.OnClosed -= OnEventRaised;
            stateMachine.StartCoroutine(CommenceFinishStep());
        }
        
        private IEnumerator CommenceFinishStep()
        {
            yield return new WaitForSeconds(1.025f);
            BlockerHUD.Instance.Show(BlockerType.Invisible);
            yield return new WaitForSeconds(1.5f);
            BlockerHUD.Instance.Hide();
            
            stateMachine.FinishStep();
        }
    }
}