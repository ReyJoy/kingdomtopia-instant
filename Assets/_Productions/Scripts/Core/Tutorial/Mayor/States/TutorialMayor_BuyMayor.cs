﻿using Bedivere.AnimatorStateMachine;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class TutorialMayor_BuyMayor : BDVStateMachineBehaviour<TutorialMayor>
    {
        private ScrollRect scrollRect;
        private BaseUpgradeCollectionItemView mayorItem;
        private TutorialRenderer _renderer;

        protected override void OnEnter()
        {
            BlockerHUD.Instance.Show(BlockerType.Black);
            var mayorView = UpgradeMenu.Instance.mayorView;
            scrollRect = mayorView.scrollRect;
            scrollRect.enabled = false;
            TopUIView.Instance.missionProgressView.GetComponent<Canvas>().sortingLayerName = "Top UI";
            
            mayorItem = UpgradeMenu.Instance.mayorView.items[0];

            _renderer = mayorItem.gameObject.AddComponent<TutorialRenderer>();
            FingerPointer.Instance.Place(mayorItem.upgradeButton.transform);
            
            UpgradeManager.Instance.mayor.OnPurchased().Take(1).Subscribe(_ => stateMachine.FinishStep());
        }

        protected override void OnExit()
        {
            BlockerHUD.Instance.Hide();
            scrollRect.enabled = true;
            _renderer.RemoveRaycaster();
            FingerPointer.Instance.Reset();
        }
    }
}