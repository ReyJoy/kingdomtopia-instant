﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialFirstDistrict_02 : BDVStateMachineBehaviour<TutorialFirstDistrict>
    {
        private TutorialRenderer building, buy;
        private IDisposable disposable;

        protected override void OnEnter()
        {
            stateMachine.StartCoroutine(ShowTutorial());
        }

        IEnumerator ShowTutorial()
        {
            BlockerHUD.Instance.Show(BlockerType.Black);
            yield return new WaitForSeconds(0.5f);

            var districtView =  DistrictHUD.Instance.table.items[0];
            building = districtView.building.gameObject.AddComponent<TutorialRenderer>();
            buy = districtView.buy.gameObject.AddComponent<TutorialRenderer>();

            FingerPointer.Instance.Place(districtView.buy.buyButton.transform);

            disposable = districtView.district.level.Subscribe(OnLevelChanged);
        }

        void OnLevelChanged(int level)
        {
            if (level > 0)
            {
                FingerPointer.Instance.Reset();
                stateMachine.FinishStep();
            }
        }

        protected override void OnExit()
        {
            BlockerHUD.Instance.Hide();
            
            building.RemoveRaycaster();
            buy.RemoveRaycaster();
            
            disposable.Dispose();
        }
    }
}