﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bedivere.AnimatorStateMachine;
using Joyseed.IdleKingdom.UI;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TutorialFirstDistrict_01 : BDVStateMachineBehaviour<TutorialFirstDistrict>
    {
        private CompositeDisposable disposables;

        protected override void OnEnter()
        {
            SequenceSystem.Instance.RunSequence(stateMachine.sequences[0], () => stateMachine.FinishStep());
        }

        protected override void OnExit()
        {
        }
    }
}