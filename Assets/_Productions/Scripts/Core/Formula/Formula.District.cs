﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Joyseed.IdleKingdom
{
    public static partial class Formula
    {
        public static double GetProfit(this District d, double externalProfitModifier)
        {
            return d.level.Value * d.InitRevenue * d.profitMultiplier * externalProfitModifier;
        }

        public static int GetUpgradeAmount(this District d, BuyMultiplier mult)
        {
            int buyAmount = CalculateBuyAmount(d, mult);

            if (mult == BuyMultiplier.Max && buyAmount <= 0) {
                buyAmount = 1;
            }

            return buyAmount;
        }

        public static double GetBuyPrice(this District d)
        {
            return d.InitCost * d.discountMultiplier;
        }

        public static double GetUpgradePriceByAmount(this District d, int buyAmount)
        {
            var price = d.InitCost * ((Math.Pow(d.Coefficient, d.level.Value) * ((Math.Pow(d.Coefficient, buyAmount) - 1)) / (d.Coefficient - 1)));
            price *= d.discountMultiplier;
            
            return price;
        }

        static int CalculateBuyAmount(District d, BuyMultiplier mult)
        {
            int amount = 1;

            if (mult == BuyMultiplier.Single) 
            {
                amount = 1;
            }
            else if (mult == BuyMultiplier.Ten)
            {
                amount = 10;
            }
            else if (mult == BuyMultiplier.Hundred)
            {
                amount = 100;
            }
            else if (mult == BuyMultiplier.Max)
            {
                double blog = (Inventory.Instance.coins.Value * (d.Coefficient - 1)) / (d.InitCost * (Math.Pow(d.Coefficient, d.level.Value))) + 1;
                var maxCount = (int)Math.Floor(Math.Log10(blog) / Math.Log10(d.Coefficient));

                amount = maxCount;
            }
            else if (mult == BuyMultiplier.Next)
            {
                amount = Mathf.Max(0, d.nextMilestone.Value - d.level.Value);
            }

            return amount;
        }
    }
}
