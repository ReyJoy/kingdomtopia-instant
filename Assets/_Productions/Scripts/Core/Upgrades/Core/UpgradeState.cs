﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class UpgradeState : DataState<UpgradeState>
    {
        public List<string> purchasedItems;
        
        private Subject<Unit> onItemPurchased;
        public IObservable<Unit> OnItemPurchased() { return onItemPurchased ?? (onItemPurchased = new Subject<Unit>());}

        public UpgradeState()
        {
            purchasedItems = new List<string>();
        }

        public override void InitValues()
        {
            purchasedItems = new List<string>();
            
            if (onItemPurchased != null) { onItemPurchased.OnNext(Unit.Default); }
        }

        public bool IsPurchased(string id)
        {
            return purchasedItems.Contains(id);
        }

        public void Purchase(string id)
        {
            purchasedItems.Add(id);

            if (onItemPurchased != null) { onItemPurchased.OnNext(Unit.Default); }
        }
    }
}