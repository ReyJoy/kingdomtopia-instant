﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace Joyseed.IdleKingdom
{
    public class UpgradeManager : Singleton<UpgradeManager>
    {
        public DistrictManager districtManager;
        public BenefitManager benefitManager;
        public List<UpgradeConfig> mayorConfigs = new List<UpgradeConfig>();
        [Title ("Injectables")]
        public UpgradeState state = new UpgradeState();

        //public RenovationCollection renovation;
        public MayorCollection mayor;
        //public HeirloomUpgradeCollection heirloomUpgrade;

        private void Awake()
        {
            state.InitValues();
            //renovation.Construct(districtManager, benefitManager);
            mayor.Construct(districtManager, benefitManager);
            //heirloomUpgrade.Construct(districtManager, benefitManager);
        }
        
        public void Start()
        {
            //renovation.Initialize();
            mayor.Initialize();
            //heirloomUpgrade.Initialize();
        }
        
        public void Rebuild()
        {
            //renovation.Rebuild();
            mayor.Rebuild();
            //heirloomUpgrade.Rebuild();
        }

        public UpgradeCollection GetCollectionByName(string name)
        {
            switch (name)
            {
                //case "Renovation":
                    //return renovation;
                case "Mayor":
                    return mayor;
                //case "HeirloomUpgrade":
                    //return heirloomUpgrade;
                default:
                    return null;
            }
        }
    }
}