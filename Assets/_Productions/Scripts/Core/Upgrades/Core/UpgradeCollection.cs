﻿using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace Joyseed.IdleKingdom
{
    public abstract class UpgradeCollection<T> : UpgradeCollection where T : UpgradeConfig
    {
        public UpgradeCollection Construct(DistrictManager districtManager, BenefitManager benefitManager)
        {
            this.districtManager = districtManager;
            this.benefitManager = benefitManager;

            //quickBuy.Construct(this);
            return this;
        }

        public void Initialize()
        {
            Initialize<T>();

            //quickBuy.Initialize();
        }
    }

    public class UpgradeCollection : MonoBehaviour
    {
        [Title ("Injectables")]
        [ReadOnly, ShowInInspector] protected UpgradeState state;
        protected DistrictManager districtManager;
        protected BenefitManager benefitManager;

        [Title ("Parameters")]
        public virtual TransactionType transactionType { get { return TransactionType.Coins; }}

        [Title ("Variables")]
        public List<UpgradeConfig> items = new List<UpgradeConfig>();
        public List<UpgradeConfig> availableItems = new List<UpgradeConfig>();
        public List<string> PurchasedItems => state.purchasedItems;

        [Title ("Streams")]
        private Subject<List<UpgradeConfig>> onPurchased;

        [Title ("Components")]
        public QuickBuyUpgrade quickBuy;

        public IObservable<List<UpgradeConfig>> OnPurchased() { return onPurchased ?? (onPurchased = new Subject<List<UpgradeConfig>>()); }

        protected void Initialize<T>() where T : UpgradeConfig
        {
            state = UpgradeManager.Instance.state;
            if (typeof(T) == typeof(MayorConfig))
            {
                items = UpgradeManager.Instance.mayorConfigs;
            }

            InitItems();
            //ApplyPurchasedUpgrades();
        }

        protected void InitItems()
        {
            CalculateAvailableItems();
        }

        protected void CalculateAvailableItems()
        {
            availableItems = items.Where(upg => !state.purchasedItems.Any(id => id == upg.Id)).ToList();
        }

        protected void ApplyPurchasedUpgrades()
        {
            var purchased = items.ToList();
            UnlockUpgrade(purchased);
        }

        public void Purchase(UpgradeConfig upgrade)
        {
            Purchase(new List<UpgradeConfig>() {upgrade});
        }

        public void Purchase(List<UpgradeConfig> upgrades)
        {
            List<UpgradeConfig> purchased = new List<UpgradeConfig>();

            for (int i = 0; i < upgrades.Count; i++)
            {
                var upg = upgrades[i];

                if (availableItems.Contains(upg) && !IsPurchased(upg) && Inventory.Instance.CanSpend(upg.CostType, upg.UpgradeCost))
                {
                    Inventory.Instance.TrySpend(upg.CostType, upg.UpgradeCost);
                    
                    state.Purchase(upg.Id);
                    purchased.Add(upg);
                }
            }

            UnlockUpgrade(purchased);
        }

        public bool IsPurchased(UpgradeConfig config)
        {
            return state.IsPurchased(config.Id);
        }

        void UnlockUpgrade(List<UpgradeConfig> unlockedItems)
        {
            for (int i = 0; i < unlockedItems.Count; i++)
            {
                benefitManager.GiveBenefit(unlockedItems[i].benefit);
            }

            CalculateAvailableItems();

            if (onPurchased != null) onPurchased.OnNext(unlockedItems);
        }

        public void Rebuild()
        {
            InitItems();
        }
        
        public UpgradeConfig GetUpgrade(string mayorID)
        {
            return items.Single(x => x.Id == mayorID);
        }
        
        public List<UpgradeConfig> GetAvailableUpgrades(int maxCount)
        {
            return availableItems.Take(maxCount).ToList();
        }
    }
}
