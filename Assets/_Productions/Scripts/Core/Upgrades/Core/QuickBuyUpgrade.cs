﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace Joyseed.IdleKingdom
{
    public abstract class QuickBuyUpgrade : MonoBehaviour
    {
        [ReadOnly] public DoubleReactiveProperty quickBuyCost;
        [ReadOnly] public DoubleReactiveProperty currency;

        [ReadOnly, ShowInInspector] public List<UpgradeConfig> quickBuyItems = new List<UpgradeConfig>();
        protected List<UpgradeConfig> availableUpgrades { get { return collection.availableItems; }}
        
        protected UpgradeCollection collection;

        public void Construct(UpgradeCollection collection)
        {
            this.collection = collection;
        }

        public virtual void Initialize()
        {
            currency = Inventory.Instance.GetCurrency(collection.transactionType);
        }

        public abstract void PurchaseAll();
    }
}