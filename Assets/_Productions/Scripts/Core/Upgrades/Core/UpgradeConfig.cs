﻿using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class UpgradeConfig : ScriptableObject, IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public double costBase;
        [ReadOnly] public double costExponent;
        [ReadOnly] public string transactionType;

        [ReadOnly] public string benefitTarget;
        [ReadOnly] public string benefitType;
        [ReadOnly] public float benefitModifier;

        [ReadOnly] public float targetCount;
        
        private BenefitConfig _benefit;
        private TransactionType _costType;

        public string dataID { get {return Id; }}
        public BenefitConfig benefit 
        {
            get 
            {
                if (_benefit == null) {
                    _benefit = new BenefitConfig(benefitTarget, (BenefitType)System.Enum.Parse(typeof(BenefitType), benefitType), benefitModifier);
                }

                return _benefit;
            }
        }

        private double upgradeCost = -1;
        public double UpgradeCost 
        { 
            get 
            {
                if (upgradeCost < 0) {
                    upgradeCost = costBase * Math.Pow(10, costExponent);
                }

                return upgradeCost;
            }
        }

        public TransactionType CostType
        {
            get
            {
                if (_costType == TransactionType.None) {
                    _costType = (TransactionType)System.Enum.Parse(typeof(TransactionType), transactionType);
                }
                
                return _costType;
            }
        }

        public override string ToString()
        {
            return $"Upgrade: id {Id}, type {benefitType}, target {benefitTarget}, value {benefitModifier}, cost {UpgradeCost})";
        }
    }
    
    [System.Serializable]
    public class UpgradeConfigMock : IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public double costBase;
        [ReadOnly] public double costExponent;
        [ReadOnly] public string transactionType;

        [ReadOnly] public string benefitTarget;
        [ReadOnly] public string benefitType;
        [ReadOnly] public float benefitModifier;

        [ReadOnly] public float targetCount;
        
        private BenefitConfig _benefit;
        private TransactionType _costType;

        public string dataID { get {return Id; }}
        public BenefitConfig benefit 
        {
            get 
            {
                if (_benefit == null) {
                    _benefit = new BenefitConfig(benefitTarget, (BenefitType)System.Enum.Parse(typeof(BenefitType), benefitType), benefitModifier);
                }

                return _benefit;
            }
        }

        private double upgradeCost = -1;
        public double UpgradeCost 
        { 
            get 
            {
                if (upgradeCost < 0) {
                    upgradeCost = costBase * Math.Pow(10, costExponent);
                }

                return upgradeCost;
            }
        }

        public TransactionType CostType
        {
            get
            {
                if (_costType == TransactionType.None) {
                    _costType = (TransactionType)System.Enum.Parse(typeof(TransactionType), transactionType);
                }
                
                return _costType;
            }
        }
    }

    [System.Serializable]
    public struct UpgradeScriptables : IMockCollection
    {
        public UpgradeConfigMock[] UpgradeConfigRow;
        public IDataModel[] Rows { get { return UpgradeConfigRow; }}
    }
}