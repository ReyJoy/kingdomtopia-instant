using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class MayorConfig : UpgradeConfig
    {
    }
    
    [System.Serializable]
    public class MayorConfigMock : UpgradeConfigMock
    {
    }

    [System.Serializable]
    public struct MayorScriptables : IMockCollection
    {
        public MayorConfigMock[] MayorConfigRow;
        public IDataModel[] Rows { get { return MayorConfigRow; }}
    }
}