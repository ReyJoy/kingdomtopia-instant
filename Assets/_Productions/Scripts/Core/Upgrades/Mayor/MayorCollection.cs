﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class MayorCollection : UpgradeCollection<MayorConfig>
    {
        public override TransactionType transactionType { get { return TransactionType.Coins; }}
    }
}