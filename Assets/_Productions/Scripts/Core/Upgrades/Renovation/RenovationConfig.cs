﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class RenovationConfig : UpgradeConfig
    {
    }
    
    [System.Serializable]
    public class RenovationConfigMock : UpgradeConfigMock
    {
    }

    [System.Serializable]
    public struct RenovationScriptables : IMockCollection
    {
        public RenovationConfigMock[] RenovationConfigRow;
        public IDataModel[] Rows { get { return RenovationConfigRow; }}
    }
}
