﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class RenovationCollection : UpgradeCollection<RenovationConfig>
    {
        public override TransactionType transactionType { get { return TransactionType.Coins; }}
    }
}