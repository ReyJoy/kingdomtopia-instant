﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class HeirloomUpgradeConfig : UpgradeConfig
    {
    }
    
    [System.Serializable]
    public class HeirloomUpgradeConfigMock : UpgradeConfigMock
    {
    }

    [System.Serializable]
    public struct HeirloomUpgradeScriptables : IMockCollection
    {
        public HeirloomUpgradeConfigMock[] HeirloomUpgradeConfigRow;
        public IDataModel[] Rows { get { return HeirloomUpgradeConfigRow; }}
    }
}
