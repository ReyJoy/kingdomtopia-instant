﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class CoinView : MonoBehaviour
    {
        public TextMeshProUGUI coinText;

        private void OnEnable()
        {
            Inventory.Instance.coins.TakeUntilDisable(this).Subscribe(OnCoinsChanged);
        }

        private void OnCoinsChanged(double value)
        {
            coinText.text = value.ToAnnotated();
        }
    }
}

