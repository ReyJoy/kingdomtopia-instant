﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public enum TransactionType
    {
        None,
        Coins,
        Gems,
        Heirloom,
        ClaimedHeirloom
    }
}
