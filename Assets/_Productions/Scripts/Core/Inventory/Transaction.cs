﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class Transaction
    {
        public TransactionType type;
        public double amount;

        public Transaction()
        {
            type = TransactionType.None;
            amount = 0.0;
        }

        public Transaction(TransactionType type, double amount)
        {
            this.type = type;
            this.amount = amount;
        }
    }
}
