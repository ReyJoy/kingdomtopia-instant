﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class InventoryState : DataState<InventoryState>
    {
        public DoubleReactiveProperty coins = new DoubleReactiveProperty(0);
        public DoubleReactiveProperty gems = new DoubleReactiveProperty(0);
        public DoubleReactiveProperty claimedHeirloom = new DoubleReactiveProperty(0);
        public DoubleReactiveProperty freeHeirloom = new DoubleReactiveProperty(0);

        public InventoryState()
        {
            coins = new DoubleReactiveProperty(0);
            gems = new DoubleReactiveProperty(0);
            claimedHeirloom = new DoubleReactiveProperty(0);
            freeHeirloom = new DoubleReactiveProperty(0);
        }

        public override void InitValues()
        {
            coins.Value = 4;
        }
    }
}