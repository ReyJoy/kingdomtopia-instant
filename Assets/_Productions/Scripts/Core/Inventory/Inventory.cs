﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class Inventory : Singleton<Inventory>
    {
        public InventoryState state;

        public DoubleReactiveProperty coins { get { return state.coins; } }
        public DoubleReactiveProperty gems { get { return state.gems; } }
        public DoubleReactiveProperty claimedHeirloom { get { return state.claimedHeirloom; }}
        public DoubleReactiveProperty freeHeirloom { get { return state.freeHeirloom; }}
        
        double _tmpValue;
        Transaction _transaction = new Transaction();

        Subject<Transaction> onSpend, onGive;

        public IObservable<Transaction> OnSpend() { return onSpend ?? (onSpend = new Subject<Transaction>()); }
        public IObservable<Transaction> OnGive() { return onGive ?? (onGive = new Subject<Transaction>()); }

        protected override void Awake()
        {
            base.Awake();
            state.InitValues();
        }

        public void Rebuild()
        {
            state.InitValues();
        }
        
        public DoubleReactiveProperty GetCurrency(TransactionType type)
        {
            var property = coins;
            switch (type)
            {
                case TransactionType.Coins:
                    property = coins;
                    break;

                case TransactionType.Gems:
                    property = gems;
                    break;

                case TransactionType.Heirloom:
                    property = freeHeirloom;
                    break;
                
                case TransactionType.ClaimedHeirloom:
                    property = claimedHeirloom;
                    break;;
            }

            return property;
        }

        public bool CanSpend(TransactionType type, double amount)
        {
            return GetCurrency(type).Value >= amount;
        }

        public void TrySpend(TransactionType type, double amount)
        {
            if (!CanSpend(type, amount))
            {
                return;
            }

            Spend(type, amount);
        }

        void Spend(TransactionType type, double amount)
        {
            AddAmountToCurrency(GetCurrency(type), -amount);

            if (onSpend != null) 
            {
                _transaction.type = type;
                _transaction.amount = amount;

                onSpend.OnNext(_transaction);
            }
        }

        public void Give(TransactionType type, double amount)
        {
            AddAmountToCurrency(GetCurrency(type), amount);

            if (onGive != null) 
            {
                _transaction.type = type;
                _transaction.amount = amount;

                onGive.OnNext(_transaction);
            }
        }

        double _amt;
        void AddAmountToCurrency(DoubleReactiveProperty currency, double amount)
        {
            _amt = currency.Value + amount;
            _amt.ClampLimit();

            currency.Value = _amt;
        }
    }
}