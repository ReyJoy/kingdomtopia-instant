﻿namespace Joyseed.IdleKingdom
{
    public enum BenefitType
    {
        Auto,
        Profit,
        Speed,
        Gem,
        Mega,
        Legacy,
        LevelUp,
        Discount
    }

    public static class BenefitTypeExtension
    {
        public static bool IsCurrencyType(this BenefitType type)
        {
            return (type == BenefitType.Gem) || (type == BenefitType.Mega);
        }

        public static bool IsDistrictType(this BenefitType type)
        {
            bool flag = false;

            if (type == BenefitType.Auto ||
                type == BenefitType.Profit ||
                type == BenefitType.Speed ||
                type == BenefitType.Discount ||
                type == BenefitType.LevelUp) 
                {
                    flag = true;
                }

            return flag;
        }

        public static DistrictParameterType ToDistrictParameterType(this BenefitType type)
        {
            DistrictParameterType districtType = DistrictParameterType.None;

            switch (type)
            {
                case BenefitType.Profit:
                districtType = DistrictParameterType.Profit;
                break;

                case BenefitType.Discount:
                districtType = DistrictParameterType.Price;
                break;

                case BenefitType.Speed:
                districtType = DistrictParameterType.Speed;
                break;
                
                case BenefitType.LevelUp:
                districtType = DistrictParameterType.LevelUp;
                break;
            }

            return districtType;
        }
    }
}