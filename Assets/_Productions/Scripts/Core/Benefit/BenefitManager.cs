﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class BenefitManager : MonoBehaviour
    {
        public DistrictManager districtManager;

        public void GiveBenefit(BenefitConfig benefit)
        {
            // Debug.Log($"<color=cyan>Applying benefit : {benefit}</color>");
            if (benefit.type.IsDistrictType()) 
            {
                if (benefit.target == "district_all") {
                    GiveBenefitAllDistrict(benefit.type, benefit.amount);
                }
                else {
                    District district = districtManager.GetDistrictById(benefit.target);
                    GiveBenefitToDistrict(district, benefit.type, benefit.amount);
                }
            }
            else if (benefit.type.IsCurrencyType())
            {
                GiveCurrency(benefit.type, benefit.amount);
            }
        }

        void GiveBenefitToDistrict(District district, BenefitType type, float amount)
        {
            if (!type.IsDistrictType()) {
                Debug.LogError($"Type {type} is not for district.");
                return;
            }

            // Debug.Log($"<color=cyan>Benefit for {district.Id} : type {type}, amount : {amount}</color>");

            if (type == BenefitType.Auto) {
                district.GiveManager();
            }
            else {
                district.AddMultiplier(type.ToDistrictParameterType(), amount);
            }
        }

        public void GiveBenefitAllDistrict(BenefitType type, float amount)
        {
            var districts = districtManager.districts;

            for (int i = 0; i < districts.Count; i++)
            {
                GiveBenefitToDistrict(districts[i], type, amount);
            }
        }

        void GiveCurrency(BenefitType type, float amount)
        {
            Debug.Log($"Benefit inventory: type {type}, amt {amount}");
            switch (type)
            {
                case BenefitType.Gem:
                    Inventory.Instance.Give(TransactionType.Gems, amount);
                    break;
            }      
        }
    }
}