﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class TopUIView : Singleton<TopUIView>
    {
        public MissionProgressView missionProgressView;
        public PlayerLevelView playerLevelView;
        public InstallButton installButtonView;

        public void ShowPlayerLevel()
        {
            playerLevelView.gameObject.SetActive(true);
        }

        public void ShowInstallButton()
        {
            installButtonView.gameObject.SetActive(true);
        }
    } 
}