﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using TMPro;

namespace Joyseed.IdleKingdom
{
    public class BottomUIView : Singleton<BottomUIView>
    {
        public TabbedSelectionView tabbedSelectionView;

        public void Start()
        {
            tabbedSelectionView.Initialize();

            BottomUIMenu.Instance.unlockUpgradeMenu.TakeUntilDestroy(this).Subscribe(UnlockUpgradeMenu);
            BottomUIMenu.Instance.unlockOfferingMenu.TakeUntilDestroy(this).Subscribe(UnlockOfferingMenu);
        }

        private void UnlockUpgradeMenu(bool unlock)
        {
            tabbedSelectionView.items[0].Unlock(unlock);
        }

        private void UnlockOfferingMenu(bool unlock)
        {
            tabbedSelectionView.items[2].Unlock(unlock);
        }
    }
}