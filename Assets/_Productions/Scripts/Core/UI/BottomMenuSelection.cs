﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public enum BottomMenuSelection
    {
        Kingdom,
        Statistics,
        Upgrades,
        Store,
        Rebuild
    }
}