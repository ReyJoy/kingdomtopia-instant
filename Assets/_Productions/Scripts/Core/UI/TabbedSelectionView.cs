﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using DG.Tweening;
using Sirenix.OdinInspector;
using System;

namespace Joyseed.IdleKingdom
{
    public class TabbedSelectionView : MonoBehaviour
    {
        [Title ("Components")]
        public RectTransform contentArea;
        public HorizontalOrVerticalLayoutGroup horizontalLayout;
        public RectTransform layoutGroupRect;
        public RectTransform selector;

        [Title ("Parameters")]
        public bool isResetWhenActiveClicked;
        public int defaultIndex;

        [Title ("Items")]
        public TabbedSelectionItemView[] items;

        [ShowInInspector, ReadOnly] TabbedSelectionItemView activeSelection;

        int indexer { get { return items.Length / 2; }}
        float shrinkSize { get { return (contentArea.rect.size.x - expandSize) / (float)(items.Length - 1); }}
        float expandSize { get { return selector.rect.width; }}

        Subject<int> onSwitch;

        void Start()
        {
            // Initialize();
        }

        public void Initialize()
        {
            InitItems();
        }

        void InitItems()
        {
            for (int i = 0; i < items.Length; i++)
            {
                var sizeDelta = items[i].rectTransform.sizeDelta;
                var idx = i;
                var item = items[i];

                item.Initialize(idx, shrinkSize, expandSize);
                item.button.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ => OnItemClicked(item));
            }

            Reset();
        }

        void OnItemClicked(TabbedSelectionItemView item)
        {
            if (activeSelection != item) 
            {
                Switch(item);
            }
            else 
            {
                if (isResetWhenActiveClicked)
                {
                    Reset();
                }
            }
        }

        void Switch(TabbedSelectionItemView item)
        {
            selector.DOAnchorPosX(shrinkSize * (item.index - indexer), 0.5f);
            StartCoroutine(RelayoutForce());

            for (int i = 0; i < items.Length; i++)
            {
                if (items[i] == item) {
                    item.Expand();
                }
                else {
                    items[i].Shrink();
                }
            }

            activeSelection = item;
            if (onSwitch != null) onSwitch.OnNext(item.index);
        }

        public void Reset()
        {
            Switch(items[defaultIndex]);
        }

        IEnumerator RelayoutForce()
        {
            horizontalLayout.childControlWidth = false;

            var time = Time.time;

            while (Time.time - time < 0.5f)
            {
                yield return new WaitForEndOfFrame();
                LayoutRebuilder.ForceRebuildLayoutImmediate(layoutGroupRect);
            }
        }

        public IObservable<int> OnSwitch() { return onSwitch ?? (onSwitch = new Subject<int>()); }
    }
}