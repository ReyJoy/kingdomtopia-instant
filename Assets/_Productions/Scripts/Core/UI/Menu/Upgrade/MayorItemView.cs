﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Spine.Unity;
using TMPro;

namespace Joyseed.IdleKingdom
{
    public class MayorItemView : BaseUpgradeCollectionItemView
    {
        public int districtIndex;
        public Image districtIcon;
        public TextMeshProUGUI districtName;
        public TextMeshProUGUI hireCost;
        public Button hireButton;
        public GameObject hireButtonBG;
        public GameObject assignedItem;
        public SkeletonGraphic skeletonGraphic;
        public Material normalMat, grayscaleMat;

        private District district;

        public override void Initialize(int index)
        {
            base.Initialize(index);
            district = DistrictManager.Instance.districts[districtIndex];
            InitView();
            InitStream();
        }

        void InitView()
        {
            //skeletonGraphic.skeletonDataAsset = upgrade.benefit.GetSpineAsset();
            //skeletonGraphic.Initialize(true);

            districtIcon.sprite = district.Icon;
            
            districtName.text = district.DistrictName;

            hireCost.text = upgrade.UpgradeCost.ToAnnotated();

            UpdatePurchasedState(UpgradeManager.Instance.mayor.IsPurchased(upgrade));
        }

        void InitStream()
        {
            Inventory.Instance.coins.TakeUntilDestroy(this).Subscribe(OnCoinsChanged);
            UpgradeManager.Instance.mayor.OnPurchased().TakeUntilDestroy(this).Subscribe(OnItemUnlocked);
            hireButton.OnClickAsObservable().TakeUntilDestroy(this)
                .Subscribe(_ => UpgradeManager.Instance.mayor.Purchase(upgrade));
        }

        void OnItemUnlocked(List<UpgradeConfig> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (upgrade.Id == items[i].Id)
                {
                    UpdatePurchasedState(true);
                }
            }
        }

        void UpdatePurchasedState(bool isPurchased)
        {   
            hireButtonBG.SetActive(!isPurchased);
            hireButton.gameObject.SetActive(!isPurchased);
            assignedItem.SetActive(isPurchased);

            skeletonGraphic.material = isPurchased ? normalMat : grayscaleMat;
            skeletonGraphic.SetMaterialDirty();

            skeletonGraphic.startingAnimation = isPurchased ? "masking animation" : "masking idle";

            skeletonGraphic.Initialize(true);
        }

        void OnCoinsChanged(double coins)
        {
            if (hireButton.gameObject.activeSelf)
            {
                hireButton.interactable = coins >= upgrade.UpgradeCost;
            }
        }
    }
}