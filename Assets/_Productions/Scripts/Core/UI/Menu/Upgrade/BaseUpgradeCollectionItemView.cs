﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public abstract class BaseUpgradeCollectionItemView : MonoBehaviour
    {
        [Title ("Injectables")]        
        protected UpgradeConfig upgrade;
        protected TransactionType transactionType;

        [Title ("Components")]
        public Button upgradeButton;

        [Title ("Parameters")]
        protected int index;

        public void Construct(UpgradeConfig upgrade, TransactionType transactionType)
        {
            this.upgrade = upgrade;
            this.transactionType = transactionType;
        }

        public virtual void Initialize(int index)
        {
            this.index = index;
        }
    }
}