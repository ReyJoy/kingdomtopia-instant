﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class MayorView : BaseUpgradeCollectionView
    {
        public override List<UpgradeConfig> UpgradesToDisplay
        {
            get
            {
                return upgradeCollection.items;
            }
        }

        private void Start()
        {
            Construct(UpgradeManager.Instance.mayor);
        }
    }
}