﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using UniRx;
using System;
using UnityEngine.UI;
using UnityEngine.UIElements;

namespace Joyseed.IdleKingdom
{
    public class BaseUpgradeCollectionView : MonoBehaviour
    {
        [Title ("Parameters")]
        public int maxItemsDisplay;

        public float heightWithoutQuickBuy;
        public float heightWithQuickBuy;

        [Title ("Components")]
        public UpgradeQuickBuyView quickBuyView;
        public ScrollRect scrollRect;

        [Title ("Variables")]
        public List<BaseUpgradeCollectionItemView> items;
        private CompositeDisposable disposables;
        [ReadOnly, ShowInInspector] protected UpgradeCollection upgradeCollection;

        public virtual List<UpgradeConfig> UpgradesToDisplay
        {
            get
            {
                return upgradeCollection.GetAvailableUpgrades(maxItemsDisplay);
            }
        }

        [Title ("Streams")]
        Subject<UpgradeConfig> onClicked = new Subject<UpgradeConfig>();
        public IObservable<UpgradeConfig> OnClicked() { return onClicked ?? (onClicked = new Subject<UpgradeConfig>());}

        public void Construct(UpgradeCollection upgradeCollection)
        {
            this.upgradeCollection =  upgradeCollection;
            Initialize();
            //quickBuyView.Construct(upgradeCollection.quickBuy);
        }

        public void UnlockQuickbuy(bool unlocked)
        {
            quickBuyView.gameObject.SetActive(unlocked);

            var rect = scrollRect.GetComponent<RectTransform>();
            if (unlocked)
            {
                rect.offsetMax = new Vector2(rect.offsetMax.x, -heightWithQuickBuy);
            }
            else
            {
                rect.offsetMax = new Vector2(rect.offsetMax.x, -heightWithoutQuickBuy);
            }
            
        }

        public void Initialize()
        {
            RefreshItems();

            //quickBuyView.Initialize();
        }

        protected void RefreshItems()
        {
            if (disposables != null && !disposables.IsDisposed)
            {
                disposables.Dispose();
            }

            disposables = new CompositeDisposable();

            var upgrades = UpgradesToDisplay;

            for (int i = 0; i < maxItemsDisplay; i++)
            {
                var idx = i;
                var item = items[i];

                if (idx < upgrades.Count)
                {
                    var upg = upgrades[i];

                    item.gameObject.SetActive(true);
                    
                    item.Construct(upg, upg.CostType);
                    item.Initialize(idx);
                    
                    item.upgradeButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ =>
                    {
                        if (onClicked != null) onClicked.OnNext(upg);
                    }).AddTo(disposables);
                }
                else
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
    }
}
