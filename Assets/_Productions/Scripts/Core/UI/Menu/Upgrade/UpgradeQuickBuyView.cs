﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using UniRx;
using System;

namespace Joyseed.IdleKingdom
{
    public class UpgradeQuickBuyView : MonoBehaviour
    {
        [Title ("Components")]
        public TextMeshProUGUI upgradeTitle;
        public TextMeshProUGUI upgradeDesc;
        public Button upgradeButton;
        public TextMeshProUGUI price;

        private QuickBuyUpgrade quickBuy;
        private Subject<QuickBuyUpgrade> onClicked;

        public void Construct(QuickBuyUpgrade quickBuy)
        {
            this.quickBuy = quickBuy;
        }

        public void Initialize()
        {
            quickBuy.currency.TakeUntilDisable(this).Subscribe(_ => UpdateInteractable());
            quickBuy.quickBuyCost.TakeUntilDisable(this).Subscribe(_ => OnQuickBuyCostChanged());
            
            upgradeButton.OnClickAsObservable().TakeUntilDisable(this).Subscribe(_ => OnUpgradeClicked());
        }

        void UpdateInteractable()
        {
            if (quickBuy.quickBuyCost.Value > 0) {
                upgradeButton.interactable = quickBuy.currency.Value >= quickBuy.quickBuyCost.Value;
            }
            else {
                upgradeButton.interactable = false;
            }
        }

        void OnQuickBuyCostChanged()
        {
            price.text = quickBuy.quickBuyCost.Value.ToAnnotated();
            UpdateInteractable();
        }

        void OnUpgradeClicked()
        {
            if (onClicked != null) onClicked.OnNext(quickBuy);
        }

        public IObservable<QuickBuyUpgrade> OnClicked() { return onClicked ?? (onClicked = new Subject<QuickBuyUpgrade>()); }
    }
}