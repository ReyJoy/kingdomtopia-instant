﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class BottomUIMenu : Singleton<BottomUIMenu>
    {
        public BottomUIView view;

        public BoolReactiveProperty unlockUpgradeMenu = new BoolReactiveProperty(false);
        public BoolReactiveProperty unlockOfferingMenu = new BoolReactiveProperty(false);
    
        private Menu activeBottomMenu;

        public void Start()
        {
            SubscribeListeners();
        }

        void SubscribeListeners()
        {
            view.tabbedSelectionView.OnSwitch().TakeUntilDisable(this).Subscribe(OnBottomMenuSwitch);
        }


        void OnBottomMenuSwitch(int index)
        {
            if (activeBottomMenu != null)
            {
                activeBottomMenu.CloseMenu();
            }

            switch (index)
            {
                case 0:
                    activeBottomMenu = UpgradeMenu.Open();
                    break;
                case 1:
                    activeBottomMenu = null;
                    break;
                case 2:
                    activeBottomMenu = null;
                    break;
            }

            
        }

        public void ClearSelection()
        {
            view.tabbedSelectionView.Reset();
        }
    }
}
