﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Sirenix.OdinInspector;

namespace Joyseed.IdleKingdom
{
    public class BottomUIItemView : TabbedSelectionItemView
    {
        [Title ("Parameters")]
        public BottomMenuSelection selection;

        public Sprite deselectedSprite;
        public Sprite focusedSprite;
        public Image lockIcon;

        private Image iconImage;
        
        public override void Initialize(int index, float shrinkSize, float expandSize)
        {
            base.Initialize(index, shrinkSize, expandSize);

            text.text = selection.ToString();
            iconImage = icon.GetComponent<Image>();
        }

        protected override void SetExpandText(bool isExpand)
        {
            text.gameObject.SetActive(isExpand);
        }

        protected override void SetExpandIcon(bool isExpand)
        {
            base.SetExpandIcon(isExpand);
            if (icon != null)
            {
                if (isExpand)
                {
                    iconImage.sprite = focusedSprite;
                }
                else
                {
                    iconImage.sprite = deselectedSprite;
                }
            }
            
        }

        public override void Unlock(bool unlocked)
        {
            base.isUnlocked.Value = unlocked;
            
            lockIcon.gameObject.SetActive(!unlocked);
            button.interactable = unlocked;
        }
    }
}