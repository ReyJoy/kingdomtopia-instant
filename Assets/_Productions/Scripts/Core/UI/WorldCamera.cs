﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class WorldCamera : Singleton<WorldCamera>
    {
        public Camera worldCamera;
        public Camera worldTutorialCamera;
    }

}