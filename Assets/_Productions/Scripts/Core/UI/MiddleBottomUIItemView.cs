﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

namespace Joyseed.IdleKingdom.UI.Royale
{
    public class MiddleBottomUIItemView : BottomUIItemView
    {
        public Button multiplier;
        public TextMeshProUGUI multiplierText;

        private bool isExpanded;

        public override void Expand()
        {
            base.Expand();
            isExpanded = true;
        }

        public override void Shrink()
        {
            base.Shrink();
            isExpanded = false;
        }
    }
}