﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using Sirenix.OdinInspector;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class TabbedSelectionItemView : MonoBehaviour
    {
        [Title ("Components")]
        public RectTransform rectTransform;
        public Button button;
        public RectTransform icon;
        public int index;
        public TextMeshProUGUI text;
        public TextMeshProUGUI textMesh;
        public Image background, beveledBackground;

        [Title ("Variables")]
        public float tweenTime = 0.5f;
        public Color expandColor, shrinkColor, disabledTextColor;
        public Color backgroundDefaultColor, beveledBackgroundDefaultColor;
        public Color disabledBackgroundColor, disabledBeveledColor;
        [ReadOnly] public BoolReactiveProperty isUnlocked = new BoolReactiveProperty(true);

        private float shrinkSize = 195f;
        private float expandSize = 300f;
        private Vector2 iconExpandSize = new Vector2(150, 150);
        private Vector2 iconShrinkSize = new Vector2(125, 125);
        private float iconExpandPosY = 50f;
        private float iconShrinkPosY = 12f;
        private float textShrinkSize = 32f;
        private float textExpandSize = 40f;


        public virtual void Initialize(int index, float shrinkSize, float expandSize)
        {
            this.index = index;
            this.shrinkSize = shrinkSize;
            this.expandSize = expandSize;
        }

        public virtual void Expand()
        {
            SetExpandView(true);
        }

        public virtual void Shrink()
        {
            SetExpandView(false);
        }

        void SetExpandView(bool isExpand)
        {
            if (isUnlocked.Value)
            {
                SetExpandRect(isExpand);
                SetExpandIcon(isExpand);
                SetExpandText(isExpand);
                SetExpandColor(isExpand);
            }
        }

        void SetExpandRect(bool isExpand)
        {
            var sizeToTween = isExpand ? expandSize : shrinkSize;

            var sizeDelta = rectTransform.sizeDelta;
            sizeDelta.x = sizeToTween;
            rectTransform.DOSizeDelta(sizeDelta, tweenTime);
        }

        protected virtual void SetExpandIcon(bool isExpand)
        {
            if (icon != null)
            {
                var sizeToTween = isExpand ? iconExpandSize : iconShrinkSize;
                icon.DOSizeDelta(sizeToTween, tweenTime);

                var posToTween = isExpand ? iconExpandPosY : iconShrinkPosY;
                icon.DOAnchorPosY(posToTween, tweenTime);
            }
        }

        protected virtual void SetExpandText(bool isExpand)
        {
            var sizeToTween = isExpand ? textExpandSize : textShrinkSize;
            //textMesh.DOFontSize(sizeToTween, tweenTime);
            textMesh.fontSize = sizeToTween;

            var colorToChange = isExpand ? expandColor : shrinkColor;
            textMesh.color = colorToChange;
        }

        void SetExpandColor(bool isExpand)
        {
            if (background != null)
            {
                if (isExpand)
                {
                    background.color = shrinkColor;
                    beveledBackground.color = shrinkColor;
                }
                else
                {
                    background.color = backgroundDefaultColor;
                    beveledBackground.color = beveledBackgroundDefaultColor;
                }
            }
        }

        public virtual void Unlock(bool unlocked)
        {
            if (unlocked)
            {
                isUnlocked.Value = true;
                button.interactable = true;
                textMesh.color = shrinkColor;
                background.color = backgroundDefaultColor;
                beveledBackground.color = beveledBackgroundDefaultColor;
            }
            else
            {
                isUnlocked.Value = false;
                button.interactable = false;
                textMesh.color = disabledTextColor;
                background.color = disabledBackgroundColor;
                beveledBackground.color = disabledBeveledColor;
            }
        }
    }
}
