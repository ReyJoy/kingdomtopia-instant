﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class MilestoneState : DataState<MilestoneState>
    {
        public List<string> unlockedOneTimeMilestone;

        Subject<Unit> onAddMilestones;
        public IObservable<Unit> OnAddMilestonesAsObservable() { return onAddMilestones ?? (onAddMilestones = new Subject<Unit>());}

        public MilestoneState()
        {
            unlockedOneTimeMilestone = new List<string>();
        }

        public override void InitValues()
        {

        }

        public void AddMilestone(MilestoneConfig milestone)
        {
            string id = milestone.Id;

            if (!unlockedOneTimeMilestone.Contains(id))
            {
                unlockedOneTimeMilestone.Add(id);
            
                if(onAddMilestones != null) { onAddMilestones.OnNext(Unit.Default); }
            }
        }

        public bool IsUnlocked(MilestoneConfig milestone)
        {
            return unlockedOneTimeMilestone.Contains(milestone.Id);
        }
    }
}