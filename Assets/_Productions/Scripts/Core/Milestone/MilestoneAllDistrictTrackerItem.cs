﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Sirenix.OdinInspector;
using System;

namespace Joyseed.IdleKingdom
{
    public class MilestoneAllDistrictTrackerItem : MonoBehaviour
    {
        [Title ("Injectables")]
        public District district;
        
        [Title ("Variables")]
        public int levelToReach;
        public int level { get { return district.level.Value; }}
        public BoolReactiveProperty hasReached = new BoolReactiveProperty();

        IDisposable stream;

        public void InitializeTrack(int levelToReach)
        {
            this.levelToReach = levelToReach;
            hasReached.Value = false;

            if (stream != null) stream.Dispose();
            stream = district.level.TakeUntilDisable(this).Subscribe(_ => OnDistrictLevelChanged());
        }

        void OnDistrictLevelChanged()
        {
            if (level >= levelToReach)
            {
                hasReached.Value = true;
            }
        }
    }
}