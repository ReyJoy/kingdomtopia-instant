﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using System.Linq;
using System;

namespace Joyseed.IdleKingdom
{
    public class MilestoneManager : Singleton<MilestoneManager>
    {
        [Title ("Components")]
        public AllDistrictMilestoneManager allDistrictMilestoneManager;
        public DistrictManager districtManager;
        public BenefitManager benefitManager;

        [Title("Parameters")]
        [ShowInInspector] public Dictionary<string, List<MilestoneConfig>> milestoneConfigs = new Dictionary<string, List<MilestoneConfig>>();
        public List<Milestone> milestones = new List<Milestone>();
        private MilestoneState state;

        [ReadOnly]public IntReactiveProperty prevAllDistrictMilestone = new IntReactiveProperty(0);
        // [ReadOnly]public IntReactiveProperty nextAllDistrictMilestone = new IntReactiveProperty(0);

        private Subject<MilestoneConfig> onMilestoneUnlocked;
        public IObservable<MilestoneConfig> OnMilestoneUnlocked() { return onMilestoneUnlocked ?? (onMilestoneUnlocked = new Subject<MilestoneConfig>()); }

        public List<MilestoneConfig> configs = new List<MilestoneConfig>();

        public void Start()
        {
            CreateConfigDictionary();
            ConstructMilestones();

            allDistrictMilestoneManager.Initialize();
        }

        void CreateConfigDictionary()
        {
            milestoneConfigs = new Dictionary<string, List<MilestoneConfig>>();

            for (int i = 0; i < configs.Count; i++)
            {
                string districtID = configs[i].district;

                if (!milestoneConfigs.ContainsKey(districtID))
                {
                    milestoneConfigs.Add(districtID, new List<MilestoneConfig>());
                }

                milestoneConfigs[districtID].Add(configs[i]);
            }
        }

        void ConstructMilestones()
        {
            for (int i = 0; i < milestones.Count; i++)
            {
                var district = districtManager.districts[i];
                milestones[i].Initialize();
            }
        }

        public List<MilestoneConfig> GetMilestones(string district)
        {
            return milestoneConfigs[district];
        }

        public List<MilestoneConfig> GetAllMilestones()
        {
            return configs;
        }

        public void Rebuild()
        {
            for (int i = 0; i < milestones.Count; i++)
            {
                milestones[i].Reset();
            }

            allDistrictMilestoneManager.Reset();
        }

        public void Unlock(MilestoneConfig milestone)
        {
            var type = milestone.benefit.type;

            if (type.IsCurrencyType() && state.IsUnlocked(milestone))
            {
                Debug.Log($"Milestone {milestone.Id} with benefit {milestone.benefitModifier} {milestone.benefitType} is already unlocked before!");
                return;
            }

            benefitManager.GiveBenefit(milestone.benefit);

            if (type.IsCurrencyType())
            {
                state.AddMilestone(milestone);
            }

            onMilestoneUnlocked?.OnNext(milestone);
        }
    }
}