﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;
using System;
using UniRx;

namespace Joyseed.IdleKingdom
{
    public class AllDistrictMilestoneManager : MonoBehaviour
    {
        public MilestoneManager milestoneManager;
        public DistrictManager districtManager;
        public BenefitManager benefitManager;

        private List<Milestone> milestones { get { return milestoneManager.milestones; }}

        private MilestoneConfig nextAllDistrictMilestone;
        private List<MilestoneConfig> AllDistrictMilestone { get { return milestoneManager.milestoneConfigs["district_all"]; }}
        private List<MilestoneConfig> allUnlockedMilestones = new List<MilestoneConfig>();

        CompositeDisposable stream;

        public void Initialize()
        {
            LoadUnlockedMilestone();
            InitTracking();
        }

        void InitTracking()
        {
            if (stream != null) stream.Dispose();
            stream = new CompositeDisposable();

            CalculateNextAllDistrictLevel();

            for (int i = 0; i < milestones.Count; i++)
            {
                var tracker = milestones[i].trackerItem;
                tracker.InitializeTrack(nextAllDistrictMilestone.levelReq);
            }

            SubsribeAllDistrictMilestone();
        }

        void CalculateNextAllDistrictLevel()
        {
            var lowestDistrict = districtManager.districts.OrderBy(d => d.level.Value).First();
            // Debug.Log($"lowestDistrict {lowestDistrict.Id} of level {lowestDistrict.level.Value}");

            var lowestLv = lowestDistrict.level.Value;
            
            var next = AllDistrictMilestone[0];
            for (int i = 0; i < AllDistrictMilestone.Count; i++)
            {
                var milestone = AllDistrictMilestone[i];

                if (!allUnlockedMilestones.Contains(milestone))
                {
                    next = milestone;
                    break;
                }
            }

            // Debug.Log($"Next Milestone: {next.Id} of level {next.levelReq}");
            nextAllDistrictMilestone = next;
        }

        void LoadUnlockedMilestone()
        {
            var lowestLv = districtManager.districts.OrderBy(d => d.level.Value).First().level.Value;

            for (int i = 0; i < AllDistrictMilestone.Count; i++)
            {
                var milestone = AllDistrictMilestone[i];

                if (milestone.levelReq <= lowestLv) {
                    UnlockMilestone(milestone);
                }
                else {
                    break;
                }
            }
        }

        void UnlockMilestone(MilestoneConfig milestone)
        {
            if (!allUnlockedMilestones.Contains(milestone))
            {
                Debug.Log($"MILESTONE ALL UNLCOKED! Unlocking {milestone.Id}...");
                allUnlockedMilestones.Add(milestone);

                milestoneManager.Unlock(milestone);
            }
        }

        void SubsribeAllDistrictMilestone()
        {

            for (int i = 0; i < milestones.Count; i++)
            {
                var tracker = milestones[i].trackerItem;
                var obs = tracker.hasReached.Where(val => val == true).TakeUntilDisable(tracker).Subscribe(_ => OnTrackerReachedTrue(tracker.district.Id)).AddTo(stream);
            }
        }

        void OnTrackerReachedTrue(string id)
        {
            //Debug.Log($"Tracker on {id} Reached TRUE!");

            bool hasAllReached = true;
            for (int i = 0; i < milestones.Count; i++)
            {
                var tracker = milestones[i].trackerItem;
                if (!tracker.hasReached.Value)
                {
                    hasAllReached = false;
                    break;
                }
            }

            if (hasAllReached)
            {
                UnlockMilestone(nextAllDistrictMilestone);
                InitTracking();
            }
        }

        public void Reset()
        {
            allUnlockedMilestones.Clear();
            InitTracking();
        }
    }
}
