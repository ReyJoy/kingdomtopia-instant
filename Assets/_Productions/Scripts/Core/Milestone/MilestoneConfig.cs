﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Joyseed.IdleKingdom
{
    public class MilestoneConfig : ScriptableObject, IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public string district;
        [ReadOnly] public int levelReq;

        [ReadOnly] public string benefitTarget;
        [ReadOnly] public string benefitType;
        [ReadOnly] public float benefitModifier;
        
        private BenefitConfig _benefit;

        public string dataID { get {return Id; }}
        public BenefitConfig benefit 
        {
            get 
            {
                if (_benefit == null) {
                    _benefit = new BenefitConfig(benefitTarget, (BenefitType)System.Enum.Parse(typeof(BenefitType), benefitType), benefitModifier);
                }

                return _benefit;
            }
        }
    }

    [System.Serializable]
    public class MilestoneConfigMock : IDataModel
    {
        [ReadOnly] public string Id;
        [ReadOnly] public string district;
        [ReadOnly] public int levelReq;
        
        [ReadOnly] public string benefitTarget;
        [ReadOnly] public string benefitType;
        [ReadOnly] public float benefitModifier;
        
        private BenefitConfig _benefit;

        public string dataID { get {return Id; }}
        public BenefitConfig benefit 
        {
            get 
            {
                if (_benefit == null) {
                    _benefit = new BenefitConfig(benefitTarget, (BenefitType)System.Enum.Parse(typeof(BenefitType), benefitType), benefitModifier);
                }

                return _benefit;
            }
        }
    }

    public struct MilestoneScriptables : IMockCollection
    {
        public MilestoneConfigMock[] MilestoneConfigRow;
        public IDataModel[] Rows { get { return MilestoneConfigRow; }}
    }

    public class BenefitConfig
    {
        public string target;
        public BenefitType type;
        public float amount;

        public BenefitConfig(string target, BenefitType type, float amount)
        {
            this.target = target;
            this.type = type;
            this.amount = amount;
        }

        public BenefitConfig(BenefitType type, float amount)
        {
            this.target = "district_all";
            this.type = type;
            this.amount = amount;
        }

        public override string ToString()
        {
            return $"{target}, type {type}, amt {amount}";
        }
    }
}