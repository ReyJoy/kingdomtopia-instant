﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UniRx;
using UnityEngine;
using System;

namespace Joyseed.IdleKingdom
{
    [System.Serializable]
    public class Milestone : MonoBehaviour
    {
        [Title ("Components")]
        public MilestoneAllDistrictTrackerItem trackerItem;
        
        public MilestoneManager milestoneManager;
        private List<MilestoneConfig> milestones;
        public District district;
        public BenefitManager benefitManager;

        [Title("Variables")]
        public List<MilestoneConfig> unlockedMilestones = new List<MilestoneConfig>();

        public int level { get { return unlockedMilestones.Count + 1; }}

        Subject<MilestoneConfig> onUnlocked;
        public IObservable<MilestoneConfig> OnUnlocked() { return onUnlocked ?? (onUnlocked = new Subject<MilestoneConfig>()); }

        public void Initialize()
        {
            milestones = milestoneManager.GetMilestones(district.Id);
            SubscribeChanges();
        }

        void SubscribeChanges()
        {
            district.level.TakeUntilDisable(this).Subscribe(_ => CheckMilestone());
        }

        void CheckMilestone()
        {
            for (int i = 0; i < milestones.Count; i++)
            {
                var mile = milestones[i];

                if (mile.levelReq > district.level.Value)
                {
                    int prev = 0;

                    if (i > 0) {
                        prev = milestones[i - 1].levelReq;
                    }
                    
                    district.SetNextMilestone(prev, mile.levelReq);
                    break;
                }

                if (!unlockedMilestones.Contains(mile))
                {
                    // Debug.Log($"New Milestone Unlocked {mile.Id}");
                    milestoneManager.Unlock(mile);
                    unlockedMilestones.Add(mile);

                    if (onUnlocked != null) onUnlocked.OnNext(mile);
                }
            }
        }

        public void Reset()
        {
            unlockedMilestones = new List<MilestoneConfig>();
            CheckMilestone();
        }
    }
}