﻿using System.Collections;
using System.Collections.Generic;
using Joyseed.IdleKingdom.UI;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class ChapterView : MonoBehaviour
    {
        [FormerlySerializedAs("itemPrefab")] public ChapterItem unlockedItem;
        public ChapterItem lockedItem;
        public Transform content;
        public Button close;
        public ScrollRect scrollRect;
    }
}