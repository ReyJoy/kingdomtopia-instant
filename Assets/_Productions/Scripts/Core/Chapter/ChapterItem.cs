﻿using System.Collections;
using System.Collections.Generic;
using Joyseed.IdleKingdom.UI;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Joyseed.IdleKingdom
{
    public class ChapterItem : MonoBehaviour
    {
        public TextMeshProUGUI chapterText;
        public TextMeshProUGUI chapterDesc;
        public TextMeshProUGUI infoText;
        public Image icon;
        public Button button;
        public GameObject locked;
        public Transform claimButton;

        private int index;
        private bool showProgress = false;
        private PlayerJourney playerJourney;
        private PlayerLevel playerLevel;
        
        public void Initialize(int i)
        {
            index = i;
            playerJourney =PlayerJourney.Instance;
            playerLevel = PlayerLevel.Instance;
            
            var data = PlayerProgressionEditor.Instance.playerProgression[i];
            chapterText.text = string.Format("Chapter {0}", i);
            chapterDesc.text = data.chapterDescription;
            icon.sprite = data.icon;

            playerLevel.Level.TakeUntilDestroy(this).Subscribe(UpdateInfo);
            playerLevel.CanLevelUp.TakeUntilDestroy(this).Subscribe(UpdateButton);
            playerJourney.lifetimeRevenue.TakeUntilDestroy(this).Subscribe(_ => UpdateProgress());
            button.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(_ => ButtonClick());

            UpdateInfo(PlayerLevel.Instance.Level.Value);
        }

        void UpdateInfo(int currentLevel)
        {
            if (index <= currentLevel)
            {
                showProgress = false;
                infoText.text = "Completed";
                locked.SetActive(false);
            } else if (index == currentLevel + 1)
            {
                showProgress = true;
                infoText.text = $"{playerJourney.lifetimeRevenue.Value.ToAnnotated()}/{playerLevel.LifetimeRevenueForNextLevel.ToAnnotated()}";
                locked.SetActive(false);
            }
            else
            {
                showProgress = false;
                infoText.text = string.Format("Unlock Chapter {0}", index - 1);
                locked.SetActive(true);
            }
            
            UpdateButton(PlayerLevel.Instance.CanLevelUp.Value);
        }

        void UpdateProgress()
        {
            if (showProgress)
            {
                infoText.text = $"{playerJourney.lifetimeRevenue.Value.ToAnnotated()}/{playerLevel.LifetimeRevenueForNextLevel.ToAnnotated()}";
            }
        }

        void UpdateButton(bool canLevelUp)
        {
            if (canLevelUp)
            {
                if (playerLevel.Level.Value + 1 == index)
                {
                    button.gameObject.SetActive(true);
                    infoText.gameObject.SetActive(false);
                    locked.SetActive(false);
                }
                else
                {
                    button.gameObject.SetActive(false);
                    infoText.gameObject.SetActive(true);
                }
            }
            else
            {
                button.gameObject.SetActive(false);
                infoText.gameObject.SetActive(true);
            }
        }

        void ButtonClick()
        {
            //Curtain.Instance.HideFully(ChapterMenu.Close, playerLevel.NextStory);
        }
    }

}