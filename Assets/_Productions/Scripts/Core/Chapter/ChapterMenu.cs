﻿using System.Collections;
using System.Collections.Generic;
using Joyseed.IdleKingdom.UI;
using UniRx;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class ChapterMenu : Menu<ChapterMenu>
    {
        public ChapterView view;

        public List<ChapterItem> items = new List<ChapterItem>();

        public bool canBack = true;
        
        protected override void Awake()
        {
            base.Awake();

            for (int i = 1; i < PlayerProgressionEditor.Instance.playerProgression.Count; i++)
            {
                var data = PlayerProgressionEditor.Instance.playerProgression[i];
                if (data.icon != null)
                {
                    var item = Instantiate(view.unlockedItem, view.content);
                    items.Add(item);
                    item.Initialize(i);
                }
                else
                {
                    var item = Instantiate(view.lockedItem, view.content);
                    item.chapterText.text = string.Format("Chapter {0}", i);
                    item.chapterDesc.text = data.chapterDescription;
                    items.Add(item);
                }
            }

            view.close.OnClickAsObservable().TakeUntilDestroy(this).Subscribe(_ => Close());
        }

        public override void CloseMenu()
        {
            if (!canBack) return;
            base.CloseMenu();
        }
    }
}