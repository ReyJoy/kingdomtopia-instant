﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class InstallButton : MonoBehaviour
{
    public RectTransform buttonRect;
    private RectTransform rect;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    void OnEnable()
    {
        buttonRect.localScale = Vector3.zero;
        rect.DOAnchorPosX(340f, .3f).From().SetEase(Ease.OutBack).OnComplete(ShowButton);
        
        //rect.DOSizeDelta(new Vector2(rect.sizeDelta.x, 0), .3f).From();
    }

    private void ShowButton()
    {
        buttonRect.DOScale(Vector3.one, .3f).SetEase(Ease.OutBack);
    }
}
