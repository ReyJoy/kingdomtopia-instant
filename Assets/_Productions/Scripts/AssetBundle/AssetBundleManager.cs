﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AssetBundleManager : MonoBehaviour
{
    public AssetPackage[] assetPackages;

    public GameObject GetGameObjectAsset(string bundleName, string assetName)
    {
        return (from assetPackage in assetPackages where assetPackage.bundleName == bundleName select assetPackage.assetBundle.LoadAsset<GameObject>(assetName)).FirstOrDefault();
    }
    
    public Sprite GetSpriteAsset(string bundleName, string assetName)
    {
        return (from assetPackage in assetPackages where assetPackage.bundleName == bundleName select assetPackage.assetBundle.LoadAsset<Sprite>(assetName)).FirstOrDefault();
    }

    public void LoadScene(string bundleName, int sceneIndex)
    {
        foreach (var assetPackage in assetPackages)
        {
            if (assetPackage.bundleName == bundleName)
            {
                string[] scenesPath = assetPackage.assetBundle.GetAllScenePaths();
                SceneManager.LoadScene(scenesPath[sceneIndex]);
            }
        }
    }

    /*
    public string GetSceneName(string bundleName, int index)
    {
        foreach (var assetPackage in assetPackages)
        {
            if (assetPackage.bundleName == bundleName)
            {
                var scenes = assetPackage.assetBundle.GetAllScenePaths();
                return scenes[index];
            }
        }

        return "";
    }*/
}
