﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameLoader : MonoBehaviour
{
    //public Image loadingBar;
    //public Text progress;
    public string bundleUrl;
    public AssetBundleManager bundleManager;
    public GameObject loading;

    private void Start()
    {
        StartCoroutine(LoadAssetBundleFromWeb());
    }

    private IEnumerator LoadAssetBundleFromWeb()
    {
        foreach (var assetPackage in bundleManager.assetPackages)
        {
            Debug.Log($"Loading AssetBundle {assetPackage.bundleName}");
            var webAssetUrl = $"{bundleUrl}{assetPackage.bundleName}";
                
            using (var www = UnityWebRequestAssetBundle.GetAssetBundle(webAssetUrl))
            {
                var op = www.SendWebRequest();

                while (!op.isDone)
                {
                    //loadingBar.fillAmount = www.downloadProgress;
                    //progress.text = $"{www.downloadProgress * 100}%";
                    yield return null;
                }
                    
                assetPackage.assetBundle = DownloadHandlerAssetBundle.GetContent(www);
                    
                if (assetPackage.assetBundle == null)
                {
                    Debug.LogError($"Failed to load AssetBundle for {assetPackage.bundleName}");
                    loading.SetActive(false);
                    yield break;
                }

                Debug.Log($"Success loading AssetBundle for {assetPackage.bundleName}");
            }
        }
        //loading.SetActive(false);
        //loadFinished.Raise();
        bundleManager.LoadScene("instant", 0);
    }
}
