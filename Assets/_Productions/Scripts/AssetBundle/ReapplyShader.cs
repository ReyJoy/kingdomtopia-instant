﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using TMPro;
using UnityEngine;

public class ReapplyShader : MonoBehaviour
{

    void Start()
    {
        ReapplyTMP();
        ReapplySpine();
    }
    
    private void ReapplyTMP()
    {
        foreach (TextMeshProUGUI rend in transform.GetComponentsInChildren<TextMeshProUGUI>(true))
        {
            Material[] materials = rend.fontSharedMaterials;
         
            for (int i = 0; i < materials.Length; i++)
            {
                if (materials[i] == null || materials[i].shader == null)
                    continue;
 
                string shaderName = materials[i].shader.name;
                Shader shader = Shader.Find(shaderName);

                if (shader != null)
                {
                    materials[i].shader = shader;
                }
            }
 
            rend.fontMaterials = materials;
        }
    }
    
    private void ReapplySpine()
    {
        foreach (SkeletonGraphic rend in transform.GetComponentsInChildren<SkeletonGraphic>(true))
        {
            Material materials = rend.material;
            
            string shaderName = materials.shader.name;
            Shader shader = Shader.Find(shaderName);
            
            materials.shader = shader;

            rend.material = materials;
        }
    }
}
