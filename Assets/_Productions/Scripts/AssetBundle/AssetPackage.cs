﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AssetPackage
{
    public string bundleName;
    public AssetBundle assetBundle;
}
