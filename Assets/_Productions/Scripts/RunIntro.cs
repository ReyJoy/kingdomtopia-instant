﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Joyseed.IdleKingdom
{
    public class RunIntro : MonoBehaviour
    {
        public Sequence introSequence;

        void Start()
        {
            introSequence.RunSequence();
        }
    }

}