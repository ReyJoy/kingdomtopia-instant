﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
namespace Joyseed.IdleKingdom
{
    public class ScriptableCreator
    {
        private static string assetPath = "Assets/_Productions/Data";

        [MenuItem("DataCreator/Create All")]
        private static void CreateAll()
        {
            CreateDistricts();
            CreateMilestones();
            CreateRenovations();
            CreateMayors();
            CreateHeirloomUpgrades();
            CreateBoosters();
            CreatePlayerLevels();
        }

        [MenuItem("DataCreator/Create Districts")]
        private static void CreateDistricts()
        {
            GenerateDatabase<DistrictConfig, DistrictScriptables>();
        }
        
        [MenuItem("DataCreator/Create Milestones")]
        private static void CreateMilestones()
        {
            GenerateDatabase<MilestoneConfig, MilestoneScriptables>();
        }
        
        [MenuItem("DataCreator/Create Renovations")]
        private static void CreateRenovations()
        {
            GenerateDatabase<RenovationConfig, RenovationScriptables>();
        }
        
        [MenuItem("DataCreator/Create Mayors")]
        private static void CreateMayors()
        {
            GenerateDatabase<MayorConfig, MayorScriptables>();
        }
        
        [MenuItem("DataCreator/Create Heirloom Upgrades")]
        private static void CreateHeirloomUpgrades()
        {
            GenerateDatabase<HeirloomUpgradeConfig, HeirloomUpgradeScriptables>();
        }
        
        [MenuItem("DataCreator/Create Boosters")]
        private static void CreateBoosters()
        {
            GenerateDatabase<BoosterConfig, BoosterScriptables>();
        }
        
        [MenuItem("DataCreator/Create Player Levels")]
        private static void CreatePlayerLevels()
        {
            GenerateDatabase<PlayerLevelConfig, PlayerLevelScriptables>();
        }

        private static void GenerateDatabase<T, U>() where T : ScriptableObject where U : IMockCollection
        {
            string typeName = typeof(T).Name;

            Debug.Log($"Generating Database of {typeName} from collection {typeof(U).Name}");
            string dataText = Resources.Load<TextAsset>($"Database/{typeName}").text;
            U collections = JsonUtility.FromJson<U>(dataText);

            GenerateScriptableObjects<T>(collections.Rows);
        }

        private static void GenerateScriptableObjects<T>(IDataModel[] items) where T : ScriptableObject
        {
            string typeName = typeof(T).Name;

            for (int i = 0; i < items.Length; i++)
            {
                EditorUtility.DisplayProgressBar($"Generating {typeName}", $"Converting {items[i].dataID}", (float)i / (float)items.Length);

                var json = JsonUtility.ToJson(items[i]);
                T data = ScriptableObject.CreateInstance<T>();
                JsonUtility.FromJsonOverwrite(json, data);
                
                string path = $"{assetPath}/{typeName}/{items[i].dataID}.asset";
                AssetDatabase.CreateAsset(data, path);
            }

            EditorUtility.ClearProgressBar();
        }
    }
}
#endif