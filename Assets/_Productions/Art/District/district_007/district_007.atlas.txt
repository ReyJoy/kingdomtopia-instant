
district_007.png
size: 2048,512
format: RGBA8888
filter: Linear,Linear
repeat: none
asep1
  rotate: false
  xy: 1053, 305
  size: 40, 39
  orig: 40, 39
  offset: 0, 0
  index: -1
asep2
  rotate: true
  xy: 71, 0
  size: 64, 56
  orig: 64, 56
  offset: 0, 0
  index: -1
back table
  rotate: true
  xy: 1959, 419
  size: 93, 85
  orig: 93, 85
  offset: 0, 0
  index: -1
backtableShadow
  rotate: false
  xy: 1752, 435
  size: 109, 77
  orig: 109, 77
  offset: 0, 0
  index: -1
barrel
  rotate: true
  xy: 1505, 394
  size: 31, 38
  orig: 31, 38
  offset: 0, 0
  index: -1
box
  rotate: false
  xy: 1284, 351
  size: 34, 37
  orig: 34, 37
  offset: 0, 0
  index: -1
boxShadow
  rotate: false
  xy: 774, 192
  size: 49, 20
  orig: 49, 20
  offset: 0, 0
  index: -1
bubble
  rotate: true
  xy: 528, 161
  size: 12, 33
  orig: 12, 33
  offset: 0, 0
  index: -1
crobong
  rotate: false
  xy: 0, 0
  size: 71, 64
  orig: 71, 64
  offset: 0, 0
  index: -1
crobong besi
  rotate: false
  xy: 182, 15
  size: 57, 49
  orig: 57, 49
  offset: 0, 0
  index: -1
ember
  rotate: false
  xy: 239, 29
  size: 34, 35
  orig: 34, 35
  offset: 0, 0
  index: -1
emberShadow
  rotate: false
  xy: 888, 285
  size: 36, 18
  orig: 36, 18
  offset: 0, 0
  index: -1
fire
  rotate: false
  xy: 774, 212
  size: 57, 51
  orig: 57, 51
  offset: 0, 0
  index: -1
kitchen
  rotate: false
  xy: 1861, 415
  size: 98, 97
  orig: 98, 97
  offset: 0, 0
  index: -1
mainBuilding
  rotate: false
  xy: 528, 263
  size: 304, 249
  orig: 304, 249
  offset: 0, 0
  index: -1
mainbuildingShadow
  rotate: false
  xy: 0, 64
  size: 331, 93
  orig: 331, 93
  offset: 0, 0
  index: -1
pizza stall
  rotate: true
  xy: 1053, 344
  size: 168, 231
  orig: 168, 231
  offset: 0, 0
  index: -1
pizzastallShadow
  rotate: false
  xy: 1284, 388
  size: 221, 124
  orig: 221, 124
  offset: 0, 0
  index: -1
plang
  rotate: true
  xy: 832, 269
  size: 34, 56
  orig: 34, 56
  offset: 0, 0
  index: -1
stall
  rotate: false
  xy: 832, 303
  size: 221, 209
  orig: 221, 209
  offset: 0, 0
  index: -1
stallShadow
  rotate: false
  xy: 331, 60
  size: 196, 97
  orig: 196, 97
  offset: 0, 0
  index: -1
table
  rotate: false
  xy: 528, 173
  size: 246, 90
  orig: 246, 90
  offset: 0, 0
  index: -1
tableShadow
  rotate: false
  xy: 1505, 425
  size: 247, 87
  orig: 247, 87
  offset: 0, 0
  index: -1
unconstructedGourmet
  rotate: false
  xy: 0, 157
  size: 528, 355
  orig: 528, 355
  offset: 0, 0
  index: -1
wajan
  rotate: true
  xy: 127, 0
  size: 64, 55
  orig: 64, 55
  offset: 0, 0
  index: -1
